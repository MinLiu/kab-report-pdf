﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace KABReport.Models
{

    public class FTPInfoViewModel
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public FTPInfoViewModel(int id)
        {
            Host = WebConfigurationManager.AppSettings["FTP_Host"] + "/" + id;
            Username = WebConfigurationManager.AppSettings["FTP_Username"];
            Password = WebConfigurationManager.AppSettings["FTP_Password"];
        }
    }
}

