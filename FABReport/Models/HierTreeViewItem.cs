﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace KABReport.Models
{

    public class HierTreeViewItem
    {
        public string id { get; set; }
        public string Name { get; set; }
        public string Metaname { get; set; }
        public bool hasChildren { get; set; }
        public int Count { get; set; }
    }
}

