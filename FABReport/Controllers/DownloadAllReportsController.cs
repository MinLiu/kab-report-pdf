﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KABReport.Models;
using ICSharpCode.SharpZipLib.Zip;

namespace KABReport.Controllers
{
    [Authorize]
    public class DownloadAllReportsController : BaseController
    {
        // GET: FileDownload
        public void Index(ProjectViewModel model)
        {
            Project project = new ProjectRepository().Find(model.ID);
            List<ProjectReport> reportList = GetReportList(model.ID);
            
            string zipName = project.Name + "_reports";


            try
            {
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + zipName + ".zip\"");
                Response.ContentType = "application/zip";
                using (var zipStream = new ZipOutputStream(Response.OutputStream))
                {
                    foreach (var item in reportList)
                    {
                        byte[] fileBytes = null;
                        try
                        {
                            fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(item.RelativePath));
                            var fileEntry = new ZipEntry(item.ReportLabel + "." + item.Type)
                            {
                                Size = fileBytes.Length
                            };
                            zipStream.PutNextEntry(fileEntry);
                            zipStream.Write(fileBytes, 0, fileBytes.Length);
                        }
                        catch { }
                    }

                    zipStream.Flush();
                    zipStream.Close();
                }

                Response.Flush();
                //Response.End();
                Logger.Info(CurrentUser.Email + "  Download " + reportList.Count() + " files.");

                // Log
                LogDownload(reportList);
            }
            catch { }

            //bool success = MultifilesDownload(zipName, reportList.Select(r => r.RelativePath));

            //return Json(model, JsonRequestBehavior.AllowGet);
        }


        private List<ProjectReport> GetReportList(int porjectID)
        {
            var userMetanames = new ProjectUserHierarchyItemRepository().Read()
                                                                        .Where(puh => puh.ProjectID == porjectID)
                                                                        .Where(puh => puh.UserID == CurrentUser.Id)
                                                                        .Select(puh => puh.Metaname)
                                                                        .ToList();
            ProjectUser projectUser = new ProjectUserRepository().Read()
                                                                 .Where(pu => pu.ProjectID == porjectID)
                                                                 .Where(pu => pu.UserID == CurrentUser.Id)
                                                                 .FirstOrDefault();

            var accessMetaNames = new List<string>();
            // Owners can look down
            if (projectUser.Role.ToLower() == "view down")
            {
                var userHierarchies = new ProjectHierarchyRepository().Read()
                                                                      .Where(x => x.ProjectID == porjectID)
                                                                      .Where(x => userMetanames.Contains(x.Metaname))
                                                                      .ToList();
                var childrenHiers = new List<ProjectHierarchy>();
                foreach (var userHierarchy in userHierarchies)
                {
                    userHierarchy.GetChildrenHiers(childrenHiers);
                }

                accessMetaNames = childrenHiers.Distinct()
                                               .Select(x => x.Metaname)
                                               .ToList();
            }
            // Only has the the privilege of his own report
            else
            {
                accessMetaNames = userMetanames;
            }

            var ReportList = new ProjectReportRepository().Read()
                                                          .Where(r => r.ProjectID == porjectID)
                                                          .Where(r => accessMetaNames.Contains(r.Metaname))
                                                          .Where(r => r.Uploaded == true)
                                                          .ToList();

            return ReportList;
        }

        public void Multifiles(int projectId, IEnumerable<int> ids)
        {
            Project project = new ProjectRepository().Find(projectId);
            List<ProjectReport> reportList = new ProjectReportRepository().Read().Where(r => ids.Contains(r.ID)).ToList();

            string zipName = project.Name + "_reports";


            try
            {
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + zipName + ".zip\"");
                Response.ContentType = "application/zip";
                using (var zipStream = new ZipOutputStream(Response.OutputStream))
                {
                    foreach (var item in reportList)
                    {
                        byte[] fileBytes = null;
                        try
                        {
                            fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(item.RelativePath));
                            var fileEntry = new ZipEntry(item.ReportLabel + "." + item.Type)
                            {
                                Size = fileBytes.Length
                            };
                            zipStream.PutNextEntry(fileEntry);
                            zipStream.Write(fileBytes, 0, fileBytes.Length);
                        }
                        catch { }
                    }

                    zipStream.Flush();
                    zipStream.Close();
                }

                Response.Flush();
                //Response.End();
                // Log
                LogDownload(reportList);
            }
            catch { }

            //bool success = MultifilesDownload(zipName, reportList.Select(r => r.RelativePath));

            //return Json("", JsonRequestBehavior.AllowGet);
        }

        public void LogDownload(List<ProjectReport> reportList)
        {
            // Get this project user
            if (reportList.Any())
            {
                var projectID = reportList.First().ProjectID;
                ProjectUser projectUser = new ProjectUserRepository().Read()
                                                                     .Where(pu => pu.ProjectID == projectID)
                                                                     .Where(pu => pu.UserID == CurrentUser.Id)
                                                                     .FirstOrDefault();
                if (projectUser == null) return;

                // Log if user is not an admin or super admin
                using (var context = new SnapDbContext())
                {
                    var logsToAdd = new List<ActivityLog>();
                    foreach (var report in reportList)
                    {
                        logsToAdd.Add(new ActivityLog()
                        {
                            UserID = CurrentUser.Id,
                            ProjectID = report.ProjectID,
                            ProjectUserID = projectUser.ID,
                            IPAddress = Request.UserHostAddress,
                            Timestamp = DateTime.Now,
                            Description = String.Format("Download File \"{0}\"", report.ReportFile)
                        });
                    }
                    context.ActivityLogs.AddRange(logsToAdd);
                    context.SaveChanges();
                }
            }
        }
    }
}