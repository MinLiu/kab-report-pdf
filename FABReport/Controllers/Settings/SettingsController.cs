﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class SettingsController : BaseController
    {
        
        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult EditLanguageOptions()
        {
            return View();
        }

    }
}