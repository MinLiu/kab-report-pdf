﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KABReport.Schedule;
using Quartz;
using Fruitful.Email;
using System.Net.Mail;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class EmailSettingsController : BaseController
    {
        [HttpGet]
        public ActionResult Edit()
        {
            using (var context = new SnapDbContext())
            {
                var model = context.EmailSettings.First();
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(Fruitful.Email.EmailSetting emailSetting)
        {
            using (var context = new SnapDbContext())
            {
                var model = context.EmailSettings.First();
                if (ModelState.IsValid)
                {
                    if (emailSetting.Threshhold / emailSetting.Interval >= 7)
                    {
                        ViewBag.ErrorMessage = "The batch conditions that you have entered are unsuitable. Please select a batch configuration that does not exceed the ratio of 7 emails sent every 1 second.";
                        return View(model);
                    }

                    Schedule.Schedule.ChangeEmailBatchingInterval(model.Interval, emailSetting.Interval);
                    Schedule.ScheduleSendingEmail.ThreshHold = emailSetting.Threshhold;

                    model.Email = emailSetting.Email;
                    model.EnableSSL = emailSetting.EnableSSL;
                    model.Host = emailSetting.Host;
                    model.Password = emailSetting.Password;
                    model.Port = emailSetting.Port;
                    model.Threshhold = emailSetting.Threshhold;
                    model.Interval = emailSetting.Interval;
                    context.SaveChanges();



                    ViewBag.Saved = "Success";
                }
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult SendTestMail(string emailFrom, string password, string host, int port, bool enableSSL, string emailTo)
        {
            EmailSetting emailSetting = new EmailSetting()
            {
                Email = emailFrom,
                Password = password,
                Host = host,
                Port = port,
                EnableSSL = enableSSL
            };

            Emailer emailer = new Emailer(emailSetting);

            MailMessage mailMessage = new MailMessage()
            {
                Subject = "K&B Email setting test",
                IsBodyHtml = true,
                Body = "Success",
            };
            mailMessage.To.Add(emailTo);
            try
            {
                string error;
                emailer.SendEmail(mailMessage, out error);
            }
            catch
            {
                return Json(new { Status = "Fail", Message = "Oops! Something wrong with your email settings." });
            }

            return Json(new { Status = "Success", Message = "Sent. Please check the mailbox to make sure the settings is correct." });
        }
    }

}