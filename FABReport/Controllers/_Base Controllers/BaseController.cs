﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using System.Web.Routing;
using Kendo.Mvc.UI;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Data.Entity;
using System;
using System.IO;
using log4net;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;

namespace KABReport.Controllers
{
    public class BaseController : Controller
    {
        public UserManager UserManager;
        public User CurrentUser;  //This is the current user logged in.
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            try
            {
                if (!User.Identity.IsAuthenticated) return;

                UserManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();

                var userId = User.Identity.GetUserId().ToString();
                var userRepo = new UserRepository();
                CurrentUser = userRepo.Read().Where(u => u.Id == userId).First();

                //if (Page == PageType.AdminPage && !UserManager.GetRoles(CurrentUser.Id).Contains("Admin"))
                //{
                //    Response.Redirect("asdw");
                //}

                ////Check Login Misuse by looking at the cookies
                //if (CurrentUser.Token != (Request.Cookies["GG"] ?? new HttpCookie("GG")).Value)
                //{
                //    ControllerContext.HttpContext.GetOwinContext().Authentication.SignOut();
                //    Response.Redirect("/Home/LoginMisuse");
                //}

            }
            catch {
                
            }

        }

        protected ILog Logger
        {
            get { return LogManager.GetLogger(GetType()); }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }
            }

            base.Dispose(disposing);
        }


        public string SaveFile(HttpPostedFileBase file, string subFolder = "/")
        {
            if (file == null) return null;

            const string contentFolderRoot = "/Uploads";

            // NEED TO BE MODIFIED
            var virtualPath = string.Format("{0}/{1}/{2}", contentFolderRoot, CurrentUser.Id.ToString(), subFolder);


            var physicalPath = Server.MapPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var extension = Path.GetExtension(file.FileName);
            var str = Guid.NewGuid().ToString() + extension;
            var physicalSavePath = Server.MapPath(virtualPath) + str;
            file.SaveAs(physicalSavePath);
            return physicalSavePath;
        }


        public bool DeleteFile(string filepath)
        {
            try
            {
                var physicalPath = Server.MapPath(filepath);
                if (System.IO.File.Exists(physicalPath)) System.IO.File.Delete(physicalPath);
                return true;
            }
            catch { return false; }
        }


        ///// <summary>
        ///// Compress multiple files to one zip file then flush to client side
        ///// </summary>
        ///// <param name="zipFileName"></param>
        ///// <param name="filePathList"> relative file paths</param>
        ///// <returns></returns>
        //protected bool MultifilesDownload(string zipFileName, IEnumerable<string> filePathList)
        //{
        //    try
        //    {
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + zipFileName + ".zip");
        //        Response.ContentType = "application/zip";
        //        using (var zipStream = new ZipOutputStream(Response.OutputStream))
        //        {
        //            foreach (string filePath in filePathList)
        //            {
        //                byte[] fileBytes = null;
        //                try
        //                {
        //                    fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(filePath));
        //                    var fileEntry = new ZipEntry(Path.GetFileName(filePath))
        //                    {
        //                        Size = fileBytes.Length
        //                    };
        //                    zipStream.PutNextEntry(fileEntry);
        //                    zipStream.Write(fileBytes, 0, fileBytes.Length);
        //                }
        //                catch { }
        //            }

        //            zipStream.Flush();
        //            zipStream.Close();
        //        }

        //        Response.Flush();
        //        Response.End();

        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

    }
}