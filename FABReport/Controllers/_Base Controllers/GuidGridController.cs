﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using Kendo.Mvc;
using System.Linq.Expressions;


namespace KABReport.Controllers
{
    public class GuidGridController<TModel, TViewModel> : BaseController
        where TModel : class, IGuidEntity, new()
        where TViewModel : class, IGuidEntityViewModel, new()
    {
        protected readonly EntityRespository<TModel> _repo;
        protected readonly ModelMapper<TModel, TViewModel> _mapper;

        public GuidGridController(EntityRespository<TModel> repo, ModelMapper<TModel, TViewModel> mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _repo.Read();
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Create([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            //if (ModelState.IsValid)
            //{
                // Map the view model to the model.
                var model = _mapper.MapToModel(viewModel);

                // Add the new entity to the database.
                _repo.Create(model);

                // Update the ID of the view model to match the ID of the newly-created entity.
                viewModel.ID = model.ID;
            //}

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Update([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            //if (ModelState.IsValid)
            //{
                // Find the existing entity.
                var model = _repo.Find(viewModel.ID);

                // Map the view model to the model and update the database.
                _mapper.MapToModel(viewModel, model);
                _repo.Update(model);
            //}

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Destroy([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            //if (ModelState.IsValid)
            //{
                // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
                var model = new TModel { ID = viewModel.ID };
                _repo.Delete(model);
           // }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        public IQueryable<TModel> ApplyQueryFiltersSort([DataSourceRequest]DataSourceRequest request, IQueryable<TModel> query, string defaultSortColumn = "ID")
        {
            //Filtering            
            if (request.Filters.Any())
            {
                var filter = request.Filters.GetFilter<TModel>();
                query = query.Where(filter);
            }

            //Sorting
            if (request.Sorts.Any())
            {
                foreach (var x in request.Sorts)
                {
                    query = query.OrderBy(x.Member + ((x.SortDirection == ListSortDirection.Ascending) ? " ASC" : " DESC")) as IQueryable<TModel>;
                }
            }
            else
            {
                query = query.OrderBy(defaultSortColumn);
            }

            return query;
        }

        protected override void Dispose(bool disposing)
        {
            if (_repo != null)
                _repo.Dispose();

            base.Dispose(disposing);
        }

    }
}