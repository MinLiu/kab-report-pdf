﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Super Admin,Admin")]
    public class UsersController : BaseController
    {


        #region User Management List View

        // GET: Users
        public ActionResult Index(ProjectViewModel viewModel)
        {
            ViewBag.ProjectID = viewModel.Name == null ? 0 : viewModel.ID;
            return View();
        }

        // READ: GridUsers
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, string filterText, int? filterProjectID, bool onlyAdmin)
        {

            var userRepo = new UserRepository();
            var roleRepo = new RoleRepository();

            var roles = roleRepo.Read().ToList();

            var users = userRepo.Read().Where(x => 
                                            (
                                                 (x.Email.ToLower().Contains(filterText) || filterText == "" || filterText == null)
                                                    || (x.FirstName.ToLower().Contains(filterText) || filterText == "" || filterText == null)
                                                    || (x.LastName.ToLower().Contains(filterText) || filterText == "" || filterText == null)
                                                   )
                                            )
                                            .Where(x => filterProjectID == null || x.ProjectUsers.Where(pu => pu.ProjectID == filterProjectID.Value).Count() > 0)
                                            .Where(x => onlyAdmin == false || x.Roles.Select(r => r.RoleId).Contains("1"))
                                            .Include(x => x.Roles)
                                            .OrderBy(x => x.Email)
                                            .ToList()
                                            .Select( u => new UserViewModel
                                                        {
                                                            ID = u.Id,
                                                            Email = u.Email,
                                                            FirstName = u.FirstName,
                                                            LastName = u.LastName,
                                                            PhoneNumber = u.PhoneNumber,
                                                            Role = string.Join(",", roles.Where(r => u.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                                                            Confirmed = u.EmailConfirmed,                                                            
                                                        });

            return Json(users.ToDataSourceResult(request));
        }



        public ActionResult Destroy([DataSourceRequest]DataSourceRequest request, UserViewModel viewModel)
        {
            ProjectUserRepository projectUserRepo = new ProjectUserRepository();
            List<ProjectUser> projectUsers = projectUserRepo.Read().Where(pu => pu.UserID == viewModel.ID).ToList();
            var projectUserIDs = projectUsers.Select(x => x.ID).ToList();

            // Delete Email Queue
            EmailQueueRepository emailQueueRepo = new EmailQueueRepository();
            IEnumerable<EmailQueue> emailList = emailQueueRepo.Read().Where(e => projectUserIDs.Contains(e.ProjectUserID));
            emailQueueRepo.Delete(emailList);

            // Delete ProjectUserHierarchyItems
            ProjectUserHierarchyItemRepository userHierarchyRepo = new ProjectUserHierarchyItemRepository();
            List<ProjectUserHierarchyItem> userHierarchyitems = userHierarchyRepo.Read().Where(uhi => uhi.UserID == viewModel.ID).ToList();
            userHierarchyRepo.Delete(userHierarchyitems);

            // Delete Activity Logs
            ActivityLogRepository activityLogRepo = new ActivityLogRepository();
            IEnumerable<ActivityLog> activityLogList = activityLogRepo.Read().Where(e => projectUserIDs.Contains(e.ProjectUserID));
            activityLogRepo.Delete(activityLogList);

            // Delete ProjectUsers
            projectUserRepo.Delete(projectUsers);

            // Delete Report Cache
            UserReportCacheRepository cacheRepo = new UserReportCacheRepository();
            IEnumerable<UserReportCache> cacheList = cacheRepo.Read().Where(x => x.UserID == viewModel.ID);
            cacheRepo.Delete(cacheList);
            
            // Delete User
            var repo = new UserRepository();
            var model = repo.Read().Where(u => u.Id == viewModel.ID).First();
            repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }



        #endregion


        #region Create User

        public ActionResult Create()
        {
            return View(
                new NewUserViewModel 
                {
                    
                }
            );
        }


        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(NewUserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "UserManagement");

            var repo = new UserRepository();

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(model);
            }


            //Check to see if email is already taken
            if (UserManager.FindByEmail(model.Email) != null)
            {
                ViewBag.ErrorMessage = "The email address entered is already being used.";
                return View(model);
            }

            var user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                //CompanyID = CurrentUser.CompanyID,  //Set the company ID as the same Current User
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                EmailConfirmed = true
            };

            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                ViewBag.SuccessMessage = "New user account created.";
                UserManager.AddToRole(user.Id, model.Role);
                UserManager.Update(user);

                return RedirectToAction("Index", "Users");
            }
            else
            {
                ViewBag.ErrorMessage = result.Errors.First().ToString();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion


        #region Edit User


        // GET: User Edit
        public ActionResult Edit(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "Users");

            try
            {
                var roleRepo = new RoleRepository();
                var roles = roleRepo.Read().ToList();

                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.Id.Equals(ID)).Include(u => u.Roles).First();

                //Create model then return it
                UserViewModel model = new UserViewModel
                {
                    ID = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    Role = string.Join(",", roles.Where(r => user.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                };

                return View("Edit", model);
            }
            catch
            {
                return RedirectToAction("Index", "Users");
            }
        }



        //
        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "Users");

            if (!ModelState.IsValid)
            {
                return PartialView("Edit", model);
            }
                        
            try
            {

                User user = UserManager.FindById(model.ID);

                //Check to see if the email address is already taken if the same email address is not input.
                if (user.Email != model.Email)
                {
                    if (UserManager.FindByEmail(model.Email) != null) //If a user already exsists with the user name then stop saving.
                    {
                        ViewBag.ErrorMessage = "Could not save changes. The Email address entered is already taken.";
                        return PartialView("Edit", model);
                    }
                }

                //Update the user
                user.UserName = model.Email;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;

                //Remove all the roles then add the selected role
                var roleRepo = new RoleRepository();
                var roleNames = roleRepo.Read().Select(r => r.Name).ToArray();
                foreach (var roleName in roleNames)
                {
                    UserManager.RemoveFromRole(user.Id, roleName);
                }
                UserManager.AddToRole(user.Id, model.Role);

                //Save Changes
                UserManager.Update(user);
                
                ViewBag.SuccessMessage = "User Account Details have successfully been Saved.";
            }
            catch
            {
                ViewBag.ErrorMessage = "Could not save changes. Unknown Exception.";
            }


            return PartialView("Edit", model);
        }

        #endregion


        #region Change Password

        // GET: Change Password
        public ActionResult ChangePassword(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "Users");

            try
            {
                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.Id.Equals(ID)).First();

                //Create model then return it
                ChangeUserPasswordViewModel model = new ChangeUserPasswordViewModel
                {
                    ID = user.Id,
                    Email = user.Email
                };

                return View(model);
            }
            catch { return RedirectToAction("Index", "Users"); }

        }


        // POST: Change Password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangeUserPasswordViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "Users");

            if (!ModelState.IsValid)
            {
                return PartialView("ChangePassword", model);
            }

            try
            {
                //Try Get User account
                var user = UserManager.FindById(model.ID);

                if (user == null)
                {
                    ViewBag.ErrorMessage = "User Account not found.";
                    return PartialView("ChangePassword", model);
                }

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                IdentityResult result = UserManager.ResetPassword(user.Id, code, model.Password);

                if (result.Succeeded)
                {
                    //user.PlainPassword = model.Password;
                    //UserManager.Update(user);
                    var projectUserRepo = new ProjectUserRepository();
                    projectUserRepo.Read().Where(pu => pu.UserID == user.Id).Each(pu => pu.Password = model.Password);
                    projectUserRepo.Update();
                    ViewBag.SuccessMessage = "Password was successfully reset.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Failed to reset password. " + result.Errors.First().ToString();
                }

                model.Password = "";
                model.ConfirmPassword = "";
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Failed to reset password. " + ex.ToString();
            }

            return PartialView(model);
        }


        #endregion




        #region Helpers


        public JsonResult DropDownRoles()
        {
            using (var context = new SnapDbContext())
            {                
                var roles = context.Roles
                    .Where(r => !r.Name.Contains("Super"))
                    .OrderBy(r => r.Name).ToArray()
                    .Select(r => new RoleViewModel { ID = r.Id, Name = r.Name })
                    .ToList();

                return Json(roles, JsonRequestBehavior.AllowGet);
            }
        }
        
        #endregion
    }
}