﻿using Fruitful.Email;
using KABReport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;

namespace KABReport.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
              
            return View();
        }


        public ActionResult Dashboard()
        {           
            return View();
        }


        //public ActionResult About()
        //{           
        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult Contact(ContactUsViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        string title = "KABReport - " + model.Category;
        //        string message = string.Format(title + "\n================================================\n");
        //        message += string.Format("\nCategory: {0}", model.Category);
        //        message += string.Format("\nContact Email: {0}", model.Email);
        //        message += string.Format("\nUsername: {0}", model.Username);
        //        message += string.Format("\n\nMessage:\n{0}", model.Message);

        //        try
        //        {
        //            EmailService.SendEmail("info@snap-suite.com", title, message, Server);
        //            ViewBag.SuccessMessage = "Thank you! We will review your message as soon as possible.";
        //        }
        //        catch
        //        {
        //            ViewBag.ErrorMessage = "Failed to send message. Please try again.";
        //        }
        //    }

        //    return View(model);
        //}

        //public ActionResult LearnMore()
        //{
        //    return View();
        //}

        //public ActionResult Pricing()
        //{
        //    return View();
        //}

        //public ActionResult Privacy()
        //{
        //    return View();
        //}

        //public ActionResult Terms()
        //{
        //    return View();
        //}

        //public ActionResult Releases()
        //{
        //    return View();
        //}


        public ActionResult SubscriptionInvalid()
        {
            return View();
        }


        public ActionResult _404()
        {
            return View();
        }


        public ActionResult LoginMisuse()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return View();
        }

        public ActionResult Inactive()
        {
            return View();
        }

        public ActionResult UpdateCache()
        {
            var projectUserList = new ProjectUserRepository().Read().ToList();

            var reportCacheRepo = new UserReportCacheRepository();
            foreach (var projectUser in projectUserList)
            {
                var accessMetaNames = new List<string>();
                var cache = reportCacheRepo.Read().Where(x => x.ProjectID == projectUser.ProjectID && x.UserID == projectUser.UserID);
                if (cache.Count() == 0)
                {
                    var userMetanames = new ProjectUserHierarchyItemRepository().Read()
                                                                                .Where(puh => puh.ProjectID == projectUser.ProjectID)
                                                                                .Where(puh => puh.UserID == projectUser.UserID)
                                                                                .Select(puh => puh.Metaname)
                                                                                .ToList();
                    // Owners can look down
                    if (projectUser.Role.ToLower() == "view down")
                    {
                        var userHierarchies = new ProjectHierarchyRepository().Read()
                                                                              .Where(x => x.ProjectID == projectUser.ProjectID)
                                                                              .Where(x => userMetanames.Contains(x.Metaname))
                                                                              .ToList();
                        var childrenHiers = new List<ProjectHierarchy>();
                        foreach (var userHierarchy in userHierarchies)
                        {
                            userHierarchy.GetChildrenHiers(childrenHiers);
                        }

                        accessMetaNames = childrenHiers.Distinct()
                                                       .Select(x => x.Metaname)
                                                       .ToList();
                    }
                    // Only has the the privilege of his own report
                    else
                    {
                        accessMetaNames = userMetanames;
                    }

                    var cacheToAdd = new List<UserReportCache>();
                    foreach (var item in accessMetaNames)
                    {
                        var toAdd = new UserReportCache()
                        {
                            AccessMetaname = item,
                            ProjectID = projectUser.ProjectID,
                            UserID = projectUser.UserID
                        };
                        cacheToAdd.Add(toAdd);
                    }
                    reportCacheRepo.Create(cacheToAdd);
                }
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }


        public ActionResult ForceSendingEmails()
        {
            Logger.Info("Force Email Sending Routine Begins");
            ProjectUserRepository projectUserRepo = new ProjectUserRepository();
            EmailQueueRepository emailQueueRepo = new EmailQueueRepository();
            Emailer emailer = new Emailer(new SnapDbContext());
            DateTime now = DateTime.Now;
            List<EmailQueue> emailsToSend = emailQueueRepo.Read().Where(eq => eq.SendingTime == null).ToList();

            foreach (EmailQueue entry in emailsToSend)
            {
                MailMessage mailMessage = new MailMessage()
                {
                    Subject = entry.Title,
                    IsBodyHtml = true,
                    Body = entry.Message + "<br />" + "<img src=\"cid:knb_email_logo.png\" height='50px'>",
                };
                mailMessage.Attachments.Add(GetLogo());
                mailMessage.To.Add(entry.EmailAddressTo);
                try
                {
                    string error = "";
                    if (emailer.SendEmail(mailMessage, out error))
                    {
                        entry.SendingTime = now;
                        Logger.Debug("Sending Email to " + entry.EmailAddressTo);
                        emailQueueRepo.Update(entry);
                    }
                    else
                    {
                        Logger.Error(error);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error("Fail to send email to " + entry.EmailAddressTo + ": " + e.ToString());
                }
            }
            Logger.Info("Force Email Sending Routine Finished");

            return Content("Success");
        }

        private Attachment GetLogo()
        {
            Attachment img = null;
            try
            {
                string logoPath = HttpRuntime.AppDomainAppPath + "Content\\knb_email_logo.png";
                img = new Attachment(logoPath);
                img.Name = "knb_email_logo.png";
                img.ContentId = "knb_email_logo.png";
            }
            catch
            {
            }
            return img;
        }
    }
}