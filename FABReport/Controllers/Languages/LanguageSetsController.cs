﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Fruitful.Import;
using System.Data;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class LanguageSetsController : GridController<LanguageSet, LanguageSetViewModel>
    {

        public LanguageSetsController()
            : base(new LanguageSetRepository(), new LanguageSetMapper())
        {

        }


        // GET: LanguageSets
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new LanguageSetViewModel());             
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddLanguageSet([DataSourceRequest] DataSourceRequest request, LanguageSetViewModel viewModel)
        {
            LanguageRepository languageRepo = new LanguageRepository();
            Language language = languageRepo.Read().Where(l => l.Name == viewModel.Language.Name && l.Label == viewModel.Language.Label).FirstOrDefault();
            
            if (language != null)
            {
                ViewBag.ErrorMessage = "Language already exists";
                return View(viewModel);
            }

            Language newLanguage = new Language()
            {
                Label = viewModel.Language.Label,
                Name = viewModel.Language.Name
            };
            languageRepo.Create(newLanguage);

            viewModel.Language.ID = newLanguage.ID;
            
            LanguageSet model = new LanguageSet();
            _mapper.MapToModel(viewModel, model);
            _repo.Create(model);

            return RedirectToAction("Index");
        }

        // GET: /Edit LanguageSet
        public ActionResult Edit(int ID)
        {
            try
            {
                LanguageSet ls = _repo.Read().Where(o => o.ID == ID).FirstOrDefault();
                LanguageSetViewModel model = _mapper.MapToViewModel(ls);
                return View(model);
            }
            catch
            {
                return View(new LanguageSetViewModel());
            }

        }


        // POST: /Edit LanguageSet
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LanguageSetViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            var model = _repo.Find(viewModel.ID);

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            model = _repo.Find(viewModel.ID);
            _mapper.MapToViewModel(model, viewModel);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return View(viewModel);

        }

        [HttpPost]
        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, LanguageSetViewModel viewModel)
        {
            // Delete language set
            var model = new LanguageSet { ID = viewModel.ID };
            _repo.Delete(model);

            // Delete email template
            var emailTemplateRepo = new ProjectEmailTemplateRepository();
            var emailTemplateList = emailTemplateRepo.Read().Where(et => et.LanguageID == viewModel.ID);
            emailTemplateRepo.Delete(emailTemplateList);

            // Delete Guidance
            var guidanceRepo = new ProjectGuidanceRepository();
            var guidancelist = guidanceRepo.Read().Where(pg => pg.LanguageID == viewModel.ID);
            guidanceRepo.Delete(guidancelist);

            // Delete Language
            var languageModel = new Language(){ ID = viewModel.Language.ID};
            new LanguageRepository().Delete(languageModel);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        // GET: LanguageImport
        public ActionResult Import()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Import(HttpPostedFileBase file)
        {
            const string relativePath = "/Imports/";

            const string column_LanguageName = "Language Name";
            const string column_LanguageLabel = "Language Label";
            const string column_Label_Reports = "Label (Reports)";
            const string column_Label_Refresh = "Label (Refresh)";
            const string column_Label_SearchPlaceholder = "Label (Search Placeholder)";
            const string column_Label_ReportName = "Label (Report Name)";
            const string column_Label_Type = "Label (Type)";
            const string column_Label_Uploaded = "Label (Uploaded)";
            const string column_Label_Hello = "Label (Hello)";
            const string column_Label_LogOff = "Label (Log Off)";
            const string column_Label_NotYet = "Label (Not Yet)";
            const string column_Label_NoReportsFound = "Label (No Reports Found)";

            if (file == null)
            {
                ViewBag.ErrorMessage = "No file has been uploaded.";
                return View();
            }

            string extension = Path.GetExtension(file.FileName);
            if (extension != ".xls" && extension != ".xlsx")
            {
                ViewBag.ErrorMessage = "Uploaded file is the wrong file format.";
                return View();
            }

            var physicalSavePath = SaveFile(file, relativePath);

            var data = ImportService.CopyExcelFileToTable(physicalSavePath, true);

            string error = "";
            string[] columns = new string[] { column_LanguageName, column_LanguageLabel, column_Label_Reports, column_Label_Refresh, column_Label_SearchPlaceholder, column_Label_ReportName, column_Label_Type, column_Label_Uploaded, column_Label_Hello, column_Label_LogOff, column_Label_NotYet, column_Label_NoReportsFound };

            if (!ImportService.IsTableValid(data, columns, out error))
            {
                ViewBag.ErrorMessage = error;
                return View();
            }

            var languageRepo = new LanguageRepository();
            var newLanguageSetList = new List<LanguageSet>();
            foreach (DataRow row in data.Rows)
            {
                if (string.IsNullOrWhiteSpace(row[column_LanguageName].ToString())) continue;

                string languageName = row[column_LanguageName].ToString();
                string languageLabel = row[column_LanguageLabel].ToString();
                string label_Reports = row[column_Label_Reports].ToString();
                string label_Refresh = row[column_Label_Refresh].ToString();
                string label_SearchPlaceholder = row[column_Label_SearchPlaceholder].ToString();
                string label_ReportName = row[column_Label_ReportName].ToString();
                string label_Type = row[column_Label_Type].ToString();
                string label_Uploade = row[column_Label_Uploaded].ToString();
                string label_Hello = row[column_Label_Hello].ToString();
                string label_LogOff = row[column_Label_LogOff].ToString();
                string label_NotYet = row[column_Label_NotYet].ToString();
                string label_NoReportsFound = row[column_Label_NoReportsFound].ToString();

                // Add new language
                var newLanguage = new Language()
                {
                    Name = languageName,
                    Label = languageLabel
                };
                languageRepo.Create(newLanguage);

                // Add new language set
                var newLanguageSet = new LanguageSet()
                {
                    LanguageID = newLanguage.ID,
                    Label_Hello = label_Hello,
                    Label_LogOff = label_LogOff,
                    Label_NoReportsFound = label_NoReportsFound,
                    Label_NotYet = label_NotYet,
                    Label_Refresh = label_Refresh,
                    Label_ReportName = label_ReportName,
                    Label_Reports = label_Reports,
                    Label_SearchPlaceholder = label_SearchPlaceholder,
                    Label_Type = label_Type,
                    Label_Uploaded = column_Label_Uploaded
                };
                newLanguageSetList.Add(newLanguageSet);
            }

            var languageSetRepo = new LanguageSetRepository();
            languageSetRepo.Create(newLanguageSetList);

            ViewBag.SuccessMessage = string.Format("{0} Language have been successfully imported.", data.Rows.Count);
            return View();
        }

    }
    
}