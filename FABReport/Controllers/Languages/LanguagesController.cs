﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KABReport.Controllers
{
    [Authorize]
    public class LanguagesController : GridController<Language, LanguageViewModel>
    {

        public LanguagesController()
            : base(new LanguageRepository(), new LanguageMapper())
        {

        }

        
        // GET: Opportunities
        public ActionResult Index()
        {
            return View();
        }
        
        // GET: /New Opportunity
        public ActionResult NewProject()
        {
            return View(new ProjectViewModel { Created = DateTime.Now  });             
        }


        // POST: /New Opportunity
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewProject(LanguageViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            
            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            return RedirectToAction("EditProject", new { ID = model.ID });           
        }



        // GET: /Edit Opportunity
        public ActionResult EditLanguage(int ID)
        {
            try
            {
                Language clt =  _repo.Read().Where(o => o.ID == ID).First();
                LanguageViewModel model = _mapper.MapToViewModel(clt);
                return View(model);
            }
            catch {
                return View("EditProject", new ProjectViewModel());
            }

        }


        // POST: /Edit Opportunity
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProject(LanguageViewModel viewModel, HttpPostedFileBase logoImg, string remove)
        {

            if (viewModel == null) return RedirectToAction("Index");

            var model = _repo.Find(viewModel.ID);

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            model = _repo.Find(viewModel.ID);
            _mapper.MapToViewModel(model, viewModel); 

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return View(viewModel);
            
        }

        private IQueryable<Language> GetLanguages()
        {
            var candidates = _repo.Read()
                               .OrderBy(c => c.Name)
                               .AsQueryable();

            return candidates;
        }


        [HttpPost]
        public ActionResult ChangeLanguage(string languageLabel)
        {
            LanguageRepository languageRepo = new LanguageRepository();

            Language language = languageRepo.Read().Where(l => l.Label == languageLabel).FirstOrDefault();

            if (language != null)
            {
                ProjectUserRepository projectUserRepo = new ProjectUserRepository();

                List<ProjectUser> userList = projectUserRepo.Read().Where(pu => pu.UserID == CurrentUser.Id).ToList();//.Each(pu => pu.DefaultLanguage = language.Name);
                foreach (ProjectUser entry in userList)
                {
                    entry.DefaultLanguage = language.Name;
                    projectUserRepo.Update(entry);
                }
            }

            return Redirect(Request.UrlReferrer.ToString());
        }

    }
    
}