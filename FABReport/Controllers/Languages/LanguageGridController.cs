﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class LanguageGridController : GridController<Language, LanguageViewModel>
    {

        public LanguageGridController()
            : base(new LanguageRepository(), new LanguageMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {            
            //User passed parameters oo filter results
            var candidates = _repo.Read().Where(
                         pro => 
                         (
                             (pro.Name.ToLower().Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null)
                         )
                     );

            candidates = ApplyQueryFiltersSort(request, candidates, "Name");

            var candidateList = candidates.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = candidateList.ToList(), Total = candidates.Count() };
            return Json(result);
        }



        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<LanguageViewModel>().ToDataSourceResult(request));
        }


    }
    
}