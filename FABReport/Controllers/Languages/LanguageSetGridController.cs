﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class LanguageSetGridController : GridController<LanguageSet, LanguageSetGridViewModel>
    {

        public LanguageSetGridController()
            : base(new LanguageSetRepository(), new LanguageSetGridMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {            
            //User passed parameters oo filter results
            var languageSets = _repo.Read().AsQueryable();

            languageSets = ApplyQueryFiltersSort(request, languageSets, "ID");

            var languageSetList = languageSets.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = languageSetList.ToList(), Total = languageSets.Count() };
            return Json(result);
        }



        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<LanguageSetGridViewModel>().ToDataSourceResult(request));
        }


    }
    
}