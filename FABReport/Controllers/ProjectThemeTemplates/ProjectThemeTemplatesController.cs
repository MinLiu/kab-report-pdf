﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectThemeTemplatesController : GridController<ProjectThemeTemplate, ProjectThemeTemplateViewModel>
    {
        public ProjectThemeTemplatesController()
            : base(new ProjectThemeTemplateRepository(), new ProjectThemeTemplateMapper())
        {

        }


        // GET: Projects
        public ActionResult Index()
        {
            return View();
        }

        #region New Project Theme Template

        // GET: /New Project
        public ActionResult New()
        {
            return View(new ProjectThemeTemplateViewModel()
            {
                PageBackgroundColor = "#424242",
                ButtonColor = "#F2F2F2",
                ButtonTextColor = "#6A6A6A",
                TreeViewBackgroundColor = "#4D4D4D",
                TreeViewTextColor = "#E631DB",
                ViewPanelBackgroundColor = "#282828",
                TitleAlign = "left",
                TitleColor = "#3771c8",
                TreeViewSelectedTextColor = "#ffffff",
                TreeViewSelectedBackgroundColor = "#3771c8",
            });             
        }


        // POST: /New Project
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(ProjectThemeTemplateViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            
            var model = _mapper.MapToModel(viewModel);
            model.Created = DateTime.Now;
            _repo.Create(model);

            return RedirectToAction("Edit", new { ID = model.ID });           
        }

        #endregion

        #region Edit Projects

        // GET: /Edit Project
        public ActionResult Edit(int ID)
        {
            var template =  _repo.Read()
                                 .Where(o => o.ID == ID)
                                 .First();
            var viewModel = _mapper.MapToViewModel(template);

            return View(viewModel);
        }

        // POST: /Edit Project
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectThemeTemplateViewModel viewModel, HttpPostedFileBase logoImg, string remove)
        {
            const string relativePath = "/Attachments/";

            if (viewModel == null) return RedirectToAction("Index");

            var model = _repo.Find(viewModel.ID);

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (!string.IsNullOrEmpty(remove))
            {
                try
                {
                    var physicalPath = Server.MapPath(viewModel.LogoPath);
                    if (System.IO.File.Exists(physicalPath))
                        System.IO.File.Delete(physicalPath);
                }
                catch { }
                viewModel.LogoPath = null;
                model.LogoPath = null;
            }
            //Save File if avaliable
            else if (logoImg != null)
            {
                if (logoImg.ContentLength > 10 * 1024 * 1024) { ViewBag.ErrorMessage = "Uploaded file is too big."; return View(viewModel); }
                var extension = Path.GetExtension(logoImg.FileName);
                if (extension != ".jpg" && extension != ".bmp" && extension != ".png")
                {
                    ViewBag.ErrorMessage = "Please upload image(jpg, bmp, png).";
                    return View(viewModel);
                }
                var str = Guid.NewGuid().ToString() + extension;
                var physicalSavePath = Server.MapPath(relativePath) + str;
                logoImg.SaveAs(physicalSavePath);
                viewModel.LogoPath = relativePath + str; //update logo url
                model.LogoPath = viewModel.LogoPath;
            }

            viewModel.LogoPath = model.LogoPath;

            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            model = _repo.Find(viewModel.ID);
            _mapper.MapToViewModel(model, viewModel); 

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return View(viewModel);
            
        }

        #endregion

    }
    
}