﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KABReport.App_Code;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectThemeTemplateGridController : GridController<ProjectThemeTemplate, ProjectThemeTemplateViewModel>
    {

        public ProjectThemeTemplateGridController()
            : base(new ProjectThemeTemplateRepository(), new ProjectThemeTemplateMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {
            //User passed parameters oo filter results
            var tpls = _repo.Read()
                            .Where(x => x.Deleted == false)
                            .Where(x => filterText.Equals("") || filterText.Equals(null) || x.Name.ToLower().Contains(filterText.ToLower()));

            tpls = ApplyQueryFiltersSort(request, tpls, "Name");

            var viewModels = tpls.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = viewModels.ToList(), Total = tpls.Count() };
            return Json(result);
        }
    }
    
}