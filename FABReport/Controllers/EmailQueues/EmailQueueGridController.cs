﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KABReport.App_Code;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class EmailQueueGridController : GridController<EmailQueue, EmailQueueViewModel>
    {

        public EmailQueueGridController()
            : base(new EmailQueueRepository(), new EmailQueueMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, int projectID, string projectUserID, string filterText, bool showUnSent)
        {
            var emails = _repo.Read()
                              .Where(x => x.ProjectUser.ProjectID == projectID)
                              .Where(x => projectUserID == null || projectUserID == "" || x.ProjectUserID == projectUserID)
                              .Where(x => filterText == null || filterText == "" || x.EmailAddressTo.ToLower().Contains(filterText.ToLower()) || x.Title.ToLower().Contains(filterText.ToLower()) || x.Message.ToLower().Contains(filterText.ToLower()))
                              .Where(x => showUnSent == false || x.SendingTime == null)
                              .OrderByDescending(x => x.ExpectedSendingTime).ThenBy(x => x.EmailAddressTo);

            var emailList = emails.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = emailList.ToList(), Total = emails.Count() };
            return Json(result);
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, EmailQueueViewModel viewModel)
        {
            return base.Update(request, viewModel);
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, EmailQueueViewModel viewModel)
        {
            return base.Destroy(request, viewModel);
        }

    }
    
}