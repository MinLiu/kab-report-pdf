﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KABReport.Controllers
{
    [Authorize]
    public class UserProjectsController : BaseController
    {

        public UserProjectsController()
            : base()
        {
        }


        // GET: 
        [HttpGet]
        public ActionResult Index(string company, string projectName)
        {
            company = Server.UrlDecode(company);
            projectName = Server.UrlDecode(projectName);

            string error;

            UserProjectPageViewModel viewModel = new UserProjectPageViewModel();
            if (!viewModel.SetUserProjectViewModel(company, projectName, CurrentUser, out error))
            {
                ViewBag.ErrorMessage = error;
                return View("Error");
            };

            ViewBag.LanguageSet = GetLanguageSet(viewModel.ProjectUser.DefaultLanguage);

            LogLogin(CurrentUser, viewModel.Project, viewModel.ProjectUser);

            return View(viewModel);

        }

        private LanguageSet GetLanguageSet(string language)
        { 
            LanguageSetRepository repo = new LanguageSetRepository();
            LanguageSet languageSet = repo.Read().Where(l => l.Language.Name == language).FirstOrDefault();
            if (languageSet == null)
                languageSet = repo.Read().OrderBy(l => l.ID).FirstOrDefault();

            return languageSet;
        }

        public JsonResult TreeViewHierarchies(int? id, int projectID)
        {
            var hierarchyrepo = new ProjectHierarchyRepository();
            var projectUser = new ProjectUserRepository().Read()
                                                         .Where(x => x.ProjectID == projectID)
                                                         .Where(x => x.UserID == CurrentUser.Id)
                                                         .FirstOrDefault();
            var allReports = new ProjectReportRepository().Read()
                                                          .Where(x => x.ProjectID == projectID)
                                                          .Where(x => x.Uploaded == true)
                                                          .ToList();
            if (!id.HasValue)
            {
                var hierarchyItemRepo = new ProjectUserHierarchyItemRepository();
                var Metanames = new ProjectUserHierarchyItemRepository().Read()
                                                                        .Where(x => x.ProjectID == projectID)
                                                                        .Where(x => x.UserID == CurrentUser.Id)
                                                                        .Select(x => x.Metaname)
                                                                        .ToList();
                var hierarchies = hierarchyrepo.Read()
                                               .Where(x => x.ProjectID == projectID)
                                               .Where(x => Metanames.Contains(x.Metaname))
                                               .OrderBy(x => x.StructureName)
                                               .ToList()
                                               .Select(x => new HierTreeViewItem()
                                               {
                                                   id = x.ID.ToString(),
                                                   Name = x.StructureName,
                                                   Metaname = x.Metaname,
                                                   hasChildren = projectUser.Role.ToLower() == "view down"? x.ChildrenHierarchies.Any() : false,
                                                   Count = allReports.Where(r => r.Metaname == x.Metaname).Count()
                                               })
                                               .ToList();

                //foreach (var hier in hierarchies)
                //{
                //    //hier.Name = string.Format("{0} ({1})", hier.Name, allReports.Where(x => x.Metaname == hier.Metaname).Count());
                //}

                hierarchies = hierarchies.Where(x => x.hasChildren == true || x.Count != 0).ToList();

                //hierarchies.Insert(0, new HierTreeViewItem() { id = "", Name = "All Assigned Reports", Metaname = "", hasChildren = false });

                return Json(hierarchies, JsonRequestBehavior.AllowGet);

            }
            else
            {
                var hierarchies = hierarchyrepo.Read()
                                               .Where(x => x.ProjectID == projectID)
                                               .Where(x => x.ParentHierarchyID == id)
                                               .OrderBy(x => x.StructureName)
                                               .ToList()
                                               .Select(x => new HierTreeViewItem()
                                               {
                                                   id = x.ID.ToString(),
                                                   Name = x.StructureName,
                                                   Metaname = x.Metaname,
                                                   hasChildren = projectUser.Role.ToLower() == "view down" ? x.ChildrenHierarchies.Any() : false,
                                                   Count = allReports.Where(r => r.Metaname == x.Metaname).Count()
                                               })
                                               .ToList();
                foreach (var hier in hierarchies)
                {
                    hier.Name = string.Format("{0} ({1})", hier.Name, allReports.Where(x => x.Metaname == hier.Metaname).Count());
                }

                hierarchies = hierarchies.Where(x => x.hasChildren == true || x.Count != 0).ToList();

                return Json(hierarchies, JsonRequestBehavior.AllowGet);
            }
        }

        public void LogLogin(User user, ProjectViewModel project, ProjectUserViewModel projectUser)
        {
            using (var context = new SnapDbContext())
            {
                context.ActivityLogs.Add(new ActivityLog()
                {
                    UserID = user.Id,
                    ProjectID = project.ID,
                    ProjectUserID = projectUser.ID,
                    Description = "Login",
                    IPAddress = Request.UserHostAddress,
                    Timestamp = DateTime.Now
                });
                context.SaveChanges();
            }
        }

        public ActionResult _PdfPreview(int id, string metaname)
        {
            ViewBag.ProjectID = id;
            ViewBag.Metaname = metaname;
            return PartialView();
        }

        public ActionResult _GeneratePDF(int id, string metaname, bool download = false)
        {
            //try
            //{
            //Get PDF generation ready.
            byte[] pdfBytes = null;
            string filename = "";
            pdfBytes = GetPDF(id, Server.UrlDecode(metaname), ref filename, out ProjectReport report);
            if (download)
            {
                Log(report, download);
                return File(pdfBytes, "application/pdf", filename + ".pdf");
            }
            else
            {
                Log(report, download);
                return File(pdfBytes, "application/pdf");
            }
            //}
            //catch
            //{
            //    var pdfBytes = PDFService<InvoicePDFViewModel>.GenerateErrorPage();
            //    return File(pdfBytes, "application/pdf");
            //}
        }

        private byte[] GetPDF(int id, string metaname, ref string filename, out ProjectReport report)
        {
            try
            {
                using (var context = new SnapDbContext())
                {
                    report = GetReport(id, metaname);
                    filename = report.ReportLabel;
                    return System.IO.File.ReadAllBytes(Server.MapPath(report.RelativePath));
                }
            }
            catch
            {
                report = null;
                filename = "NoFile";
                return System.IO.File.ReadAllBytes(Server.MapPath("/Attachments/NoFile.pdf"));
            }
        }

        private ProjectReport GetReport(int id, string metaname)
        {
            var repo = new ProjectReportRepository();
            var projectReports = repo.Read()
                                     .Where(pro => pro.ProjectID == id)
                                     .Where(pro => pro.Uploaded == true);

            var accessMetaNames = new List<string>();

            var cacheRepo = new UserReportCacheRepository();
            var reportCache = cacheRepo.Read()
                                       .Where(x => x.ProjectID == id && x.UserID == CurrentUser.Id)
                                       .Select(x => x.AccessMetaname);
            if (reportCache.Count() == 0)
            {
                var userMetanames = new ProjectUserHierarchyItemRepository().Read()
                                                                            .Where(puh => puh.ProjectID == id)
                                                                            .Where(puh => puh.UserID == CurrentUser.Id)
                                                                            .Select(puh => puh.Metaname)
                                                                            .ToList();
                ProjectUser projectUser = new ProjectUserRepository().Read()
                                                                     .Where(pu => pu.ProjectID == id)
                                                                     .Where(pu => pu.UserID == CurrentUser.Id)
                                                                     .FirstOrDefault();
                // Owners can look down
                if (projectUser.Role.ToLower() == "view down")
                {
                    var userHierarchies = new ProjectHierarchyRepository().Read()
                                                                          .Where(x => x.ProjectID == id)
                                                                          .Where(x => userMetanames.Contains(x.Metaname))
                                                                          .ToList();
                    var childrenHiers = new List<ProjectHierarchy>();
                    foreach (var userHierarchy in userHierarchies)
                    {
                        userHierarchy.GetChildrenHiers(childrenHiers);
                    }

                    accessMetaNames = childrenHiers.Distinct()
                                                   .Select(x => x.Metaname)
                                                   .ToList();
                }
                // Only has the the privilege of his own report
                else
                {
                    accessMetaNames = userMetanames;
                }

                var cacheToAdd = new List<UserReportCache>();
                foreach (var item in accessMetaNames)
                {
                    var toAdd = new UserReportCache()
                    {
                        AccessMetaname = item,
                        ProjectID = id,
                        UserID = CurrentUser.Id
                    };
                    cacheToAdd.Add(toAdd);
                }
                cacheRepo.Create(cacheToAdd);
            }
            else
            {
                accessMetaNames = reportCache.ToList();
            }

            var report = repo.Read()
                              .Where(x => x.ProjectID == id)
                              .Where(x => accessMetaNames.Contains(x.Metaname))
                              .Where(x => x.Uploaded == true)
                              .Where(x => metaname == "" || metaname == null || x.Metaname == metaname)
                              .FirstOrDefault();

            return report;
        }

        public void Log(ProjectReport projectReport, bool download)
        {
            if (projectReport == null) return;

            // Get this project user
            ProjectUser projectUser = new ProjectUserRepository().Read()
                                                                 .Where(pu => pu.ProjectID == projectReport.ProjectID)
                                                                 .Where(pu => pu.UserID == CurrentUser.Id)
                                                                 .FirstOrDefault();
            if (projectUser == null) return;

            // Log if user is not an admin or super admin
            using (var context = new SnapDbContext())
            {
                context.ActivityLogs.Add(new ActivityLog()
                {
                    UserID = CurrentUser.Id,
                    ProjectID = projectReport.ProjectID,
                    ProjectUserID = projectUser.ID,
                    IPAddress = Request.UserHostAddress,
                    Timestamp = DateTime.Now,
                    Description = String.Format("{0} File \"{1}\"", download? "Download" : "View", projectReport.ReportFile)
                });
                context.SaveChanges();
            }
        }

    }

}