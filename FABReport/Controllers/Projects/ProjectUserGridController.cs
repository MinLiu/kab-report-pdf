﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KABReport.App_Code;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectUserGridController : GuidGridController<ProjectUser, ProjectUserGridViewModel>
    {

        public ProjectUserGridController()
            : base(new ProjectUserRepository(), new ProjectUserGridMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request
                                    , int id
                                    , string filterText
                                    , IEnumerable<string> searchGrades
                                    , IEnumerable<string> searchDefaultLanguages
                                    , IEnumerable<string> searchAlignments
                                    , IEnumerable<string> searchDists
                                    , IEnumerable<string> searchRoles
                                    , IEnumerable<string> searchUserDefined1
                                    , IEnumerable<string> searchUserDefined2
                                    , IEnumerable<string> searchUserDefined3
                                    , IEnumerable<string> searchUserDefined4
                                    , IEnumerable<string> searchUserDefined5
                                    , IEnumerable<string> searchUserDefined6
                                    , IEnumerable<string> searchUserDefined7
                                    , IEnumerable<string> searchUserDefined8
                                    , IEnumerable<string> searchUserDefined9
                                    , IEnumerable<string> searchUserDefined10)
        {
            if (searchGrades == null) searchGrades = Enumerable.Empty<string>();
            if (searchDefaultLanguages == null) searchDefaultLanguages = Enumerable.Empty<string>();
            if (searchAlignments == null) searchAlignments = Enumerable.Empty<string>();
            if (searchDists == null) searchDists = Enumerable.Empty<string>();
            if (searchRoles == null) searchRoles = Enumerable.Empty<string>();
            if (searchUserDefined1 == null) searchUserDefined1 = Enumerable.Empty<string>();
            if (searchUserDefined2 == null) searchUserDefined2 = Enumerable.Empty<string>();
            if (searchUserDefined3 == null) searchUserDefined3 = Enumerable.Empty<string>();
            if (searchUserDefined4 == null) searchUserDefined4 = Enumerable.Empty<string>();
            if (searchUserDefined5 == null) searchUserDefined5 = Enumerable.Empty<string>();
            if (searchUserDefined6 == null) searchUserDefined6 = Enumerable.Empty<string>();
            if (searchUserDefined7 == null) searchUserDefined7 = Enumerable.Empty<string>();
            if (searchUserDefined8 == null) searchUserDefined8 = Enumerable.Empty<string>();
            if (searchUserDefined9 == null) searchUserDefined9 = Enumerable.Empty<string>();
            if (searchUserDefined10 == null) searchUserDefined10 = Enumerable.Empty<string>();
            //User passed parameters oo filter results
            var projectUsers = _repo.Read().Where(
                         pu =>
                         (
                             pu.ProjectID == id
                             && ((pu.ProjectUserHierarchyItem.Select(pi => pi.Metaname.ToString()).ToList().Contains(filterText.ToLower())
                             || pu.Email.ToLower().Contains(filterText.ToLower())
                             || pu.FirstName.ToLower().Contains(filterText.ToLower())
                             || pu.LastName.ToLower().Contains(filterText.ToLower())
                             || pu.Alignment.ToLower().Contains(filterText.ToLower())
                             || pu.Role.ToLower().Contains(filterText.ToLower())
                             || pu.Parent.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField1.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField2.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField3.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField4.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField5.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField6.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField7.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField8.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField9.ToLower().Contains(filterText.ToLower())
                             || pu.UserDefinedField10.ToLower().Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null))
                             && (searchGrades.Count() == 0 || searchGrades.Contains(pu.Grade))
                             && (searchDefaultLanguages.Count() == 0 || searchDefaultLanguages.Contains(pu.DefaultLanguage))
                             && (searchAlignments.Count() == 0 || searchAlignments.Contains(pu.Alignment))
                             && (searchDists.Count() == 0 || searchDists.Contains(pu.Dist))
                             && (searchRoles.Count() == 0 || searchRoles.Contains(pu.Role))
                             && (searchUserDefined1.Count() == 0 || searchUserDefined1.Contains(pu.UserDefinedField1))
                             && (searchUserDefined2.Count() == 0 || searchUserDefined2.Contains(pu.UserDefinedField2))
                             && (searchUserDefined3.Count() == 0 || searchUserDefined3.Contains(pu.UserDefinedField3))
                             && (searchUserDefined4.Count() == 0 || searchUserDefined4.Contains(pu.UserDefinedField4))
                             && (searchUserDefined5.Count() == 0 || searchUserDefined5.Contains(pu.UserDefinedField5))
                             && (searchUserDefined6.Count() == 0 || searchUserDefined6.Contains(pu.UserDefinedField6))
                             && (searchUserDefined7.Count() == 0 || searchUserDefined7.Contains(pu.UserDefinedField7))
                             && (searchUserDefined8.Count() == 0 || searchUserDefined8.Contains(pu.UserDefinedField8))
                             && (searchUserDefined9.Count() == 0 || searchUserDefined9.Contains(pu.UserDefinedField9))
                             && (searchUserDefined10.Count() == 0 || searchUserDefined10.Contains(pu.UserDefinedField10))
                         )
                     );
            if (request.Sorts.Count() == 0)
                projectUsers = projectUsers.OrderBy(pu => pu.ProjectUserHierarchyItem.OrderBy(puhi => puhi.Metaname).FirstOrDefault().Metaname);
            else
            {
                for (int i = request.Sorts.Count() - 1; i >= 0; i--)
                {
                    if (request.Sorts[i].Member == "MetanameIDs")
                    {
                        projectUsers = request.Sorts[i].SortDirection == System.ComponentModel.ListSortDirection.Ascending ? projectUsers.OrderBy(pu => pu.ProjectUserHierarchyItem.OrderBy(puhi => puhi.Metaname).FirstOrDefault().Metaname) 
                                                                                                                           : projectUsers.OrderByDescending(pu => pu.ProjectUserHierarchyItem.OrderBy(puhi => puhi.Metaname).FirstOrDefault().Metaname);
                    }
                    else if (request.Sorts[i].Member == "EmailExpectedSendingTime")
                    {
                        projectUsers = request.Sorts[i].SortDirection == System.ComponentModel.ListSortDirection.Ascending ? projectUsers.OrderBy(pu => pu.SentEmails.OrderByDescending(em => em.ID).FirstOrDefault().ExpectedSendingTime)
                                                                                                                           : projectUsers.OrderByDescending(pu => pu.SentEmails.OrderByDescending(em => em.ID).FirstOrDefault().ExpectedSendingTime);
                    }
                    else if (request.Sorts[i].Member == "EmailSendingTime")
                    {
                        projectUsers = request.Sorts[i].SortDirection == System.ComponentModel.ListSortDirection.Ascending ? projectUsers.OrderBy(pu => pu.SentEmails.OrderByDescending(em => em.ID).FirstOrDefault().SendingTime)
                                                                                                                           : projectUsers.OrderByDescending(pu => pu.SentEmails.OrderByDescending(em => em.ID).FirstOrDefault().SendingTime);
                    }
                    else if (request.Sorts[i].Member == "EmailsTotal")
                    {
                        projectUsers = request.Sorts[i].SortDirection == System.ComponentModel.ListSortDirection.Ascending ? projectUsers.OrderBy(pu => pu.SentEmails.Count)
                                                                                                                           : projectUsers.OrderByDescending(pu => pu.SentEmails.Count);
                    }
                    else if (request.Sorts[i].Member == "LastLoginTime")
                    {
                        projectUsers = request.Sorts[i].SortDirection == System.ComponentModel.ListSortDirection.Ascending ? projectUsers.OrderBy(pu => pu.ActivityLogs.Where(x => x.Description == "Login").OrderByDescending(x => x.Timestamp).FirstOrDefault().Timestamp)
                                                                                                                           : projectUsers.OrderByDescending(pu => pu.ActivityLogs.Where(x => x.Description == "Login").OrderByDescending(x => x.Timestamp).FirstOrDefault().Timestamp);
                    }
                    else
                    {
                        projectUsers = ApplyQueryFiltersSort(request, projectUsers, "ID ASC");
                    }
                }
            }

            var projectHierarchyList = projectUsers.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = projectHierarchyList.ToList(), Total = projectUsers.Count() };

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer() { MaxJsonLength = 2147483647 };

            return new ContentResult() { Content = serializer.Serialize(result), ContentType = "application/json" };

            //return Json(result);
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, ProjectUserGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Find the existing entity.
                var model = _repo.Find(viewModel.ID);

                // Map the view model to the model and update the database.
                _mapper.MapToModel(viewModel, model);

                _repo.Update(model);

                if (viewModel.Email != model.Email)
                {
                    // Update email queue
                    var emailQueueRepo = new EmailQueueRepository();
                    var emailQueue = emailQueueRepo.Read().Where(x => x.ProjectUserID == model.ID).ToList();
                    emailQueue.ForEach(x => x.EmailAddressTo = viewModel.Email);
                    emailQueueRepo.Update();

                    // Update all project users
                    var projectUsers = _repo.Read().Where(x => x.UserID == model.UserID).ToList();
                    projectUsers.ForEach(x => x.Email = viewModel.Email);
                    _repo.Update();

                    // Update User
                    var userRepo = new UserRepository();
                    var user = userRepo.Read().Where(x => x.Id == model.UserID).First();
                    user.Email = model.Email;
                    user.UserName = model.Email;
                    userRepo.Update(user);
                }

                if (viewModel.Password != "USING PREVIOUS PASSWORD" && viewModel.Password != model.Password)
                {
                    // Update all project users
                    var projectUsers = _repo.Read().Where(x => x.UserID == model.UserID).ToList();
                    projectUsers.ForEach(x => x.Password = viewModel.Password);
                    foreach (var projectUser in projectUsers)
                    _repo.Update();

                    // Update User
                    var userRepo = new UserRepository();
                    var user = userRepo.Read().Where(x => x.Id == model.UserID).First();
                    user.PasswordHash = UserManager.PasswordHasher.HashPassword(viewModel.Password);
                    userRepo.Update(user);
                }

                if (viewModel.Role != model.Role)
                {
                    model.Role = viewModel.Role;
                    _repo.Update(model);

                    // Update User report cache
                    var reportCacheRepo = new UserReportCacheRepository();
                    var reportCache = reportCacheRepo.Read().Where(x => x.ProjectID == model.ProjectID && x.UserID == model.UserID);
                    reportCacheRepo.Delete(reportCache);
                }

                //var oriMetanames = model.ProjectUserHierarchyItem.Select(pu => new MetanameItemViewModel { Name = pu.Metaname }).ToList();
                if (viewModel.MetanameIDs != null)
                {
                    // Delete Old hierarchies
                    var userHierarchyItemRepo = new ProjectUserHierarchyItemRepository();
                    userHierarchyItemRepo.Delete(userHierarchyItemRepo.Read().Where(x => x.ProjectUserID == model.ID));

                    // Create New Hierarchies
                    foreach (var metaname in viewModel.MetanameIDs)
                    {
                        userHierarchyItemRepo.Create(new ProjectUserHierarchyItem()
                        {
                            Metaname = metaname.Name,
                            ProjectID = model.ProjectID,
                            ProjectUserID = model.ID,
                            UserID = model.UserID
                        });
                    }

                    // Update User report cache
                    var reportCacheRepo = new UserReportCacheRepository();
                    var reportCache = reportCacheRepo.Read().Where(x => x.ProjectID == model.ProjectID && x.UserID == model.UserID);
                    reportCacheRepo.Delete(reportCache);
                }

            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, ProjectUserGridViewModel viewModel)
        {
            CommonFunc.DeltetProjectUser(viewModel.ID, out string error);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, ProjectUserGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Check if this pending project user is already in this project
                var isExist = _repo.Read().Where(x => x.ProjectID == viewModel.ProjectID && x.Email.ToLower() == viewModel.Email.ToLower()).Any();
                if (isExist)
                    return Json("Already Exists");

                // Map the view model to the model.
                var model = _mapper.MapToModel(viewModel);
                model.ID = Guid.NewGuid().ToString();
                model.Email = viewModel.Email;
                model.Role = viewModel.Role;
                model.ProjectID = viewModel.ProjectID;
                model.Password = viewModel.Password;

                // Check if there is a user account already
                var userRepo = new UserRepository();
                var userRoleRepo = new UserRoleRepository();
                var user = userRepo.Read().Where(x => x.UserName == model.Email).FirstOrDefault();
                if (user == null)
                {
                    // Create new user
                    user = new User()
                    {
                        Id = Guid.NewGuid().ToString(),
                        UserName = model.Email,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password),
                        SecurityStamp = Guid.NewGuid().ToString()
                    };
                    userRepo.Create(user);

                    IdentityUserRole userRole = new IdentityUserRole()
                    {
                        UserId = user.Id,
                        RoleId = "2"
                    };
                    userRoleRepo.Create(userRole);
                }
                else
                {
                    model.Password = "";
                }

                model.UserID = user.Id;

                // Add the new entity to the database.
                _repo.Create(model);

                // Create New Hierarchies
                var userHierarchyItemRepo = new ProjectUserHierarchyItemRepository();
                foreach (var metaname in viewModel.MetanameIDs)
                {
                    userHierarchyItemRepo.Create(new ProjectUserHierarchyItem()
                    {
                        Metaname = metaname.Name,
                        ProjectID = model.ProjectID,
                        ProjectUserID = model.ID,
                        UserID = model.UserID
                    });
                }

                // Update the ID of the view model to match the ID of the newly-created entity.
                viewModel.ID = model.ID;
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

    }
    
}