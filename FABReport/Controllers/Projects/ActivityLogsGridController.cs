﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KABReport.App_Code;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ActivityLogsGridController : BaseController
    {
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, int projectID, string projectUserID, string filterText)
        {
            var repo = new ActivityLogRepository();
            var logs = repo.Read().Where(x => x.ProjectID == projectID)
                                  .Where(x => projectUserID == null || projectUserID == "" || x.ProjectUserID == projectUserID)
                                  .Where(x => filterText == null || filterText == "" || x.ProjectUser.Email.ToLower().Contains(filterText.ToLower()) || x.Description.ToLower().Contains(filterText.ToLower()))
                                  .OrderByDescending(x => x.Timestamp)
                                  .Select(x => new ActivityLogViewModel()
                                  {
                                      Username = x.User.UserName,
                                      FirstName = x.ProjectUser.FirstName,
                                      LastName = x.ProjectUser.LastName,
                                      IPAddress = x.IPAddress,
                                      Timestamp = x.Timestamp,
                                      Description = x.Description
                                  });

            var results = logs.Skip((request.Page - 1) * request.PageSize).Take(request.PageSize);

            DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = logs.Count() };
            return Json(result);
        }
    }
    
}