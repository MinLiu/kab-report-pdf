﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace KABReport.Controllers
{
    [Authorize]
    public class ProjectLanguagesController : GridController<Language, LanguageViewModel>
    {

        public ProjectLanguagesController()
            : base(new LanguageRepository(), new LanguageMapper())
        {

        }


        public JsonResult DropDownLanguages()
        {
            var list = _repo.Read()
                .OrderBy(t => t.Name)
                .ToArray()
                .Select(t => _mapper.MapToViewModel(t))
                .ToList();
            return Json(list, JsonRequestBehavior.AllowGet);

        }

    }

}