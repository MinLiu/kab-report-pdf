﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectEmailTemplatesController : GridController<ProjectEmailTemplate, ProjectEmailTemplateViewModel>
    {

        public ProjectEmailTemplatesController()
            : base(new ProjectEmailTemplateRepository(), new ProjectEmailTemplateMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, int cID)
        {
            var list = _repo.Read().Where(p => p.ProjectID == cID)
                                    .OrderBy(o => o.Language.Name).ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ProjectEmailTemplateViewModel>().ToDataSourceResult(request));
        }

        [ValidateInput(false)]
        public override JsonResult Create(DataSourceRequest request, ProjectEmailTemplateViewModel viewModel)
        {
            return base.Create(request, viewModel);
        }

        [ValidateInput(false)]
        public override JsonResult Update(DataSourceRequest request, ProjectEmailTemplateViewModel viewModel)
        {
            return base.Update(request, viewModel);
        }

        [ValidateInput(false)]
        public override JsonResult Destroy(DataSourceRequest request, ProjectEmailTemplateViewModel viewModel)
        {
            return base.Destroy(request, viewModel);
        }
       
    }

}