﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Fruitful.Import;
using System.Data;
using System.Threading.Tasks;
using KABReport.App_Code;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectImportController : BaseController
    {

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadHierarchies(int id, HttpPostedFileBase hierarchyFile)
        {
            const string relativePath = "/Imports/";

            string error;

            #region Validation

            // Check if it is a valid project
            ProjectRepository projectRepo = new ProjectRepository();
            Project project = projectRepo.Find(id);
            if (project == null)
            {
                TempData["ErrorMessage"] = "Invalid operation";
                return View("Error");
            }

            if (hierarchyFile == null)
            {
                TempData["ErrorMessage"] = "No file has been uploaded.";
                //return View("~/Views/Projects/EditProject.cshtml", new ProjectMapper().MapToViewModel(project));
                return RedirectToAction("EditProject", "Projects", new { ID = id});
            }

            string extension = Path.GetExtension(hierarchyFile.FileName);
            if (extension != ".xls" && extension != ".xlsx")
            {
                TempData["ErrorMessage"] = "Uploaded file is the wrong file format.";
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            #endregion

            var physicalSavePath = SaveFile(hierarchyFile, relativePath);

            var data = ImportService.CopyExcelFileToTable(physicalSavePath, true);

            #region Delete the old data

            // Delete all the old project users
            if (!CommonFunc.DeltetProjectUsers(id, out error))
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            // Delete all the old project reports
            if (!CommonFunc.DeleteReports(id, out error))
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }
            // Delete all the old project hierarchies
            if (!CommonFunc.DeleteHierarchy(id, out error))
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            #endregion

            // Import Hierarchies
            int totalCount = ImportHierarchies(project.ID, data, out error);
            if (totalCount < 0)
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            // Update project
            project.HierarchyUploaded = true;
            project.ReportListUploaded = false;
            project.UserListUploaded = false;

            projectRepo.Update(project);


            TempData["SuccessMessage"] = string.Format("{0} hierarchies have been successfully imported.", totalCount);

            return RedirectToAction("EditProject", "Projects", new { ID = id });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadReportList(int id, HttpPostedFileBase reportListFile)
        {
            const string relativePath = "/Imports/";

            const string column_Metaname = "metaname";
            const string column_Type = "type";
            const string column_ReportFile = "report_file";
            const string column_ReportLabel = "report_label";

            // Check if is a valid project
            ProjectRepository projectRepo = new ProjectRepository();
            Project project = projectRepo.Find(id);

            #region Validation
            
            if (project == null)
            {
                ViewBag.errorMessage = "Invalid operation";
                return View("Error");
            }

            if (reportListFile == null)
            {
                TempData["ErrorMessage"] = "No file has been uploaded.";
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            string extension = Path.GetExtension(reportListFile.FileName);
            if (extension != ".xls" && extension != ".xlsx")
            {
                TempData["ErrorMessage"] = "Uploaded file is the wrong file format.";
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            #endregion

            var physicalSavePath = SaveFile(reportListFile, relativePath);

            var data = ImportService.CopyExcelFileToTable(physicalSavePath, true);

            string error = "";
            string[] columns = new string[] { column_Metaname, column_Type, column_ReportFile, column_ReportLabel };

            if (!ImportService.IsTableValid(data, columns, out error))
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            List<ProjectReport> projectReportToAdd = new List<ProjectReport>();

            // Delete all the old reportList
            if (!CommonFunc.DeleteReports(id, out error))
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            foreach (DataRow row in data.Rows)
            {
                if (string.IsNullOrWhiteSpace(row[column_Metaname].ToString())) continue;
                    
                var newReport = new ProjectReport()
                {
                    ProjectID = project.ID,
                    Uploaded = false,
                    Metaname = row[column_Metaname].ToString(),
                    Type = row[column_Type].ToString(),
                    ReportFile = row[column_ReportFile].ToString(),
                    ReportLabel = row[column_ReportLabel].ToString()
                };

                // Check if there more than 1 report under the same metaname
                if (projectReportToAdd.Where(x => x.Metaname == newReport.Metaname).Any())
                {
                    TempData["ErrorMessage"] = String.Format("There are 2 reports under '{0}'", newReport.Metaname);
                    return RedirectToAction("EditProject", "Projects", new { ID = id });
                }

                projectReportToAdd.Add(newReport);
            }

            // Add new reports
            ProjectReportRepository reportRepo = new ProjectReportRepository();
            reportRepo.Create(projectReportToAdd);

            // Update project
            project.ReportListUploaded = true;
            projectRepo.Update(project);

            // Create a Directory
            CreateReportDirectory(id);

            TempData["SuccessMessage"] = string.Format("{0} report records have been successfully imported.", projectReportToAdd.Count());

            return RedirectToAction("EditProject", "Projects", new { ID = id });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult UploadReports(int id, IEnumerable<HttpPostedFileBase> reportFiles)
        {
            string relativePath = "/Attachments/" + id + "/";

            // Check if is a valid project
            ProjectRepository projectRepo = new ProjectRepository();
            Project project = projectRepo.Find(id);
            if (project == null)
            {
                ViewBag.errorMessage = "Invalid operation";
                return View("Error");
            }

            if (reportFiles.FirstOrDefault() == null)
            {
                ViewBag.ErrorMessage = "No file has been uploaded.";
                return View("~/Views/Projects/ProjectReports.cshtml", new ProjectViewModel(){ ID = id });
            }
            
            int successful = 0;
            int failed = 0;

            var reportRepo = new ProjectReportRepository();

            List<string> failedFileList = new List<string>();
            foreach (HttpPostedFileBase file in reportFiles)
            {
                string fileName = Path.GetFileName(file.FileName);
                string extension = Path.GetExtension(file.FileName);
                //fileName = "First_Bus_Eastern_Counties_verbatim.xlsx";
                //extension = Path.GetExtension(fileName);


                ProjectReport projectReport = reportRepo.Read().Where(pr => pr.ProjectID == id && pr.ReportFile == fileName).FirstOrDefault();
                if (projectReport == null)
                {
                    failedFileList.Add(fileName);
                    failed++;
                    continue;
                }

                var directoryPath = Server.MapPath(relativePath);

                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
                var physicalSavePath = directoryPath + fileName;

                file.SaveAs(physicalSavePath);
                projectReport.FileSize = (Int32)(file.ContentLength / 1024);
                projectReport.UrlPath = "/FileDownload?rId=" + projectReport.ID;
                projectReport.RelativePath = relativePath + fileName;
                projectReport.Uploaded = true;

                successful++;
            }
            reportRepo.Update();

            ViewBag.SuccessMessage = string.Format("{0} reports have been successfully uploaded, {1} reports failed", successful, failed);
            if (failedFileList.Count() > 0)
            {
                //ViewBag.ErrorMessage += "Can't match File(s): " + string.Join(", ", failedFileList) + ".";
                return Json(new { result = "Fail", FileName = string.Join(", ", failedFileList) });
            }

            //return View("~/Views/Projects/ProjectReports.cshtml", new ProjectMapper().MapToViewModel(project));

            return Json(new { result = "Success" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadUserList(int id, HttpPostedFileBase userListFile)
        {
            System.Web.HttpContext.Current.Server.ScriptTimeout = 60 * 60;

            const string relativePath = "/Imports/";

            // Check if it is a valid project
            var projectRepo = new ProjectRepository();
            Project project = projectRepo.Find(id);

            #region Validation

            if (project == null)
            {
                ViewBag.ErrorMessage = "Invalid operation";
                return View("Error");
            }

            if (userListFile == null)
            {
                TempData["ErrorMessage"] = "No file has been uploaded.";
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            string extension = Path.GetExtension(userListFile.FileName);
            if (extension != ".xls" && extension != ".xlsx")
            {
                TempData["ErrorMessage"] = "Uploaded file is the wrong file format.";
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            #endregion

            var physicalSavePath = SaveFile(userListFile, relativePath);

            string error;
            // Delete all the old user List
            if (!CommonFunc.DeltetProjectUsers(id, out error))
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            // Import project users
            DataTable data = ImportService.CopyExcelFileToTable(physicalSavePath, false, true, "Userlist");
            int userCount = ImportUser(project.ID, data, out error);
            if (userCount < 0)
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            // Import project users with multi metaname
            data = ImportService.CopyExcelFileToTable(physicalSavePath, true, "Multi Metaname");
            bool success = ImportUserMultiMetaName(project.ID, data, out error);
            if (!success)
            {
                TempData["ErrorMessage"] = error;
                return RedirectToAction("EditProject", "Projects", new { ID = id });
            }

            // Update project
            project.UserListUploaded = true;
            projectRepo.Update(project);

            TempData["SuccessMessage"] = string.Format("{0} users have been successfully uploaded", userCount);
            return RedirectToAction("EditProject", "Projects", new { ID = id });
        }

        #region Helper

        #region Import Users

        private int ImportUser(int projectId, DataTable data, out string error)
        {
            error = "";

            const string column_Metaname = "Metaname";
            const string column_Email = "Email";
            const string column_Role = "Role";
            const string column_FirstName = "Firstname";
            const string column_LastName = "Name";
            const string column_Language = "u_language";
            const string column_NonQBLanguage = "Non-QB Language";
            const string column_Grade = "u_Grade";
            const string column_Alignment = "u_alignment";
            const string column_Dist = "u_Dist";
            const string column_Parent = "u_parent";
            const string column_Password = "Password";
            const string column_UseNewPassword = "UseNewPassword";
            List<ProjectCustomizedLabel> column_UserFields = new List<ProjectCustomizedLabel>();



            string[] columns = new string[] { column_Metaname, column_Email, column_Role, column_FirstName, column_LastName, column_Language, column_NonQBLanguage, column_Grade, column_Alignment, column_Dist, column_Parent, column_Password, column_UseNewPassword };//, column_UserField1, column_UserField2, column_UserField3, column_UserField4, column_UserField5, column_UserField6, column_UserField7, column_UserField8, column_UserField9, column_UserField10 };

            if (!ImportService.IsTableValid(data, columns, out error))
            {
                return -1;
            }

            List<User> userToAdd = new List<User>();
            List<IdentityUserRole> userRoleToAdd = new List<IdentityUserRole>();
            List<ProjectUser> projectUserToAdd = new List<ProjectUser>();
            List<ProjectUserHierarchyItem> projectUserhierarchyToAdd = new List<ProjectUserHierarchyItem>();

            UserRepository userRepo = new UserRepository();
            UserRoleRepository userRoleRepo = new UserRoleRepository();
            ProjectUserRepository projectUserRepo = new ProjectUserRepository();
            ProjectUserHierarchyItemRepository userHierItemRepo = new ProjectUserHierarchyItemRepository();

            // Get Header
            DataRow header = data.Rows[0];
            var customizedLabelRepo = new ProjectCustomizedLabelRepository();
            for (int i = 13; i < header.ItemArray.Length && i < 23; i++)
            {
                var customizedLabel = new ProjectCustomizedLabel()
                {
                    ProjectID = projectId,
                    Name = header[i].ToString(),
                    LabelIndex = "Field" + (i - 12)
                };
                column_UserFields.Add(customizedLabel);
            }
            customizedLabelRepo.Create(column_UserFields);
            data.Rows.RemoveAt(0);

            foreach (DataRow row in data.Rows)
            {
                if (string.IsNullOrWhiteSpace(row[column_Metaname].ToString())) continue;

                string metaname = row[column_Metaname].ToString();
                string email = row[column_Email].ToString();
                string role = row[column_Role].ToString();
                string firstName = row[column_FirstName].ToString();
                string lastName = row[column_LastName].ToString();
                string language = row[column_Language].ToString();
                string nonQBLanguage = row[column_NonQBLanguage].ToString();
                string grade = row[column_Grade].ToString();
                string alignment = row[column_Alignment].ToString();
                string dist = row[column_Dist].ToString();
                string parent = row[column_Parent].ToString();
                string password = row[column_Password].ToString();
                bool useNewPassword = row[column_UseNewPassword].ToString().ToLower().Replace(" ", string.Empty) == "Use This Password".ToLower().Replace(" ", string.Empty);
                string userField1 = column_UserFields.Count() >= 1 ? row[column_UserFields[0].Name].ToString() : "";
                string userField2 = column_UserFields.Count() >= 2 ? row[column_UserFields[1].Name].ToString() : "";
                string userField3 = column_UserFields.Count() >= 3 ? row[column_UserFields[2].Name].ToString() : "";
                string userField4 = column_UserFields.Count() >= 4 ? row[column_UserFields[3].Name].ToString() : "";
                string userField5 = column_UserFields.Count() >= 5 ? row[column_UserFields[4].Name].ToString() : "";
                string userField6 = column_UserFields.Count() >= 6 ? row[column_UserFields[5].Name].ToString() : "";
                string userField7 = column_UserFields.Count() >= 7 ? row[column_UserFields[6].Name].ToString() : "";
                string userField8 = column_UserFields.Count() >= 8 ? row[column_UserFields[7].Name].ToString() : "";
                string userField9 = column_UserFields.Count() >= 9 ? row[column_UserFields[8].Name].ToString() : "";
                string userField10 = column_UserFields.Count() >= 10 ? row[column_UserFields[9].Name].ToString() : "";

                User user = userRepo.Read().Where(u => u.Email == email).FirstOrDefault();

                bool useOldPassword = false;
                if (user == null)
                {
                    // Create a new user
                    user = new User()
                    {
                        Id = Guid.NewGuid().ToString(),
                        UserName = email,
                        Email = email,
                        FirstName = firstName,
                        LastName = lastName,
                        PasswordHash = UserManager.PasswordHasher.HashPassword(password),
                        //PlainPassword = password,
                        SecurityStamp = Guid.NewGuid().ToString()
                    };
                    userToAdd.Add(user);

                    IdentityUserRole userRole = new IdentityUserRole()
                    {
                        UserId = user.Id,
                        RoleId = "2"
                    };
                    userRoleToAdd.Add(userRole);
                }
                else
                {
                    // The user already exist
                    // Check if need to update password
                    if (useNewPassword)
                    {
                        user.PasswordHash = UserManager.PasswordHasher.HashPassword(password);
                        //user.PlainPassword = password;
                        userRepo.Update(user);
                        projectUserRepo.Read().Where(pu => pu.UserID == user.Id).Each(pu => pu.Password = password);
                        projectUserRepo.Update();
                    }
                    else
                    {
                        useOldPassword = true;
                    }
                }
                // Map to project hierarchy

                ProjectUser projectUser = projectUserToAdd.Where(pu => pu.Email == email).FirstOrDefault();
                if (projectUser == null)
                {
                    projectUser = new ProjectUser()
                    {
                        ID = Guid.NewGuid().ToString(),
                        ProjectID = projectId,
                        UserID = user.Id,
                        Email = email,
                        Role = role,
                        FirstName = firstName,
                        LastName = lastName,
                        DefaultLanguage = nonQBLanguage,
                        Grade = grade,
                        Alignment = alignment,
                        Dist = dist,
                        Parent = parent,
                        Password = useOldPassword? "" : password,
                        UserDefinedField1 = userField1,
                        UserDefinedField2 = userField2,
                        UserDefinedField3 = userField3,
                        UserDefinedField4 = userField4,
                        UserDefinedField5 = userField5,
                        UserDefinedField6 = userField6,
                        UserDefinedField7 = userField7,
                        UserDefinedField8 = userField8,
                        UserDefinedField9 = userField9,
                        UserDefinedField10 = userField10
                    };
                    projectUserToAdd.Add(projectUser);
                }

                var newProjectUserHierarchyItem = new ProjectUserHierarchyItem()
                {
                    Metaname = metaname,
                    ProjectID = projectUser.ProjectID,
                    UserID = projectUser.UserID,
                    ProjectUserID = projectUser.ID
                };
                projectUserhierarchyToAdd.Add(newProjectUserHierarchyItem);
            }
            userRepo.Create(userToAdd);
            userRoleRepo.Create(userRoleToAdd);
            projectUserRepo.Create(projectUserToAdd);
            userHierItemRepo.Create(projectUserhierarchyToAdd);

            return projectUserToAdd.Count();
        }

        #endregion

        #region Import Users With Multi-Metaname

        private bool ImportUserMultiMetaName(int projectId, DataTable data, out string error)
        {
            error = "";
            const string column_Email = "Email";
            const string column_Metaname = "Metaname";

            var projectUserRepo = new ProjectUserRepository();
            var userHierarchyItemRepo = new ProjectUserHierarchyItemRepository();
            List<ProjectUserHierarchyItem> projectUserhierarchyToAdd = new List<ProjectUserHierarchyItem>();
            foreach (DataRow row in data.Rows)
            {
                string metaname = row[column_Metaname].ToString();
                string email = row[column_Email].ToString();

                var projectUser = projectUserRepo.Read().Where(pu => pu.Email == email && pu.ProjectID == projectId).FirstOrDefault();
                if (projectUser != null)
                {
                    if (userHierarchyItemRepo.Read().Where(puhi => puhi.Metaname == metaname && puhi.ProjectID == projectId && puhi.ProjectUserID == projectUser.ID).Count() == 0)
                    {
                        var newItem = new ProjectUserHierarchyItem()
                        {
                            Metaname = metaname,
                            ProjectID = projectId,
                            UserID = projectUser.UserID,
                            ProjectUserID = projectUser.ID
                        };
                        projectUserhierarchyToAdd.Add(newItem);
                    }
                }
            }
            userHierarchyItemRepo.Create(projectUserhierarchyToAdd);
            return true;
        }

        #endregion

        #region Import Hierarchies

        private int ImportHierarchies(int projectId, DataTable data, out string error)
        {
            const string column_Metaname = "Metaname";
            const string column_LabelNameInternal = "Label name (internal)";
            const string column_LabelNameExternal = "Label name (external)";
            const string column_StructureLevel1 = "Structure level 1";
            const string column_StructureLevel2 = "Structure level 2";
            const string column_StructureLevel3 = "Structure level 3";
            const string column_StructureLevel4 = "Structure level 4";
            const string column_StructureLevel5 = "Structure level 5";
            const string column_StructureLevel6 = "Structure level 6";
            const string column_StructureLevel7 = "Structure level 7";
            const string column_StructureLevel8 = "Structure level 8";
            const string column_StructureLevel9 = "Structure level 9";
            const string column_StructureLevel10 = "Structure level 10";
            const string column_StructureLevel11 = "Structure level 11";
            const string column_StructureLevel12 = "Structure level 12";
            const string column_StructureLevel13 = "Structure level 13";
            const string column_StructureLevel14 = "Structure level 14";
            const string column_StructureLevel15 = "Structure level 15";
            const string column_StructureLevel16 = "Structure level 16";
            const string column_StructureLevel17 = "Structure level 17";
            const string column_StructureLevel18 = "Structure level 18";
            const string column_StructureLevel19 = "Structure level 19";
            const string column_StructureLevel20 = "Structure level 20";
            error = "";
            string[] columns = new string[] 
            {
                column_Metaname,
                column_LabelNameInternal,
                column_LabelNameExternal,
                column_StructureLevel1,
                column_StructureLevel2,
                column_StructureLevel3,
                column_StructureLevel4,
                column_StructureLevel5,
                column_StructureLevel6,
                column_StructureLevel7,
                column_StructureLevel8,
                column_StructureLevel9,
                column_StructureLevel10,
                column_StructureLevel11,
                column_StructureLevel12,
                column_StructureLevel13,
                column_StructureLevel14,
                column_StructureLevel15,
                column_StructureLevel16,
                column_StructureLevel17,
                column_StructureLevel18,
                column_StructureLevel19,
                column_StructureLevel20
            };

            if (!ImportService.IsTableValid(data, columns, out error))
            {
                return -1;
            }

            List<ProjectHierarchy> hierarchiesToAdd = new List<ProjectHierarchy>();

            foreach (DataRow row in data.Rows)
            {
                if (string.IsNullOrWhiteSpace(row[column_Metaname].ToString())) continue;

                var newHierarchy = new ProjectHierarchy();
                newHierarchy.ProjectID = projectId;
                newHierarchy.Metaname = row[column_Metaname].ToString();
                newHierarchy.InternalLabelName = row[column_LabelNameInternal].ToString();
                newHierarchy.ExternalLabelName = row[column_LabelNameExternal].ToString();

                for (int i = 1; i <= 20; i++)
                {
                    string structureName = row["Structure level " + i].ToString();
                    if (structureName != "")
                    {
                        newHierarchy.StructureName = structureName;
                        newHierarchy.Level = i;
                        break;
                    }
                }

                hierarchiesToAdd.Add(newHierarchy);

                // Add each structure
                for (int i = newHierarchy.Level; i > 0; i--)
                {
                    // Add new HierarchyStructureItem
                    var bossHierarchy = hierarchiesToAdd.Where(h => h.Level == i).LastOrDefault();
                    if (bossHierarchy != null)
                    {
                        if (string.IsNullOrEmpty(newHierarchy.ParentMetaname) && bossHierarchy != newHierarchy)
                        {
                            newHierarchy.ParentMetaname = bossHierarchy.Metaname;
                            newHierarchy.ParentHierarchyID = bossHierarchy.ID;
                        }
                    }
                }
                ProjectHierarchyRepository hierarchyRepo = new ProjectHierarchyRepository();
                hierarchyRepo.Create(newHierarchy);

            }




            // Add new hierarchies
            //ProjectHierarchyRepository hierarchyRepo = new ProjectHierarchyRepository();
            //hierarchyRepo.Create(hierarchiesToAdd);

            return hierarchiesToAdd.Count();
        }

        #endregion

        #region Create Report Directory

        private void CreateReportDirectory(int id)
        {
            string relativePath = "/Attachments/" + id + "/";
            var directoryPath = Server.MapPath(relativePath);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }

        #endregion

        #endregion

    }

}