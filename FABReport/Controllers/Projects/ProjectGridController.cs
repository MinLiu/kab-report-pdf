﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KABReport.App_Code;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectGridController : GridController<Project, ProjectViewModel>
    {

        public ProjectGridController()
            : base(new ProjectRepository(), new ProjectMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, string createdBy)
        {
            //User passed parameters oo filter results
            var projects = _repo.Read().Where(
                         pro => 
                         (
                             filterText.Equals("") || filterText.Equals(null) || pro.Name.ToLower().Contains(filterText.ToLower())|| pro.Client.ToLower().Contains(filterText.ToLower()) || pro.CreatedBy.Email.ToLower().Contains(filterText.ToLower())
                         ) && (createdBy == null || createdBy == "" || pro.CreatedByID.ToString() == createdBy)
                     );

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "CreatedBy") request.Sorts[x].Member = "CreatedBy.Email";
            }

            projects = ApplyQueryFiltersSort(request, projects, "Created DESC");

            var projectList = projects.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = projectList.ToList(), Total = projects.Count() };
            return Json(result);
        }



        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<ProjectViewModel>().ToDataSourceResult(request));
        }

        public override JsonResult Destroy(DataSourceRequest request, ProjectViewModel viewModel)
        {

            // Delete email templates
            ProjectEmailTemplateRepository emailtemplateRepo = new ProjectEmailTemplateRepository();
            IEnumerable<ProjectEmailTemplate> emailtemplateList = emailtemplateRepo.Read().Where(pet => pet.ProjectID == viewModel.ID);
            emailtemplateRepo.Delete(emailtemplateList);

            // Delete guidances
            ProjectGuidanceRepository guidanceRepo = new ProjectGuidanceRepository();
            IEnumerable<ProjectGuidance> guidanceList = guidanceRepo.Read().Where(g => g.ProjectID == viewModel.ID);
            guidanceRepo.Delete(guidanceList);

            string error;

            // Delete project users
            if (!CommonFunc.DeltetProjectUsers(viewModel.ID, out error))
            {
                return Json(error);
            }

            // Delete reports
            if (!CommonFunc.DeleteReports(viewModel.ID, out error))
            {
                return Json(error);
            }

            // Delete Hierarchies
            if (!CommonFunc.DeleteHierarchy(viewModel.ID, out error))
            {
                return Json(error);
            }

            var model = new Project { ID = viewModel.ID };
            _repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


    }
    
}