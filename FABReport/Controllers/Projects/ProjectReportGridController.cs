﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KABReport.Controllers
{
    [Authorize]
    public class ProjectReportGridController : GridController<ProjectReport, ProjectReportGridViewModel>
    {
        public ProjectReportGridController()
            : base(new ProjectReportRepository(), new ProjectReportGridMapper())
        {
            
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize(Roles = "Admin, Super Admin")]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, int id, IEnumerable<bool> statusList, string filterText, string type, string metaName)
        {
            if (statusList == null)
                statusList = new List<bool>();
            
            var projectReports = _repo.Read()
                                      .Where(pro => pro.ProjectID == id)
                                      .Where(pro => filterText.Equals("") || filterText.Equals(null) || pro.Metaname.ToLower().Contains(filterText.ToLower()) || pro.Type.ToLower().Contains(filterText.ToLower()) || pro.ReportFile.ToLower().Contains(filterText.ToLower()) || pro.ReportLabel.ToLower().Contains(filterText.ToLower()))
                                      .Where(pro => statusList.Contains(pro.Uploaded))
                                      .Where(pro => type == "" || pro.Type == type);

            projectReports = ApplyQueryFiltersSort(request, projectReports, "Metaname ASC");

            var projectReportList = projectReports.Skip(request.PageSize * (request.Page - 1))
                                                  .Take(request.PageSize)
                                                  .ToList()
                                                  .Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = projectReportList.ToList(), Total = projectReports.Count() };

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer() { MaxJsonLength = 2147483647 };

            return new ContentResult() { Content = serializer.Serialize(result), ContentType = "application/json" };

            //return Json(result);
        }

        public ActionResult UserRead([DataSourceRequest]DataSourceRequest request, int id, string filterText, string type, string metaname)
        {
            var projectReports = _repo.Read()
                                      .Where(pro => pro.ProjectID == id)
                                      .Where(pro => filterText.Equals("") || filterText.Equals(null) || pro.ReportLabel.ToLower().Contains(filterText.ToLower()))
                                      .Where(pro => pro.Uploaded == true)
                                      .Where(pro => type == "" || pro.Type == type);

            var accessMetaNames = new List<string>();

            var cacheRepo = new UserReportCacheRepository();
            var reportCache = cacheRepo.Read()
                                       .Where(x => x.ProjectID == id && x.UserID == CurrentUser.Id)
                                       .Select(x => x.AccessMetaname);
            if (reportCache.Count() == 0)
            {
                var userMetanames = new ProjectUserHierarchyItemRepository().Read()
                                                                            .Where(puh => puh.ProjectID == id)
                                                                            .Where(puh => puh.UserID == CurrentUser.Id)
                                                                            .Select(puh => puh.Metaname)
                                                                            .ToList();
                ProjectUser projectUser = new ProjectUserRepository().Read()
                                                                     .Where(pu => pu.ProjectID == id)
                                                                     .Where(pu => pu.UserID == CurrentUser.Id)
                                                                     .FirstOrDefault();
                // Owners can look down
                if (projectUser.Role.ToLower() == "view down")
                {
                    var userHierarchies = new ProjectHierarchyRepository().Read()
                                                                          .Where(x => x.ProjectID == id)
                                                                          .Where(x => userMetanames.Contains(x.Metaname))
                                                                          .ToList();
                    var childrenHiers = new List<ProjectHierarchy>();
                    foreach (var userHierarchy in userHierarchies)
                    {
                        userHierarchy.GetChildrenHiers(childrenHiers);
                    }

                    accessMetaNames = childrenHiers.Distinct()
                                                   .Select(x => x.Metaname)
                                                   .ToList();
                }
                // Only has the the privilege of his own report
                else
                {
                    accessMetaNames = userMetanames;
                }

                var cacheToAdd = new List<UserReportCache>();
                foreach (var item in accessMetaNames)
                {
                    var toAdd = new UserReportCache()
                    {
                        AccessMetaname = item,
                        ProjectID = id,
                        UserID = CurrentUser.Id
                    };
                    cacheToAdd.Add(toAdd);
                }
                cacheRepo.Create(cacheToAdd);
            }
            else
            {
                accessMetaNames = reportCache.ToList();
            }

            var reports = _repo.Read()
                               .Where(x => x.ProjectID == id)
                               .Where(x => accessMetaNames.Contains(x.Metaname))
                               .Where(x => filterText == "" || filterText == null || x.ReportLabel.ToLower().Contains(filterText.ToLower()))
                               .Where(x => x.Uploaded == true)
                               .Where(x => type == "" || type == null || x.Type == type)
                               .Where(x => metaname == "" || metaname == null || x.Metaname == metaname);

            // Filter the result
            reports = ApplyQueryFiltersSort(request, reports, "ID ASC");

            var projectReportList = reports.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = projectReportList.ToList(), Total = reports.Count() };

            return Json(result);
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<ProjectReportViewModel>().ToDataSourceResult(request));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, ProjectReportGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Check if there is more than 1 reports per metaname
                if (_repo.Read().Where(x => x.ProjectID == viewModel.ProjectID && x.Metaname == viewModel.Metaname).Any())
                {
                    ModelState.AddModelError("Duplicate", "No more than 1 report under the same metaname");
                    return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
                }

                // Map the view model to the model.
                var model = _mapper.MapToModel(viewModel);

                // Add the new entity to the database.
                _repo.Create(model);

                // Update the ID of the view model to match the ID of the newly-created entity.
                viewModel.ID = model.ID;
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, ProjectReportGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (_repo.Read().Where(x => x.ProjectID == viewModel.ProjectID && x.Metaname == viewModel.Metaname && x.ID != viewModel.ID).Any())
                {
                    ModelState.AddModelError("Duplicate", "No more than 1 report under the same metaname");
                    return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
                }

                var model = _repo.Find(viewModel.ID);

                _mapper.MapToModel(viewModel, model);
                _repo.Update(model);

            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, ProjectReportGridViewModel viewModel)
        {
            var model = new ProjectReport { ID = viewModel.ID };
            _repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult DropDownReportType()
        {
            var listType = _repo.Read().GroupBy(r => r.Type).Select(r => new { Value = r.Key, Text = r.Key });
            return Json(listType.ToList(), JsonRequestBehavior.AllowGet);
        }


    }
    
}