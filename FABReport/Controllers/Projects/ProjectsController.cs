﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectsController : GridController<Project, ProjectViewModel>
    {
        public ProjectsController()
            : base(new ProjectRepository(), new ProjectMapper())
        {

        }


        // GET: Projects
        public ActionResult Index()
        {
            ViewBag.CurrentUserID = CurrentUser.Id;
            return View();
        }

        #region New Project

        // GET: /New Project
        public ActionResult NewProject()
        {
            return View(new ProjectViewModel { Created = DateTime.Now, PageTitle = "Reports", ShowHeaderLogo = true, ShowTitleLogo = false });             
        }


        // POST: /New Project
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewProject(ProjectViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (_repo.Read().Where(p => p.Name == viewModel.Name).Count() > 0)
            {
                ViewBag.ErrorMessage = "The project name already exists.";
                return View(viewModel);
            }
            
            var model = _mapper.MapToModel(viewModel);
            model.HierarchyUploaded = false;
            model.ReportListUploaded = false;
            model.UserListUploaded = false;
            model.CreatedByID = CurrentUser.Id;
            model.Created = DateTime.Now;
            _repo.Create(model);

            return RedirectToAction("EditProject", new { ID = model.ID });           
        }

        #endregion

        #region Edit Projects

        // GET: /Edit Project
        public ActionResult EditProject(int ID)
        {
            Project clt =  _repo.Read()
                                .Where(o => o.ID == ID)
                                .First();
            ProjectViewModel model = _mapper.MapToViewModel(clt);

            // Get User Defined Label
            GetLabel(ID);

            return View(model);
        }

        // POST: /Edit Project
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProject(ProjectViewModel viewModel, HttpPostedFileBase logoImg, HttpPostedFileBase titleLogoImg, string remove, string removeTitleLogo)
        {
            const string relativePath = "/Attachments/";

            if (viewModel == null) return RedirectToAction("Index");

            var model = _repo.Find(viewModel.ID);

            // Get User Defined Label
            GetLabel(model.ID);

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (_repo.Read().Where(p => p.Name == viewModel.Name && p.ID != viewModel.ID).Count() > 0)
            {
                ViewBag.ErrorMessage = "The project name already exists.";
                return View(viewModel);
            }

            if (!string.IsNullOrEmpty(remove))
            {
                try
                {
                    var physicalPath = Server.MapPath(viewModel.LogoPath);
                    if (System.IO.File.Exists(physicalPath))
                        System.IO.File.Delete(physicalPath);
                }
                catch { }
                viewModel.LogoPath = null;
                model.LogoPath = null;
            }
            //Save File if avaliable
            else if (logoImg != null)
            {
                if (logoImg.ContentLength > 10 * 1024 * 1024) { ViewBag.ErrorMessage = "Uploaded file is too big."; return View(viewModel); }
                var extension = Path.GetExtension(logoImg.FileName);
                if (extension != ".jpg" && extension != ".bmp" && extension != ".png")
                {
                    ViewBag.ErrorMessage = "Please upload image(jpg, bmp, png).";
                    return View(viewModel);
                }
                var str = Guid.NewGuid().ToString() + extension;
                var physicalSavePath = Server.MapPath(relativePath) + str;
                logoImg.SaveAs(physicalSavePath);
                viewModel.LogoPath = relativePath + str; //update logo url
                model.LogoPath = viewModel.LogoPath;
            }

            if (!string.IsNullOrEmpty(removeTitleLogo))
            {
                try
                {
                    var physicalPath = Server.MapPath(viewModel.TitleLogoPath);
                    if (System.IO.File.Exists(physicalPath))
                        System.IO.File.Delete(physicalPath);
                }
                catch { }
                viewModel.TitleLogoPath = null;
                model.TitleLogoPath = null;
            }
            //Save File if avaliable
            else if (titleLogoImg != null)
            {
                if (titleLogoImg.ContentLength > 10 * 1024 * 1024) { ViewBag.ErrorMessage = "Uploaded file is too big."; return View(viewModel); }
                var extension = Path.GetExtension(titleLogoImg.FileName);
                if (extension != ".jpg" && extension != ".bmp" && extension != ".png")
                {
                    ViewBag.ErrorMessage = "Please upload image(jpg, bmp, png).";
                    return View(viewModel);
                }
                var str = Guid.NewGuid().ToString() + extension;
                var physicalSavePath = Server.MapPath(relativePath) + str;
                titleLogoImg.SaveAs(physicalSavePath);
                viewModel.TitleLogoPath = relativePath + str; //update logo url
                model.TitleLogoPath = viewModel.TitleLogoPath;
            }

            viewModel.LogoPath = model.LogoPath;
            viewModel.TitleLogoPath = model.TitleLogoPath;

            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            model = _repo.Find(viewModel.ID);
            _mapper.MapToViewModel(model, viewModel); 

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return View(viewModel);
            
        }

        #endregion

        #region Hierarchies

        public ActionResult ProjectHierarchies(ProjectViewModel model)
        {
            return View(model);
        }

        #endregion

        #region Project Users

        public ActionResult ProjectUsers(ProjectViewModel model)
        {
            var mapper = new ProjectCustomizedLabeltMapper();
            var customizedLabel = new ProjectCustomizedLabelRepository().Read().Where(pcl => pcl.ProjectID == model.ID).OrderBy(pcl => pcl.ID).ToList().Select(pcl => mapper.MapToViewModel(pcl));
            var labelCount = customizedLabel.Count();
            var label = new string[10];
            for (int i = 0; i < 10; i++)
            {
                label[i] = labelCount > i ? customizedLabel.ElementAt(i).Name : ""; 
            }
            ViewBag.Label = label;
            ViewBag.LabelCount = labelCount;
            return View(model);
        }

        #endregion

        #region Sending Emails

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EmailForm(ProjectViewModel model
                                    , bool sendToAll
                                    , string title
                                    , string message
                                    , string emailSendingTime
                                    , string idToSend
                                    , string conditionFilterText
                                    , string conditionDists
                                    , string conditionAlignments
                                    , string conditionGrades
                                    , string conditionDefaultLanguages
                                    , string conditionRoles
                                    , string conditionUserDefined1
                                    , string conditionUserDefined2
                                    , string conditionUserDefined3
                                    , string conditionUserDefined4
                                    , string conditionUserDefined5
                                    , string conditionUserDefined6
                                    , string conditionUserDefined7
                                    , string conditionUserDefined8
                                    , string conditionUserDefined9
                                    , string conditionUserDefined10)
        {
            // Repositories
            var projectUserRepo = new ProjectUserRepository();
            var projectRepo = new ProjectRepository();
            var emailQueueRepo = new EmailQueueRepository();
           
            message = Server.HtmlDecode(message);

            List<string> userIDs;
            // Check method
            if (sendToAll)
            {
                List<string> distList = string.IsNullOrEmpty(conditionDists) ? new List<string>() : conditionDists.Split(',').ToList();
                List<string> alignmentList = string.IsNullOrEmpty(conditionAlignments) ? new List<string>() : conditionAlignments.Split(',').ToList();
                List<string> gradeList = string.IsNullOrEmpty(conditionGrades) ? new List<string>() : conditionGrades.Split(',').ToList();
                List<string> defaultLanguageList = string.IsNullOrEmpty(conditionDefaultLanguages) ? new List<string>() : conditionDefaultLanguages.Split(',').ToList();
                List<string> roleList = string.IsNullOrEmpty(conditionRoles) ? new List<string>() : conditionRoles.Split(',').ToList();
                List<string> userDefined1 = string.IsNullOrEmpty(conditionUserDefined1) ? new List<string>() : conditionUserDefined1.Split(',').ToList();
                List<string> userDefined2 = string.IsNullOrEmpty(conditionUserDefined2) ? new List<string>() : conditionUserDefined2.Split(',').ToList();
                List<string> userDefined3 = string.IsNullOrEmpty(conditionUserDefined3) ? new List<string>() : conditionUserDefined3.Split(',').ToList();
                List<string> userDefined4 = string.IsNullOrEmpty(conditionUserDefined4) ? new List<string>() : conditionUserDefined4.Split(',').ToList();
                List<string> userDefined5 = string.IsNullOrEmpty(conditionUserDefined5) ? new List<string>() : conditionUserDefined5.Split(',').ToList();
                List<string> userDefined6 = string.IsNullOrEmpty(conditionUserDefined6) ? new List<string>() : conditionUserDefined6.Split(',').ToList();
                List<string> userDefined7 = string.IsNullOrEmpty(conditionUserDefined7) ? new List<string>() : conditionUserDefined7.Split(',').ToList();
                List<string> userDefined8 = string.IsNullOrEmpty(conditionUserDefined8) ? new List<string>() : conditionUserDefined8.Split(',').ToList();
                List<string> userDefined9 = string.IsNullOrEmpty(conditionUserDefined9) ? new List<string>() : conditionUserDefined9.Split(',').ToList();
                List<string> userDefined10 = string.IsNullOrEmpty(conditionUserDefined10) ? new List<string>() : conditionUserDefined10.Split(',').ToList();

                // use filter to specify users to send
                userIDs = projectUserRepo.Read().Where(pu => pu.ProjectID == model.ID
                                                        && ((pu.ProjectUserHierarchyItem.Select(pi => pi.Metaname.ToString()).ToList().Contains(conditionFilterText.ToLower())
                                                            || pu.Email.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.FirstName.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.LastName.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.Alignment.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.Dist.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.Parent.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField1.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField2.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField3.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField4.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField5.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField6.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField7.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField8.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField9.ToLower().Contains(conditionFilterText.ToLower())
                                                            || pu.UserDefinedField10.ToLower().Contains(conditionFilterText.ToLower())
                                                        ) || conditionFilterText.Equals("") || conditionFilterText.Equals(null))
                                                        && (distList.Count() == 0 || distList.Contains(pu.Dist))
                                                        && (alignmentList.Count() == 0 || alignmentList.Contains(pu.Alignment))
                                                        && (gradeList.Count() == 0 || gradeList.Contains(pu.Grade))
                                                        && (defaultLanguageList.Count() == 0 || defaultLanguageList.Contains(pu.DefaultLanguage))
                                                        && (roleList.Count() == 0 || roleList.Contains(pu.Role))
                                                        && (userDefined1.Count() == 0 || userDefined1.Contains(pu.UserDefinedField1))
                                                        && (userDefined2.Count() == 0 || userDefined2.Contains(pu.UserDefinedField2))
                                                        && (userDefined3.Count() == 0 || userDefined3.Contains(pu.UserDefinedField3))
                                                        && (userDefined4.Count() == 0 || userDefined4.Contains(pu.UserDefinedField4))
                                                        && (userDefined5.Count() == 0 || userDefined5.Contains(pu.UserDefinedField5))
                                                        && (userDefined6.Count() == 0 || userDefined6.Contains(pu.UserDefinedField6))
                                                        && (userDefined7.Count() == 0 || userDefined7.Contains(pu.UserDefinedField7))
                                                        && (userDefined8.Count() == 0 || userDefined8.Contains(pu.UserDefinedField8))
                                                        && (userDefined9.Count() == 0 || userDefined9.Contains(pu.UserDefinedField9))
                                                        && (userDefined10.Count() == 0 || userDefined10.Contains(pu.UserDefinedField10))
                                                        ).Select(pu => pu.UserID)
                                                        .ToList();

            }
            else
            {
                // use idToSend to get email 
                userIDs = idToSend.Split(',').ToList();
            }
            List<EmailQueue> emailToAdd = new List<EmailQueue>();
            foreach (string userID in userIDs)
            {
                try
                {
                    // Get user by user id
                    ProjectUser user = projectUserRepo.Read().Where(pu => pu.UserID == userID && pu.ProjectID == model.ID).FirstOrDefault();
                    // Get project by project id
                    Project project = projectRepo.Read().Where(p => p.ID == model.ID).FirstOrDefault();

                    if (string.IsNullOrEmpty(user.Email))
                        continue;

                    EmailQueue newEmail = new EmailQueue()
                    {
                        EmailAddressTo = user.Email,
                        Title = ReplaceEmailTemplate(title, project, user),
                        Message = ReplaceEmailTemplate(message, project, user),
                        ExpectedSendingTime = DateTime.Parse(emailSendingTime),
                        ProjectUserID = user.ID
                    };
                    emailToAdd.Add(newEmail);
                }
                catch(Exception e) 
                {   
                    Logger.Error(e.ToString());
                }
            }
            emailQueueRepo.Create(emailToAdd);
            return RedirectToAction("ProjectUsers", model);

        }

        private string ReplaceEmailTemplate(string template, Project project, ProjectUser user)
        {

            Dictionary<string, string> replacements = new Dictionary<string, string>();
            replacements.Add("{FIRSTNAME}", user.FirstName);
            replacements.Add("{LASTNAME}", user.LastName);
            replacements.Add("{PROJECTNAME}", project.Name);
            replacements.Add("{PROJECTURL}", "<a href=\"" + Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + "/UserProjects?company=" + Server.UrlEncode(project.Client) + "&projectName=" + Server.UrlEncode(project.Name) + "\">" + project.Name + "</a>");
            replacements.Add("{USERNAME}", user.Email);
            replacements.Add("{PASSWORD}", user.Password == "" ? "USING PREVIOUS PASSWORD" : user.Password);

            var customizeLabels = new ProjectCustomizedLabelRepository().Read().Where(pcl => pcl.ProjectID == project.ID);
            foreach (ProjectCustomizedLabel label in customizeLabels)
            {
                switch (label.LabelIndex)
                {
                    case "Field1":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField1);
                        break;
                    case "Field2":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField2);
                        break;
                    case "Field3":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField3);
                        break;
                    case "Field4":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField4);
                        break;
                    case "Field5":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField5);
                        break;
                    case "Field6":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField6);
                        break;
                    case "Field7":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField7);
                        break;
                    case "Field8":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField8);
                        break;
                    case "Field9":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField9);
                        break;
                    case "Field10":
                        replacements.Add("{" + label.Name.Replace(" ", string.Empty).ToUpper() + "}", user.UserDefinedField10);
                        break;
                }
                
            }

            foreach (KeyValuePair<string, string> entry in replacements)
            {
                template = template.Replace(entry.Key, entry.Value);
            }
            return template;
        }

        //private string AppendLogo(string message, Project project)
        //{
        //    StringBuilder str = new StringBuilder();
        //    str.AppendLine(message);
        //    str.AppendLine("<br />");
        //    str.AppendLine("<div style='background-color:#3c3c3b; width:250px'>&nbsp<img src='http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/Content/karian_and_box_logo_nav.svg" + "' height='50px'></div>");
        //    return str.ToString();
        //}

        #endregion

        #region Project Reports

        public ActionResult ProjectReports(ProjectViewModel model)
        {
            return View(model);
        }

        public ActionResult DeleteReportFile(int id) {
            var reportRepo = new ProjectReportRepository();
            var report = reportRepo.Find(id);

            if (report.Uploaded == true)
            {
                DeleteFile(report.RelativePath);
                report.RelativePath = null;
                report.UrlPath = null;
                report.Uploaded = false;
                report.FileSize = 0;
                reportRepo.Update(report);
            }

            return Json("Success");
        }

        public ActionResult ScanReports(int id) {

            string relativePath = "/Attachments/" + id + "/";
            var directoryPath = Server.MapPath(relativePath);

            var reportRepo = new ProjectReportRepository();

            var reportList = reportRepo.Read().Where(r => r.ProjectID == id).ToList();

            //var filesInDirectory = Directory.GetFiles(directoryPath).ToList();

            DirectoryInfo di = new DirectoryInfo(directoryPath);
            var fileInfoList = di.GetFiles().ToList();
            foreach (var report in reportList)
            {
                var fileInfo = fileInfoList.Where(f => f.Name == report.ReportFile).FirstOrDefault();
                if (fileInfo != null)
                {
                    report.Uploaded = true;
                    report.UrlPath = "/FileDownload?rId=" + report.ID;
                    report.RelativePath = relativePath + report.ReportFile;
                    report.FileSize = (int)(fileInfo.Length / 1024);
                }
                else
                {
                    report.Uploaded = false;
                    report.UrlPath = null;
                    report.RelativePath = null;
                    report.FileSize = 0;
                }
            }
            reportRepo.Update();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult _FTPInfo(int id) {
            return PartialView(new FTPInfoViewModel(id));
        }

        #endregion

        #region Helper

        private IQueryable<Project> GetProjects()
        {
            var projects = _repo.Read()
                               .OrderBy(c => c.Name)
                               .AsQueryable();

            return projects;
        }

        #endregion


        [HttpGet]
        public JsonResult DropDownProjects()
        {
            var IsSuperAdmin = User.IsInRole("Super Admin");
            var results = _repo.Read()
                               .Where(p => IsSuperAdmin || (!IsSuperAdmin && p.CreatedByID == CurrentUser.Id))
                               .Select(p => new DropDownListItem() { Value = p.ID.ToString(), Text = p.Client + " - " + p.Name })
                               .ToList()
                               .OrderBy(i => i.Text);

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        private void GetLabel(int id)
        {
            // Get Customized Labels
            var mapper = new ProjectCustomizedLabeltMapper();
            var customizedLabel = new ProjectCustomizedLabelRepository().Read().Where(pcl => pcl.ProjectID == id).OrderBy(pcl => pcl.ID).ToList().Select(pcl => mapper.MapToViewModel(pcl));
            var labelCount = customizedLabel.Count();
            var label = new string[10];
            for (int i = 0; i < 10; i++)
            {
                label[i] = labelCount > i ? customizedLabel.ElementAt(i).Name : "";
            }
            ViewBag.Label = label;
            ViewBag.LabelCount = labelCount;
        }

        public ActionResult _ActivityLogs(ProjectViewModel viewModel)
        {
            return PartialView(viewModel);
        }

        public ActionResult _EmailCampaign(ProjectViewModel viewModel)
        {
            return PartialView(viewModel);
        }

    }
    
}