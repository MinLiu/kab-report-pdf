﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectHierarchyGridController : GridController<ProjectHierarchy, ProjectHierarchyViewModel>
    {

        public ProjectHierarchyGridController()
            : base(new ProjectHierarchyRepository(), new ProjectHierarchyMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, int id, string filterText)
        {
            //User passed parameters oo filter results
            var projectHierarchies = _repo.Read().Where(
                         pro =>
                         (
                             pro.ProjectID == id
                             && ((pro.Metaname.ToString().Contains(filterText.ToLower())
                             || pro.InternalLabelName.ToLower().Contains(filterText.ToLower())
                             || pro.ExternalLabelName.ToLower().Contains(filterText.ToLower())
                             || pro.StructureName.ToLower().Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null))
                         )
                     );

            projectHierarchies = ApplyQueryFiltersSort(request, projectHierarchies, "Metaname ASC");

            var projectHierarchyList = projectHierarchies.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            DataSourceResult result = new DataSourceResult { Data = projectHierarchyList.ToList(), Total = projectHierarchies.Count() };
            return Json(result);
        }



        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<ProjectViewModel>().ToDataSourceResult(request));
        }


    }
    
}