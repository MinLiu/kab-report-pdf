﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using KABReport.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace KABReport.Controllers
{
    [Authorize(Roles = "Admin, Super Admin")]
    public class ProjectGuidancesController : GridController<ProjectGuidance, ProjectGuidanceViewModel>
    {

        public ProjectGuidancesController()
            : base(new ProjectGuidanceRepository(), new ProjectGuidanceMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, int cID)
        {
            var list = _repo.Read().Where(p => p.ProjectID == cID)
                                    .OrderBy(o => o.Language.Name).ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<ProjectGuidanceViewModel>().ToDataSourceResult(request));
        }

        [ValidateInput(false)]
        public override JsonResult Create(DataSourceRequest request, ProjectGuidanceViewModel viewModel)
        {
            return base.Create(request, viewModel);
        }

        [ValidateInput(false)]
        public override JsonResult Update(DataSourceRequest request, ProjectGuidanceViewModel viewModel)
        {
            return base.Update(request, viewModel);
        }

        [ValidateInput(false)]
        public override JsonResult Destroy(DataSourceRequest request, ProjectGuidanceViewModel viewModel)
        {
            return base.Destroy(request, viewModel);
        }
       
    }

}