﻿using KABReport.Models;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KABReport.Controllers
{
    public class DropdownsController : BaseController
    {
      
        public JsonResult DropDownUsers()
        {
            //Get the teams where the user is in     
            var users = SnapDbContext.GetUsers(CurrentUser)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersWithNotAssigned()
        {
            var users = SnapDbContext.GetUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Add(new UserItemViewModel { ID = "-1", Email = "Not Assigned" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownEventUsers()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetUsers(CurrentUser)
                                    .Where(u => u.Id != CurrentUser.Id)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersSearch()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            //users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersAll()
        {
            //Get the teams where the user is in  
            var userRepo = new UserRepository();

            var users = userRepo.Read()
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownProjectGrades(int id)
        {
            ProjectUserRepository pUserRepo = new ProjectUserRepository();
            IQueryable<DropDownListItem> list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.Grade).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownDefaultLanguages(int id)
        {
            ProjectUserRepository pUserRepo = new ProjectUserRepository();
            IQueryable<DropDownListItem> list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.DefaultLanguage).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownAlignments(int id)
        {
            ProjectUserRepository pUserRepo = new ProjectUserRepository();
            IQueryable<DropDownListItem> list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.Alignment).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownDists(int id)
        {
            ProjectUserRepository pUserRepo = new ProjectUserRepository();
            IQueryable<DropDownListItem> list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.Dist).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownProjectRoles(int id)
        {
            ProjectUserRepository pUserRepo = new ProjectUserRepository();
            IQueryable<DropDownListItem> list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.Role).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownProjectUserDefined(int id, int labelNo)
        {
            ProjectUserRepository pUserRepo = new ProjectUserRepository();
            IQueryable<DropDownListItem> list = null;
            switch (labelNo)
            {
                case 0:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField1).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 1:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField2).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 2:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField3).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 3:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField4).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 4:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField5).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 5:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField6).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 6:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField7).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 7:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField8).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 8:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField9).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
                case 9:
                    list = pUserRepo.Read().Where(pu => pu.ProjectID == id).GroupBy(pu => pu.UserDefinedField10).Select(pu => new DropDownListItem() { Text = pu.Key, Value = pu.Key });
                    break;
            }

            return Json(list.Where(g => g.Text != "").ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownEmailTemplates(int id)
        {
            ProjectEmailTemplateRepository emailTemplateRepo = new ProjectEmailTemplateRepository();
            ProjectEmailTemplateMapper mapper = new ProjectEmailTemplateMapper();
            List<ProjectEmailTemplateViewModel> list = emailTemplateRepo.Read()
                                                                        .Where(pu => pu.ProjectID == id)
                                                                        .ToList()
                                                                        .Select(et => mapper.MapToViewModel(et))
                                                                        .ToList();
            
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownMetanames(int projectID)
        {
            var projectHierarchyRepo = new ProjectHierarchyRepository();
            var results = projectHierarchyRepo.Read()
                                              .Where(x => x.ProjectID == projectID)
                                              .Select(x => new MetanameItemViewModel()
                                              {
                                                  Name = x.Metaname
                                              }).ToList();

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownProjectUsers(int projectID)
        {
            using (var context = new SnapDbContext())
            {
                var results = context.ProjectUsers
                                     .Where(x => x.ProjectID == projectID)
                                     .OrderBy(x => x.Email)
                                     .Select(x => new DropDownListItemViewModel
                                     {
                                         ID = x.ID,
                                         Name = x.Email
                                     }).ToList();
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownAdmins()
        {
            using (var context = new SnapDbContext())
            {
                var results = context.Users
                                     .Where(x => x.Roles.Select(r=> r.RoleId).Contains("1"))
                                     .OrderBy(x => x.Email)
                                     .Select(x => new DropDownListItemViewModel
                                     {
                                         ID = x.Id,
                                         Name = x.Email
                                     }).ToList();
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownThemes()
        {
            using (var context = new SnapDbContext())
            {
                var result = context.ProjectThemeTemplates
                                    .Where(x => x.Deleted == false)
                                    .Select(x => new DropDownListItemViewModel
                                    {
                                        ID = x.ID.ToString(),
                                        Name = x.Name
                                    }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

    }


}