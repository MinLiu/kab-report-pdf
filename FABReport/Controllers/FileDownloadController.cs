﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KABReport.Models;

namespace KABReport.Controllers
{
    [Authorize]
    public class FileDownloadController : BaseController
    {
        // GET: FileDownload
        public ActionResult Index(int rId)
        {

            ProjectReport projectReport = new ProjectReportRepository().Find(rId);

            if (projectReport == null)
            {
                ViewBag.ErrorMessage = "Invalid operation.";
                return View("Error");
            }

            // Check the user privillage
            if (!HasPrivilege(CurrentUser, projectReport))
            {
                ViewBag.ErrorMessage = "You can not access this file.";
                return View("Error");
            }

            if (projectReport.RelativePath == null)
            {
                ViewBag.ErrorMessage = "This file hasn't been uploaded or has been deleted.";
                return View("Error");
            }
            byte[] fileBytes = null;
            try
            {
                fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(projectReport.RelativePath));
            }
            catch(Exception e)
            {
                Logger.Error(e.ToString());
                ViewBag.ErrorMessage = "This file hasn't been uploaded or has been deleted.";
                return View("Error");
            }

            LogDownload(projectReport);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, projectReport.ReportLabel + "." + projectReport.Type);
        }

        private bool HasPrivilege(User currentUser, ProjectReport report)
        {
            if (UserManager.IsInRole(currentUser.Id, "Admin") || UserManager.IsInRole(currentUser.Id, "Super Admin"))
            {
                // Admin
                return true;
            }
            else
            { 
                // User
                // Get this project user
                ProjectUser projectUser = new ProjectUserRepository().Read()
                                                                     .Where(pu => pu.ProjectID == report.ProjectID)
                                                                     .Where(pu => pu.UserID == CurrentUser.Id)
                                                                     .FirstOrDefault();

                // Get user's hierarchy
                var userMetanames = new ProjectUserHierarchyItemRepository().Read()
                                                                            .Where(puh => puh.ProjectID == report.ProjectID)
                                                                            .Where(puh => puh.UserID == CurrentUser.Id)
                                                                            .Select(puh => puh.Metaname)
                                                                            .ToList();
                var accessMetanames = new List<string>();
                if (projectUser.Role.ToLower() == "view down")
                {
                    // Owners can look down
                    var userHierarchies = new ProjectHierarchyRepository().Read()
                                                                      .Where(x => x.ProjectID == report.ProjectID)
                                                                      .Where(x => userMetanames.Contains(x.Metaname))
                                                                      .ToList();
                    var childrenHiers = new List<ProjectHierarchy>();
                    foreach (var userHierarchy in userHierarchies)
                    {
                        userHierarchy.GetChildrenHiers(childrenHiers);
                    }

                    accessMetanames = childrenHiers.Distinct()
                                                   .Select(x => x.Metaname)
                                                   .ToList();
                }
                else
                {
                    accessMetanames = userMetanames;
                }
                if (accessMetanames.Contains(report.Metaname))
                    return true;
                return false;
            }
        }

        public void LogDownload(ProjectReport projectReport)
        {
            // Get this project user
            ProjectUser projectUser = new ProjectUserRepository().Read()
                                                                 .Where(pu => pu.ProjectID == projectReport.ProjectID)
                                                                 .Where(pu => pu.UserID == CurrentUser.Id)
                                                                 .FirstOrDefault();
            if (projectUser == null) return;

            // Log if user is not an admin or super admin
            using (var context = new SnapDbContext())
            {
                context.ActivityLogs.Add(new ActivityLog()
                {
                    UserID = CurrentUser.Id,
                    ProjectID = projectReport.ProjectID,
                    ProjectUserID = projectUser.ID,
                    IPAddress = Request.UserHostAddress,
                    Timestamp = DateTime.Now,
                    Description = String.Format("Download File \"{0}\"", projectReport.ReportFile)
                });
                context.SaveChanges();
            }
        }

    }
}