﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KABReport.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using System.Net.Mail;
using Fruitful.Email;
using log4net;


namespace KABReport.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleSendingEmail : IJob
    {
        public static int ThreshHold = new SnapDbContext().EmailSettings.First().Threshhold;
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        public void Execute(IJobExecutionContext context)
        {
            DateTime now = DateTime.Now;
            Logger.Info("Email Sending Routine Begins at " + now.ToString("dd/MM/yyyy HH:mm"));
            ProjectUserRepository projectUserRepo = new ProjectUserRepository();
            EmailQueueRepository emailQueueRepo = new EmailQueueRepository();
            Emailer emailer = new Emailer(new SnapDbContext());
            List<EmailQueue> emailsToSend = emailQueueRepo.Read().Where(eq => eq.SendingTime == null && eq.ExpectedSendingTime <= now).Take(ThreshHold).ToList();

            foreach (EmailQueue entry in emailsToSend)
            {
                MailMessage mailMessage = new MailMessage()
                {
                    Subject = entry.Title,
                    IsBodyHtml = true,
                    Body = entry.Message + "<br />" + "<img src=\"cid:knb_email_logo.png\" height='50px'>",
                };
                mailMessage.Attachments.Add(GetLogo());
                mailMessage.To.Add(entry.EmailAddressTo);
                try
                {
                    string error = "";
                    if (emailer.SendEmail(mailMessage, out error))
                    {
                        entry.SendingTime = now;
                        Logger.Debug("Sending Email to " + entry.EmailAddressTo);
                        emailQueueRepo.Update(entry);
                    }
                    else
                    {
                        Logger.Error(error);
                    }
                }
                catch (Exception e) 
                {
                    Logger.Error("Fail to send email to " + entry.EmailAddressTo + ": " + e.ToString());
                }
            }
            Logger.Info("Email Sending Routine Finished");
        }

        private Attachment GetLogo()
        {
            Attachment img = null;
            try
            {
                string logoPath = HttpRuntime.AppDomainAppPath + "Content\\knb_email_logo.png";
                img = new Attachment(logoPath);
                img.Name = "knb_email_logo.png";
                img.ContentId = "knb_email_logo.png";
            }
            catch
            {
            }
            return img;
        }
    }
}
