﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Quartz.Job;

namespace KABReport.Schedule
{
    public class Schedule
    {
        private static IScheduler sched;

        public static void ScheduleStart()
        {
            // Construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // Get a scheduler
            sched = schedFact.GetScheduler();
            sched.Start();

            // Define the job and tie it to the class
            IJobDetail job = JobBuilder.Create<ScheduleSendingEmail>()
                .WithIdentity("Job_SendingEmail")
                .Build();

            // Trigger the job to run now, and then every 15 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("Trigger_SendingEmail")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(EmailBatchingInterval())
                    .RepeatForever())
                .Build();

            // Define the job and tie it to the class
            IJobDetail job_CreateCache = JobBuilder.Create<ScheduleCreatingCache>()
                .WithIdentity("Job_CreatingCache")
                .Build();

            // Trigger the job to run now, and then every 15 seconds
            ITrigger trigger_CreateCache = TriggerBuilder.Create()
                .WithIdentity("Trigger_CreatingCache")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(1)
                    .RepeatForever())
                .Build();

            sched.ScheduleJob(job, trigger);
            sched.ScheduleJob(job_CreateCache, trigger_CreateCache);
        }

        public static void ChangeEmailBatchingInterval(int oldInterval, int newInterval)
        {
            if (oldInterval == newInterval) return;

            ITrigger oldTrigger = sched.GetTrigger(new TriggerKey("Trigger_SendingEmail"));

            TriggerBuilder tb = oldTrigger.GetTriggerBuilder();

            ITrigger newTrigger = tb.WithIdentity("Trigger_SendingEmail")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(newInterval)
                    .RepeatForever())
                .Build();

            sched.RescheduleJob(oldTrigger.Key, newTrigger);
        }

        private static int EmailBatchingInterval()
        {
            try
            {
                int interval = new Models.SnapDbContext().EmailSettings.First().Interval;
                return interval;
            }
            catch
            {
                return 15;
            }
            
        }
    }
}