﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KABReport.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using System.Net.Mail;
using Fruitful.Email;
using log4net;


namespace KABReport.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleCreatingCache : IJob
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Cache Creating Routine Begins");
            try
            {
                var projectUserList = new ProjectUserRepository().Read().ToList();

                var reportCacheRepo = new UserReportCacheRepository();
                foreach (var projectUser in projectUserList)
                {
                    var accessMetaNames = new List<string>();
                    var cache = reportCacheRepo.Read().Where(x => x.ProjectID == projectUser.ProjectID && x.UserID == projectUser.UserID);
                    if (cache.Count() == 0)
                    {
                        var userMetanames = new ProjectUserHierarchyItemRepository().Read()
                                                                                    .Where(puh => puh.ProjectID == projectUser.ProjectID)
                                                                                    .Where(puh => puh.UserID == projectUser.UserID)
                                                                                    .Select(puh => puh.Metaname)
                                                                                    .ToList();
                        // Owners can look down
                        if (projectUser.Role.ToLower() == "view down")
                        {
                            var userHierarchies = new ProjectHierarchyRepository().Read()
                                                                                  .Where(x => x.ProjectID == projectUser.ProjectID)
                                                                                  .Where(x => userMetanames.Contains(x.Metaname))
                                                                                  .ToList();
                            var childrenHiers = new List<ProjectHierarchy>();
                            foreach (var userHierarchy in userHierarchies)
                            {
                                userHierarchy.GetChildrenHiers(childrenHiers);
                            }

                            accessMetaNames = childrenHiers.Distinct()
                                                           .Select(x => x.Metaname)
                                                           .ToList();
                        }
                        // Only has the the privilege of his own report
                        else
                        {
                            accessMetaNames = userMetanames;
                        }

                        var cacheToAdd = new List<UserReportCache>();
                        foreach (var item in accessMetaNames)
                        {
                            var toAdd = new UserReportCache()
                            {
                                AccessMetaname = item,
                                ProjectID = projectUser.ProjectID,
                                UserID = projectUser.UserID
                            };
                            cacheToAdd.Add(toAdd);
                        }
                        reportCacheRepo.Create(cacheToAdd);
                        if (cacheToAdd.Count() > 0)
                        {
                            Logger.Info(String.Format("Creating Cache for '{0}'", projectUser.Email));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
            Logger.Info("Cache Creating Routine Finished");
        }
    }
}