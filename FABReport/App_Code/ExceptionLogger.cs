﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using log4net;
using System.Web.Mvc;
using System.IO;
using System.Web;
using KABReport.Models;

namespace KABReport.App_Code
{
    public class GlobalExceptionLogger : IExceptionFilter
    {
        private ILog log;

        public GlobalExceptionLogger()
        {
        }

        public virtual void OnException(System.Web.Mvc.ExceptionContext filterContext)
        {
            log = LogManager.GetLogger(filterContext.Controller.GetType());

            LogMessage message = new LogMessage()
            {
                Controller = filterContext.RouteData.Values["controller"].ToString(),
                Action = filterContext.RouteData.Values["action"].ToString(),
                Detail = filterContext.Exception.Message,
                FormData = filterContext.HttpContext.Request.Form.ToString(),
                IP = filterContext.HttpContext.Request.UserHostAddress,
                Method = filterContext.HttpContext.Request.HttpMethod,
                QueryString = filterContext.HttpContext.Request.QueryString.ToString(),
                StackTrace = filterContext.Exception.StackTrace,
                Url = filterContext.HttpContext.Request.Url.ToString(),
                User = filterContext.HttpContext.User.Identity.Name
            };

            log.Error(message);
        }

    }



}
