﻿using KABReport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


public static class Miscellaneous
{

    public static void CheckLoginMisuse(User User, ControllerContext Context, HttpRequestBase Request, HttpResponseBase Response)
    {
        if (User.Token != (Request.Cookies["GG"] ?? new HttpCookie("GG")).Value) {
            Context.HttpContext.GetOwinContext().Authentication.SignOut();
            //Controller.RedirectToAction("Index", "Home");
            Response.Redirect("/");
        } 
    }


    public static string Truncate(string source, int length)
    {
        if (source.Length > length)
        {
            source = source.Substring(0, length);
        }
        return source;
    }


}

