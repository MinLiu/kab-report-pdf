﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KABReport.Models;
using log4net;

namespace KABReport.App_Code
{
    public class CommonFunc
    {
        private static ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        public static bool DeleteHierarchy(int projectID, out string error)
        {
            error = "";
            try
            {
                // Delete Hierarchies
                ProjectHierarchyRepository hierarchyRepo = new ProjectHierarchyRepository();
                IEnumerable<ProjectHierarchy> hierarchyList = hierarchyRepo.Read().Where(ph => ph.ProjectID == projectID);
                hierarchyRepo.Delete(hierarchyList);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Fail to Delete Hierarchies, Error: " + e.ToString());
                error += e.ToString();
                return false;
            }
        }

        public static bool DeleteReports(int projectID, out string error)
        {
            error = "";
            try
            {
                // Delete Reports
                ProjectReportRepository _repo = new ProjectReportRepository();
                IEnumerable<ProjectReport> reportList = _repo.Read().Where(pr => pr.ProjectID == projectID);

                foreach (ProjectReport entry in reportList)
                {
                    if (entry.Uploaded == true)
                    {
                        try
                        {
                            var physicalPath = System.Web.HttpContext.Current.Server.MapPath(entry.RelativePath);
                            if (System.IO.File.Exists(physicalPath))
                                System.IO.File.Delete(physicalPath);

                        }
                        catch(Exception e) 
                        {
                            Logger.Error(string.Format("Fail to Delete Report (relative path :{0}), Error: {1}", entry.RelativePath, e.ToString()));
                        }
                    }
                }
                _repo.Delete(reportList);

                return DeleteReportFolder(projectID, out error);
            }
            catch (Exception e)
            {
                Logger.Error("Fail to Delete Reports, Error: " + e.ToString());
                error += e.ToString();
                return false;
            }
        }

        public static bool DeltetProjectUsers(int projectID, out string error)
        {
            error = "";
            try
            {
                // Delete Email Queue
                EmailQueueRepository emailQueueRepo = new EmailQueueRepository();
                IEnumerable<EmailQueue> emailList = emailQueueRepo.Read().Where(e => e.ProjectUser.ProjectID == projectID);
                emailQueueRepo.Delete(emailList);

                // Delete project user hierarchies
                ProjectUserHierarchyItemRepository userHierarchyRepo = new ProjectUserHierarchyItemRepository();
                IEnumerable<ProjectUserHierarchyItem> userHierarchyList = userHierarchyRepo.Read().Where(puhi => puhi.ProjectID == projectID);
                userHierarchyRepo.Delete(userHierarchyList);

                // Delete project user activity logs
                ActivityLogRepository activityLogRepo = new ActivityLogRepository();
                IEnumerable<ActivityLog> activityLogList = activityLogRepo.Read().Where(x => x.ProjectID == projectID);
                activityLogRepo.Delete(activityLogList);

                // Delete project users
                ProjectUserRepository projectUserRepo = new ProjectUserRepository();
                IEnumerable<ProjectUser> projectUserList = projectUserRepo.Read().Where(pu => pu.ProjectID == projectID);
                projectUserRepo.Delete(projectUserList);

                // Delete Customized Labels
                ProjectCustomizedLabelRepository labelRepo = new ProjectCustomizedLabelRepository();
                IEnumerable<ProjectCustomizedLabel> labelList = labelRepo.Read().Where(pcl => pcl.ProjectID == projectID);
                labelRepo.Delete(labelList);

                // Delete Report Cache
                UserReportCacheRepository cacheRepo = new UserReportCacheRepository();
                IEnumerable<UserReportCache> cacheList = cacheRepo.Read().Where(x => x.ProjectID == projectID);
                cacheRepo.Delete(cacheList);

                // Delete Users which are not in other projects
                UserRoleRepository userRoleRepo = new UserRoleRepository();
                UserRepository userRepo = new UserRepository();
                var adminRoleID = new RoleRepository().Read().Where(r => r.Name == "Admin").First().Id;
                var superAdminRoleID = new RoleRepository().Read().Where(r => r.Name == "Super Admin").First().Id;
                IEnumerable<User> userList = userRepo.Read().Where(u => u.Roles.Where(r => r.RoleId == adminRoleID || r.RoleId == superAdminRoleID).Count() == 0 && u.ProjectUsers.Count() == 0).ToList();
                var userIDList = userList.Select(u => u.Id.ToString());
                var userRoleList = userRoleRepo.Read().Where(ur => userIDList.Contains(ur.UserId));

                cacheList = cacheRepo.Read().Where(x => userIDList.Contains(x.UserID)).ToList();
                cacheRepo.Delete(cacheList);

                userRoleRepo.Delete(userRoleList);
                userRepo.Delete(userList);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Fail to Delete ProjectUsers, Error: " + e.ToString());
                error += e.ToString();
                return false;
            }
        }

        public static bool DeltetProjectUser(string puID, out string error)
        {
            error = "";
            try
            {
                // Delete Email Queue
                EmailQueueRepository emailQueueRepo = new EmailQueueRepository();
                IEnumerable<EmailQueue> emailList = emailQueueRepo.Read().Where(e => e.ProjectUser.ID == puID);
                emailQueueRepo.Delete(emailList);

                // Delete project user hierarchies
                ProjectUserHierarchyItemRepository userHierarchyRepo = new ProjectUserHierarchyItemRepository();
                IEnumerable<ProjectUserHierarchyItem> userHierarchyList = userHierarchyRepo.Read().Where(puhi => puhi.ProjectUserID == puID);
                userHierarchyRepo.Delete(userHierarchyList);

                // Delete project user activity logs
                ActivityLogRepository activityLogRepo = new ActivityLogRepository();
                IEnumerable<ActivityLog> activityLogList = activityLogRepo.Read().Where(x => x.ProjectUserID == puID);
                activityLogRepo.Delete(activityLogList);

                // Delete project users
                ProjectUserRepository projectUserRepo = new ProjectUserRepository();
                var projectUser = projectUserRepo.Read().Where(pu => pu.ID == puID).FirstOrDefault();
                projectUserRepo.Delete(projectUser);

                // Delete Report Cache
                UserReportCacheRepository cacheRepo = new UserReportCacheRepository();
                IEnumerable<UserReportCache> cacheList = cacheRepo.Read().Where(x => x.ProjectID == projectUser.ProjectID && x.UserID == projectUser.UserID);
                cacheRepo.Delete(cacheList);

                // Delete Users which are not in other projects
                UserRoleRepository userRoleRepo = new UserRoleRepository();
                UserRepository userRepo = new UserRepository();
                var adminRoleID = new RoleRepository().Read().Where(r => r.Name == "Admin").First().Id;
                var superAdminRoleID = new RoleRepository().Read().Where(r => r.Name == "Super Admin").First().Id;
                IEnumerable<User> userList = userRepo.Read().Where(u => u.Roles.Where(r => r.RoleId == adminRoleID || r.RoleId == superAdminRoleID).Count() == 0 && u.ProjectUsers.Count() == 0).ToList();
                var userIDList = userList.Select(u => u.Id.ToString());
                var userRoleList = userRoleRepo.Read().Where(ur => userIDList.Contains(ur.UserId));

                cacheList = cacheRepo.Read().Where(x => userIDList.Contains(x.UserID)).ToList();
                cacheRepo.Delete(cacheList);

                userRoleRepo.Delete(userRoleList);
                userRepo.Delete(userList);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Fail to Delete ProjectUsers, Error: " + e.ToString());
                error += e.ToString();
                return false;
            }
        }

        public static bool DeleteReportFolder(int projectID, out string error)
        {
            error = "";
            try
            {
                string relativePath = "/Attachments/" + projectID + "/";
                var directoryPath = System.Web.HttpContext.Current.Server.MapPath(relativePath);
                if (System.IO.Directory.Exists(directoryPath))
                {
                    System.IO.Directory.Delete(directoryPath, true);
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Fail to Delete Project Folder, Error: " + e.ToString());
                error += e.ToString();
                return false;
            }
        }
    }
}