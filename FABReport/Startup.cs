﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KABReport.Startup))]
namespace KABReport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Schedule.Schedule.ScheduleStart();
        }
    }
}
