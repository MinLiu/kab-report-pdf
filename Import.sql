
/*
UPDATE Candidates
SET Candidates.Email = All_Candidates.[CV Link]
FROM Candidates  INNER JOIN All_Candidates ON Candidates.ID = All_Candidates.ID WHERE Candidates.ID > 42545413

*/


DECLARE @LoopCounter int , @MaxID int, @NewID int
SELECT @LoopCounter = min(ID), @MaxID = max(ID)  FROM All_Candidates -- WHERE ID >= 42545414
 

DECLARE @Sector nvarchar(MAX)

WHILE ( @LoopCounter IS NOT NULL AND  @LoopCounter <= @MaxID)
BEGIN

	--INSERT INTO CandidateRoles(Name)
	SELECT TOP 1 @Sector = [Candidate's Sector Speciality] FROM All_Candidates WHERE ID = @LoopCounter

	INSERT INTO CandidateSectorItems (CandidateID, CandidateSectorID )
	SELECT @LoopCounter, CandidateSectors.ID FROM SplitCSV_Codes(@Sector) split
	INNER JOIN CandidateSectors ON  REPLACE(LOWER(split.ID), ' ', '') LIKE REPLACE(LOWER(CandidateSectors.Name), ' ', '')

	SELECT @LoopCounter  = min(ID) FROM All_Candidates WHERE ID > @LoopCounter


END



/*
DECLARE @LoopCounter int , @MaxID int, @NewID int
SELECT @LoopCounter = min(ID), @MaxID = max(ID)  FROM All_Candidates -- WHERE ID >= 42545414
 

DECLARE @Sector nvarchar(MAX)

WHILE ( @LoopCounter IS NOT NULL AND  @LoopCounter <= @MaxID)
BEGIN

	--INSERT INTO CandidateRoles(Name)
	SELECT TOP 1 @Sector = [Candidate's Sector Speciality] FROM All_Candidates WHERE ID = @LoopCounter

	INSERT INTO CandidateSectors (Name )
	SELECT ID FROM SplitCSV_Codes(@Sector) split WHERE REPLACE(LOWER(split.ID), ' ', '') NOT IN (SELECT REPLACE(LOWER(Name), ' ', '') FROM CandidateSectors)
	
	SELECT @LoopCounter  = min(ID) FROM All_Candidates WHERE ID > @LoopCounter

END
*/