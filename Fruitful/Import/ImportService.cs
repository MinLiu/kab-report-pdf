﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;

namespace Fruitful.Import
{

    public static class ImportService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="deleteFile"></param>
        /// <param name="sheetNo"> First: 1, Second: 2, etc... Default = 1</param>
        /// <returns></returns>
        //public static DataTable CopyExcelFileToTable(string filePath, bool deleteFile, int sheetNo = 1)
        //{
        //    string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 12.0;HDR={1}'";
        //    var table = new DataTable();

        //    using (var conn = new OleDbConnection(connString))
        //    {
        //        conn.Open();
        //        try
        //        {
        //            var firstSheetName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[sheetNo - 1]["TABLE_NAME"].ToString();

        //            using (var adapter = new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", conn))
        //            {
        //                adapter.Fill(table);
        //            }
        //        }
        //        catch { }
        //        finally
        //        {
        //            conn.Close();
        //        }
        //    }

        //    // Delete the file from the server to free up space if required.
        //    if (deleteFile)
        //    {
        //        try { File.Delete(filePath); }
        //        catch { }
        //    }

        //    return table;
        //}

        public static bool IsTableValid(DataTable table, string[] columns, out string message)
        {
            bool valid = true;
            message = String.Empty;
            var missingColumns = new List<string>();

            // Check all columns exist and are labelled correctly. 
            foreach (var label in columns)
            {
                if (!table.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).Contains(label.ToLower()))
                {
                    valid = false;
                    missingColumns.Add(label);
                }
            }

            if (!valid)
            {
                message = String.Format("Import file is missing the following columns: '{0}'", String.Join(",", missingColumns));
                return false;
            }

            return valid;
        }

        public static DataTable CopyExcelFileToTable(string filePath, bool deleteFile)
        {
            return CopyExcelFileToTable(filePath, deleteFile, false, null);
        }

        public static DataTable CopyExcelFileToTable(string filePath, bool deleteFile, string sheetName)
        {
            return CopyExcelFileToTable(filePath, deleteFile, false, sheetName);
        }

        public static DataTable CopyExcelFileToTable(string filePath, bool deleteFile, bool keepHeader, string sheetName = null)
        {
            DataTable dt = new DataTable();

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filePath, false))
            {

                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                string relationshipId = sheetName == null? spreadSheetDocument.WorkbookPart.Workbook.Descendants<Sheet>().First().Id.Value : spreadSheetDocument.WorkbookPart.Workbook.Descendants<Sheet>().First(s => s.GetAttribute("name", "").Value.ToLower() == sheetName.ToLower()).Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                }

                foreach (Row row in rows) // this will also include the header row...
                {
                    DataRow tempRow = dt.NewRow();
                    int columnIndex = 0;
                    foreach (Cell cell in row.Descendants<Cell>())
                    {
                        // Gets the column index of the cell withdata
                        int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                        cellColumnIndex--; // zero based index
                        if (columnIndex < cellColumnIndex)
                        {
                            do
                            {
                                try
                                {
                                    tempRow[columnIndex] = ""; // Insert blank data
                                }
                                catch { }
                                columnIndex++;
                            } while (columnIndex < cellColumnIndex);
                        }
                        try
                        {
                            tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);
                        }
                        catch { }

                        columnIndex++;
                    }

                    dt.Rows.Add(tempRow);
                }

            }
            if (!keepHeader)
            {
                dt.Rows.RemoveAt(0); // Delete the header row
            }

            if (deleteFile)
            {
                try { File.Delete(filePath); }
                catch { }
            }
            return dt;
        }

        private static string GetCellValue(SpreadsheetDocument doc, Cell cell)
        {
            try
            {
                string value = cell.CellValue.InnerText;
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
                }
                return value;
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// Given a cell name, parses the specified cell to get the column name.
        /// </summary>
        /// <param name="cellReference">Address of the cell (ie. B2)</param>
        /// <returns>Column Name (ie. B)</returns>
        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }
        /// <summary>
        /// Given just the column name (no row index), it will return the zero based column index.
        /// Note: This method will only handle columns with a length of up to two (ie. A to Z and AA to ZZ). 
        /// A length of three can be implemented when needed.
        /// </summary>
        /// <param name="columnName">Column Name (ie. A or AB)</param>
        /// <returns>Zero based index if the conversion was successful; otherwise null</returns>
        public static int? GetColumnIndexFromName(string columnName)
        {

            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }
    }
}


