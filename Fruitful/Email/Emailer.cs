﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Fruitful.Email
{
    public class Emailer
    {
        private readonly EmailSetting _settings;

        public Emailer(DbContext db)
        {
            _settings = db.Set<EmailSetting>().First();
        }

        public Emailer(EmailSetting settings)
        {
            _settings = settings;
        }

        public bool SendEmail(string email, string subject, string body, out string error)
        {
            return SendEmail(email, subject, body, Enumerable.Empty<Attachment>(), out error);
        }

        public bool SendEmail(string email, string subject, string body, IEnumerable<Attachment> attachments, out string error)
        {
            try
            {
                var mailMessage = CreateMailMessage(email, subject, body, attachments);
                if (SendEmail(mailMessage, out error))
                {
                    return true;
                }
                return false;
            }
            catch(Exception e)
            {
                error = e.ToString();
                return false;
            }
        }

        public bool SendEmail(MailMessage message, out string error)
        {
            try
            {
                var client = GetClient();

                if (SendEmail(client, message, out error))
                {
                    return true;
                }
                return false;
            }
            catch(Exception e)
            {
                error = e.ToString();
                return false;
            }
        }

        public bool SendEmail(SmtpClient client, MailMessage message, out string error)
        {
            try
            {
                error = "";
                message.From = new MailAddress(_settings.Email);
                client.Send(message);
                return true;
            }
            catch(Exception e)
            {
                error = e.ToString();
                return false;
            }
        }

        public MailMessage CreateMailMessage(string email, string subject, string body, IEnumerable<Attachment> attachments)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(email);
            mailMessage.From = new MailAddress(_settings.Email);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;

            foreach (var attachment in attachments)
            {
                mailMessage.Attachments.Add(attachment);
            }

            return mailMessage;
        }

        public SmtpClient GetClient()
        {
            var host = _settings.Host;
            var port = _settings.Port;
            var userName = _settings.Email;
            var password = _settings.Password;
            var enableSsl = _settings.EnableSSL;

            var client = new SmtpClient(host, port)
            {
                Credentials = new NetworkCredential(userName, password),
                EnableSsl = enableSsl
            };

            return client;
        }
    }
}
