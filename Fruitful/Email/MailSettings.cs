﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruitful.Email
{

    public class EmailSetting
    {
        [Key]
        public Guid ID { get; set; }
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool EnableSSL { get; set; }
        [Display(Name = "Threshhold (Maximum emails per batch)")]
        public int Threshhold { get; set; }
        [Display(Name = "Interval (Seconds)")]
        public int Interval { get; set; }
    }
}
