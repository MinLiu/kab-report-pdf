﻿using EvoPdf;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fruitful.PDF
{

    public static class PDFService<Model>
    {

        public static byte[] GenerateOrder(Model model, ControllerContext controllerContext, PDFTemplate template)
        {
            return GenerateOrder(model, controllerContext, template.headerView, template.bodyView, template.footerView, template.pagerView, template.showHeaderEveryPage, template.showFooterEveryPage);
        }

        public static byte[] GenerateOrder(Model model, ControllerContext controllerContext, string headerView, string bodyView, string footerView, string pagerView, bool showHeaderEveryPage, bool showFooterEveryPage)
        {
            NReco.PdfGenerator.HtmlToPdfConverter generator = new NReco.PdfGenerator.HtmlToPdfConverter();
            generator.Orientation = NReco.PdfGenerator.PageOrientation.Portrait;
            var ViewData = new ViewDataDictionary();
            var TempData = new TempDataDictionary();

            ViewData.Model = model;

            string headerHtml = "";
            string bodyHtml = "";
            string footerHtml = "";
            string pagerHtml = "";

            if ((headerView ?? "") != "")
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult headerResult = ViewEngines.Engines.FindPartialView(controllerContext, headerView);
                    headerResult.View.Render(new ViewContext(controllerContext, headerResult.View, ViewData, TempData, sw), sw);
                    headerHtml = sw.GetStringBuilder().ToString();
                }
            }

            if ((bodyView ?? "") != "")
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult bodyResult = ViewEngines.Engines.FindPartialView(controllerContext, bodyView);
                    bodyResult.View.Render(new ViewContext(controllerContext, bodyResult.View, ViewData, TempData, sw), sw);
                    bodyHtml = sw.GetStringBuilder().ToString();
                }
            }

            if ((footerView ?? "") != "")
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult footerResult = ViewEngines.Engines.FindPartialView(controllerContext, footerView);
                    footerResult.View.Render(new ViewContext(controllerContext, footerResult.View, ViewData, TempData, sw), sw);
                    footerHtml = sw.GetStringBuilder().ToString();
                }
            }

            if ((pagerView ?? "") != "")
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult pagerResult = ViewEngines.Engines.FindPartialView(controllerContext, pagerView);
                    pagerResult.View.Render(new ViewContext(controllerContext, pagerResult.View, ViewData, TempData, sw), sw);
                    pagerHtml = sw.GetStringBuilder().ToString();
                }
            }

            
            //Add Header based on Quote Preview Settings
            if (showHeaderEveryPage) generator.PageHeaderHtml = headerHtml;
            else bodyHtml = headerHtml + bodyHtml;

            //Add Footer based on Quote Preview Settings
            if (showFooterEveryPage) generator.PageFooterHtml = footerHtml;
            else bodyHtml = bodyHtml + footerHtml;
            
            
            /*
            // Use the current page URL as base URL
            string baseUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            // Convert the current page HTML string a PDF document in a memory buffer
            byte[] outPdfBuffer = new EvoPDFGenerator().GeneratePDF(headerHtml, bodyHtml, footerHtml, baseUrl);
            

            return outPdfBuffer;
             */

            
            //Add Pager
            if (generator.PageFooterHtml != null)
                generator.PageFooterHtml = generator.PageFooterHtml.Replace("position:fixed;", "position:initial;") + pagerHtml;
            else
                generator.PageFooterHtml = pagerHtml;

            //Set margins 
            var margins = new NReco.PdfGenerator.PageMargins();
            if (!showHeaderEveryPage) margins.Top = 15;
            if (!showFooterEveryPage) margins.Bottom = 20;

            //Because the PDF generator is weird, there is a footer but no header then add extra breaks.
            if (!showHeaderEveryPage && showFooterEveryPage)
                generator.PageHeaderHtml = "<br /><br />";


            margins.Left = 10;
            margins.Right = 10;

            generator.Margins = margins;
            return generator.GeneratePdf(bodyHtml);

        }



        public static byte[] GeneratePages(Model model, ControllerContext controllerContext, PDFTemplate [] templates)
        {
            
            PdfDocument outputDocument = new PdfDocument();

            foreach (var temp in templates)
            {
                MemoryStream pdfStream = new MemoryStream();
                var pdfArray = GenerateOrder(model, controllerContext, temp);

                pdfStream.Write(pdfArray, 0, pdfArray.Length);
                pdfStream.Seek(0, System.IO.SeekOrigin.Begin);

                PdfSharp.Pdf.PdfDocument document = PdfSharp.Pdf.IO.PdfReader.Open(pdfStream, PdfDocumentOpenMode.Import);

                foreach (PdfSharp.Pdf.PdfPage page in document.Pages)
                {
                    outputDocument.AddPage(page);
                }
            }

            using (MemoryStream stream = new MemoryStream())
            {
                outputDocument.Save(stream, true);
                return stream.ToArray();
            }
            
        }

        public static byte[] GenerateErrorPage()
        {
            NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
            return pdfConverter.GeneratePdf(String.Format("<h1>Error: Can not generate PDF</h1>"));
        }


        

    }


    public class EvoPDFGenerator
    {
        private HtmlToPdfConverter htmlToPdfConverter;

        public EvoPDFGenerator() {
            htmlToPdfConverter = new HtmlToPdfConverter();            
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = 10;
        }



        public byte[] GeneratePDF(string headerHtml, string bodyHtml, string footerHtml, string baseUrl)
        {
            if(!string.IsNullOrEmpty(headerHtml)) 
            {
                htmlToPdfConverter.PdfDocumentOptions.ShowHeader = true;
                HtmlToPdfElement headerHtmlElement = new HtmlToPdfElement(headerHtml, baseUrl);
                headerHtmlElement.NavigationCompletedEvent += new NavigationCompletedDelegate(headerHtml_NavigationCompletedEvent);
                htmlToPdfConverter.PdfHeaderOptions.AddElement(headerHtmlElement);
            }


            if(!string.IsNullOrEmpty(footerHtml))
            {
                htmlToPdfConverter.PdfDocumentOptions.ShowFooter = true;
                HtmlToPdfElement footerHtmlElement = new HtmlToPdfElement(footerHtml, baseUrl);
                footerHtmlElement.NavigationCompletedEvent += new NavigationCompletedDelegate(footerHtml_NavigationCompletedEvent);
                htmlToPdfConverter.PdfFooterOptions.AddElement(footerHtmlElement);
            }

            htmlToPdfConverter.PrepareRenderPdfPageEvent += new PrepareRenderPdfPageDelegate(htmlToPdfConverter_PrepareRenderPdfPageEvent);

            return htmlToPdfConverter.ConvertHtml(bodyHtml, baseUrl);

        }



        void headerHtml_NavigationCompletedEvent(NavigationCompletedParams eventParams)
        {
            // Get the header HTML width and height from event parameters
            float headerHtmlWidth = eventParams.HtmlContentWidthPt;
            float headerHtmlHeight = eventParams.HtmlContentHeightPt;

            // Calculate the header width from coverter settings
            float headerWidth = htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Width - htmlToPdfConverter.PdfDocumentOptions.LeftMargin -
                        htmlToPdfConverter.PdfDocumentOptions.RightMargin;

            // Calculate a resize factor to fit the header width
            float resizeFactor = 1;
            if (headerHtmlWidth > headerWidth)
                resizeFactor = headerWidth / headerHtmlWidth;

            // Calculate the header height to preserve the HTML aspect ratio
            float headerHeight = headerHtmlHeight * resizeFactor;

            if (!(headerHeight < htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Height - htmlToPdfConverter.PdfDocumentOptions.TopMargin -
                        htmlToPdfConverter.PdfDocumentOptions.BottomMargin))
            {
                throw new Exception("The header height cannot be bigger than PDF page height");
            }

            // Set the calculated header height
            htmlToPdfConverter.PdfDocumentOptions.DocumentObject.Header.Height = headerHeight;
        }


        void footerHtml_NavigationCompletedEvent(NavigationCompletedParams eventParams)
        {
            // Get the footer HTML width and height from event parameters
            float footerHtmlWidth = eventParams.HtmlContentWidthPt;
            float footerHtmlHeight = eventParams.HtmlContentHeightPt;

            // Calculate the footer width from coverter settings
            float footerWidth = htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Width - htmlToPdfConverter.PdfDocumentOptions.LeftMargin -
                        htmlToPdfConverter.PdfDocumentOptions.RightMargin;

            // Calculate a resize factor to fit the footer width
            float resizeFactor = 1;
            if (footerHtmlWidth > footerWidth)
                resizeFactor = footerWidth / footerHtmlWidth;

            // Calculate the footer height to preserve the HTML aspect ratio
            float footerHeight = footerHtmlHeight * resizeFactor;

            if (!(footerHeight < htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Height - htmlToPdfConverter.PdfDocumentOptions.TopMargin -
                        htmlToPdfConverter.PdfDocumentOptions.BottomMargin))
            {
                throw new Exception("The footer height cannot be bigger than PDF page height");
            }

            // Set the calculated footer height
            htmlToPdfConverter.PdfDocumentOptions.DocumentObject.Footer.Height = footerHeight;
        }


        void htmlToPdfConverter_PrepareRenderPdfPageEvent(PrepareRenderPdfPageParams eventParams)
        {
            // Set the header visibility in first, odd and even pages
            //if (addHeaderCheckBox.Checked)
            //{
            //    if (eventParams.PageNumber == 1)
            //        eventParams.Page.ShowHeader = showHeaderInFirstPageCheckBox.Checked;
            //    else if ((eventParams.PageNumber % 2) == 0 && !showHeaderInEvenPagesCheckBox.Checked)
            //        eventParams.Page.ShowHeader = false;
            //    else if ((eventParams.PageNumber % 2) == 1 && !showHeaderInOddPagesCheckBox.Checked)
            //        eventParams.Page.ShowHeader = false;
            //}

            //// Set the footer visibility in first, odd and even pages
            //if (addFooterCheckBox.Checked)
            //{
            //    if (eventParams.PageNumber == 1)
            //        eventParams.Page.ShowFooter = showFooterInFirstPageCheckBox.Checked;
            //    else if ((eventParams.PageNumber % 2) == 0 && !showFooterInEvenPagesCheckBox.Checked)
            //        eventParams.Page.ShowFooter = false;
            //    else if ((eventParams.PageNumber % 2) == 1 && !showFooterInOddPagesCheckBox.Checked)
            //        eventParams.Page.ShowFooter = false;
            //}
        }

    }

}