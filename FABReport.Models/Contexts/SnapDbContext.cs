﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace KABReport.Models
{
    public class SnapDbContext : IdentityDbContext<User>
    {
        public SnapDbContext() : base("name=DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Disable cascade delete by default for one-to-many relationships.
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            // Override default convertion for Decimals.
            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            modelBuilder.Conventions.Add(new DecimalPropertyConvention(28, 5)); //Set max decimal places to 5

            // Specify entities which will cascade delete.
            modelBuilder.Entity<User>().HasMany(q => q.Claims).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Logins).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Roles).WithRequired().WillCascadeOnDelete();

            //Add Cascade Delete Roles Here
            /*             
                DropForeignKey("dbo.AspNetUsers", "CompanyID", "dbo.Companies");
                DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");             
             */

            base.OnModelCreating(modelBuilder);
        }

        public static SnapDbContext Create() {  return new SnapDbContext();  }

        // Projects
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectGuidance> ProjectGuidances { get; set; }
        public virtual DbSet<ProjectHierarchy> ProjectHierarchies { get; set; }
        public virtual DbSet<ProjectReport> ProjectReports { get; set; }
        public virtual DbSet<ProjectUser> ProjectUsers { get; set; }
        public virtual DbSet<ProjectEmailTemplate> ProjectEmailTemplates { get; set; }
        public virtual DbSet<ProjectUserHierarchyItem> ProjectUserHierarchyItems { get; set; }
        public virtual DbSet<ProjectCustomizedLabel> ProjectCustomizedLabels { get; set; }
        public virtual DbSet<ProjectThemeTemplate> ProjectThemeTemplates { get; set; }

        // Languages
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<LanguageSet> LanguageSets { get; set; }

        // Email
        public virtual DbSet<EmailQueue> EmailQueues { get; set; }
        public virtual DbSet<Fruitful.Email.EmailSetting> EmailSettings { get; set; }

        // Cache
        public virtual DbSet<UserReportCache> UserReportCaches { get; set; }

        // ActivityLogs
        public virtual DbSet<ActivityLog> ActivityLogs { get; set; }

        public static List<User> GetUsers(User CurrentUser) {

            using (var context = new SnapDbContext())
            {                
                return context.Users//.Where(u => u.CompanyID == CurrentUser.CompanyID)
                                        .OrderBy(u => u.Email).ToList();
            }
        }
    }
}
