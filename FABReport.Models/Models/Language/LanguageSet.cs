﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class LanguageSet : IEntity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int LanguageID { get; set; }
        [Required]
        public string Label_Reports { get; set; }
        [Required]
        public string Label_Refresh { get; set; }
        [Required]
        public string Label_SearchPlaceholder { get; set; }
        [Required]
        public string Label_ReportName { get; set; }
        [Required]
        public string Label_Type { get; set; }
        [Required]
        public string Label_Uploaded { get; set; }
        [Required]
        public string Label_Hello { get; set; }
        [Required]
        public string Label_LogOff { get; set; }
        [Required]
        public string Label_NotYet { get; set; }
        [Required]
        public string Label_NoReportsFound { get; set; }
        [Required]
        public string Label_SelectAll { get; set; }
        [Required]
        public string Label_Download { get; set; }
        [Required]
        public string Label_DownloadAll { get; set; }

        public virtual Language Language { get; set; }
    }

    public class LanguageSetViewModel : IEntityViewModel
    {
        [Required]
        public int ID { get; set; }
        public Language Language { get; set; }
        [Required]
        public string Label_Reports { get; set; }
        [Required]
        public string Label_Refresh { get; set; }
        [Required]
        public string Label_SearchPlaceholder { get; set; }
        [Required]
        public string Label_ReportName { get; set; }
        [Required]
        public string Label_Type { get; set; }
        [Required]
        public string Label_Uploaded { get; set; }
        [Required]
        public string Label_Hello { get; set; }
        [Required]
        public string Label_LogOff { get; set; }
        [Required]
        public string Label_NotYet { get; set; }
        [Required]
        public string Label_NoReportsFound { get; set; }
        [Required]
        public string Label_SelectAll { get; set; }
        [Required]
        public string Label_Download { get; set; }
        [Required]
        public string Label_DownloadAll { get; set; }
    }

    public class LanguageSetGridViewModel : IEntityViewModel
    {

        public int ID { get; set; }
        public Language Language { get; set; }
        public string Label_Hello { get; set; }
        public string Label_Type { get; set; }
        public string Label_Refresh { get; set; }
        public string Label_Reports { get; set; }
        public string Label_NotYet { get; set; }
        public string Label_LogOff { get; set; }
    }

    public class LanguageSetMapper : ModelMapper<LanguageSet, LanguageSetViewModel>
    {
        public override void MapToViewModel(LanguageSet model, LanguageSetViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Label_Reports = model.Label_Reports;
            viewModel.Label_Refresh = model.Label_Refresh;
            viewModel.Label_SearchPlaceholder = model.Label_SearchPlaceholder;
            viewModel.Label_ReportName = model.Label_ReportName;
            viewModel.Label_Type = model.Label_Type;
            viewModel.Label_Uploaded = model.Label_Uploaded;
            viewModel.Label_Hello = model.Label_Hello;
            viewModel.Label_LogOff = model.Label_LogOff;
            viewModel.Label_NotYet = model.Label_NotYet;
            viewModel.Label_NoReportsFound = model.Label_NoReportsFound;
            viewModel.Label_SelectAll = model.Label_SelectAll;
            viewModel.Label_Download = model.Label_Download;
            viewModel.Label_DownloadAll = model.Label_DownloadAll;
            viewModel.Language = model.Language;
        }

        public override void MapToModel(LanguageSetViewModel viewModel, LanguageSet model)
        {
            model.ID = viewModel.ID;
            model.Label_Reports = viewModel.Label_Reports;
            model.Label_Refresh = viewModel.Label_Refresh;
            model.Label_SearchPlaceholder = viewModel.Label_SearchPlaceholder;
            model.Label_ReportName = viewModel.Label_ReportName;
            model.Label_Type = viewModel.Label_Type;
            model.Label_Uploaded = viewModel.Label_Uploaded;
            model.Label_Hello = viewModel.Label_Hello;
            model.Label_LogOff = viewModel.Label_LogOff;
            model.Label_NotYet = viewModel.Label_NotYet;
            model.Label_NoReportsFound = viewModel.Label_NoReportsFound;
            model.Label_SelectAll = viewModel.Label_SelectAll;
            model.Label_Download = viewModel.Label_Download;
            model.Label_DownloadAll = viewModel.Label_DownloadAll;
            model.LanguageID = viewModel.Language.ID;
        }

    }

    public class LanguageSetGridMapper : ModelMapper<LanguageSet, LanguageSetGridViewModel>
    {
        public override void MapToViewModel(LanguageSet model, LanguageSetGridViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Label_Hello = model.Label_Hello;
            viewModel.Label_LogOff = model.Label_LogOff;
            viewModel.Label_NotYet = model.Label_NotYet;
            viewModel.Label_Refresh = model.Label_Refresh;
            viewModel.Label_Reports = model.Label_Reports;
            viewModel.Label_Type = model.Label_Type;
            viewModel.Language = model.Language;
        }
        public override void MapToModel(LanguageSetGridViewModel viewModel, LanguageSet model)
        {
            throw new NotImplementedException();
        }
    }
}
