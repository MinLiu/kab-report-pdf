﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class LanguageRepository : EntityRespository<Language>
    {
        public LanguageRepository()
            : this(new SnapDbContext())
        {

        }

        public LanguageRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Language entity)
        {
            var set = _db.Set<Language>().Where(a => a.ID == entity.ID);
            _db.Set<Language>().RemoveRange(set);
            _db.SaveChanges();
        }


    }

}
