﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class LanguageSetRepository : EntityRespository<LanguageSet>
    {
        public LanguageSetRepository()
            : this(new SnapDbContext())
        {

        }

        public LanguageSetRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
