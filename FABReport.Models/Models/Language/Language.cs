﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class Language : IEntity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Label { get; set; }
    }

    public class LanguageViewModel : IEntityViewModel
    {

        public int ID { get; set; }
        [Required(ErrorMessage = "* The Name field is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "* The Label field is required.")]
        public string Label { get; set; }
    }

    public class LanguageMapper : ModelMapper<Language, LanguageViewModel>
    {
        public override void MapToViewModel(Language model, LanguageViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.Label = model.Label;
        }

        public override void MapToModel(LanguageViewModel viewModel, Language model)
        {
            model.ID = viewModel.ID;
            model.Name = viewModel.Name;
            model.Label = viewModel.Label;
        }

    }
}
