﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace KABReport.Models
{
    public class EmailQueue : IEntity
    {

        [Key]
        public int ID { get; set; }
        [Required]
        public string EmailAddressTo { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        public DateTime ExpectedSendingTime { get; set; }
        public DateTime? SendingTime { get; set; }
        public string ProjectUserID { get; set; }

        public virtual ProjectUser ProjectUser { get; set; }
    }

    public class EmailQueueViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailAddressTo { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        [AllowHtml]
        public string Message { get; set; }
        [Required]
        public DateTime ExpectedSendingTime { get; set; }
        public DateTime? SendingTime { get; set; }
    }

    public class EmailQueueMapper : ModelMapper<EmailQueue, EmailQueueViewModel>
    {
        public override void MapToModel(EmailQueueViewModel viewModel, EmailQueue model)
        {
            model.EmailAddressTo = viewModel.EmailAddressTo;
            model.ExpectedSendingTime = viewModel.ExpectedSendingTime;
            model.Title = viewModel.Title;
            model.Message = viewModel.Message;
        }

        public override void MapToViewModel(EmailQueue model, EmailQueueViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.EmailAddressTo = model.EmailAddressTo;
            viewModel.Title = model.Title;
            viewModel.Message = model.Message;
            viewModel.ExpectedSendingTime = model.ExpectedSendingTime;
            viewModel.SendingTime = model.SendingTime;
        }
    }
}
