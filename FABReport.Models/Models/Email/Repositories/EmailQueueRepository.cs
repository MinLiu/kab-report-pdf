﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class EmailQueueRepository : EntityRespository<EmailQueue>
    {
        public EmailQueueRepository()
            : this(new SnapDbContext())
        {

        }

        public EmailQueueRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
