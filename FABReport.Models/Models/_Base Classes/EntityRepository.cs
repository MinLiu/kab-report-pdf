﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class EntityRespository<T> : IDisposable where T : class
    {
        protected DbContext _db;

        public EntityRespository(DbContext db)
        {
            _db = db;
            _db.Database.CommandTimeout = 60;
        }

        public virtual void Create(IEnumerable<T> entityList)
        {
            _db.Set<T>().AddRange(entityList);
            _db.SaveChanges();
        }

        public virtual void Create(T entity)
        {
            _db.Set<T>().Add(entity);
            _db.SaveChanges();
        }

        public virtual DbSet<T> Read()
        {
            return _db.Set<T>();
        }

        public virtual T Find(params object[] keyValues)
        {
            return _db.Set<T>().Find(keyValues);
        }

        public virtual void Update(T entity, string [] updatedColumns = null)
        {
            if ((updatedColumns ?? new string [0]).Length > 0)
            {
                _db.Set<T>().Attach(entity);
                foreach (string col in updatedColumns)
                {
                    _db.Entry(entity).Property(col).IsModified = true;
                    
                }
                _db.SaveChanges();
            }
            else
            {
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }
            
        }

        public virtual void Update()
        {
            _db.SaveChanges();
        }

        public virtual void Delete(IEnumerable<T> entityList)
        {
            _db.Set<T>().RemoveRange(entityList);
            _db.SaveChanges();
        } 

        public virtual void Delete(T entity)
        {
            if (entity != null)
            {
                _db.Entry(entity).State = EntityState.Deleted;
                _db.SaveChanges();
            }
        }       

        public void Dispose()
        {
            if (_db != null)
                _db.Dispose();
        }
    }
}
