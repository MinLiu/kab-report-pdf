﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{

    public interface IGuidEntity
    {
        //public IEntity() { ID = int.Newint(); }
        string ID { get; set; }
    }


    public interface IGuidEntityViewModel
    {
        [StringLength(128)]
        string ID { get; set; }
    }



}
