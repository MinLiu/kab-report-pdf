﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{

    public interface IEntity
    {
        //public IEntity() { ID = int.Newint(); }
        int ID { get; set; }
    }


    public interface IEntityViewModel
    {
        [StringLength(128)]
        int ID { get; set; }
    }



}
