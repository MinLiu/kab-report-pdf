﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class LogMessage
    {
        public string User { get; set; }
        public string Url { get; set; }
        public string Method { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string QueryString { get; set; }
        public string FormData { get; set; }
        public string IP { get; set; }
        public string Detail { get; set; }
        public string StackTrace { get; set; }
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine();
            str.AppendLine("-----------------Error Begin----------------");
            str.AppendLine("User        : " + User);
            str.AppendLine("Url         : " + Url);
            str.AppendLine("Method      : " + Method);
            str.AppendLine("Controller  : " + Controller);
            str.AppendLine("Action      : " + Action);
            str.AppendLine("QueryString : " + QueryString);
            str.AppendLine("FormData    : " + FormData);
            str.AppendLine("IP          : " + IP);
            str.AppendLine("Detail      : " + Detail);
            str.AppendLine("Stack Trace : " + StackTrace);
            str.AppendLine("-----------------Error End----------------");
            return str.ToString();
        }
    }
}
