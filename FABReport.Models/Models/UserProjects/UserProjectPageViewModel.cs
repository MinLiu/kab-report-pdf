﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{

    public class UserProjectPageViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        public ProjectViewModel Project { get; set; }
        public ProjectUserViewModel ProjectUser { get; set; }
        public ProjectGuidanceViewModel Projectguidance { get; set; }
        public List<ProjectUserHierarchyItemViewModel> UserHierarchies { get; set; }
        private string Company { get; set; }
        private string ProjectName { get; set; }
        private User User { get; set; }
        public bool SetUserProjectViewModel(string company, string projectName, User user, out string error)
        {
            error = "";

            Company = company;
            ProjectName = projectName;
            User = user;

            if(!SetProjectViewModel(out error))
                return false;
            if(!SetProjectUserViewModel(out error))
                return false;
            if(!SetProjectGuidance(out error))
                return false;
            if (!SetProjectUserHierarchies(out error))
                return false;

            return true;

        }

        private bool SetProjectViewModel(out string error)
        {
            error = "";
            Project = new ProjectViewModel();
            ProjectRepository projectRepo = new ProjectRepository();
            ProjectMapper projectMapper = new ProjectMapper();
            Project pj = projectRepo.Read()
                                    .Where(x => x.Enabled == true)
                                    .Where(p => p.Client == Company && p.Name == ProjectName).FirstOrDefault();
            if (pj == null)
            {
                error = "The project has been removed.";
                return false;
            }
            else
            {
                projectMapper.MapToViewModel(pj, Project);
                return true;
            }
        }

        private bool SetProjectUserViewModel(out string error)
        {
            error = "";
            ProjectUser = new ProjectUserViewModel();
            ProjectUserRepository pUserRepo = new ProjectUserRepository();
            ProjectUserMapper projectUserMapper = new ProjectUserMapper();
            ProjectUser pUser = pUserRepo.Read().Where(pu => pu.ProjectID == Project.ID && pu.UserID == User.Id).FirstOrDefault();
            if (pUser == null)
            {
                error = "Access Not Allowed.<br /> Your user credentials do not give you access to this report area. If you think this is incorrect, please contact Karian and Box.";
                return false;
            }
            else
            {
                projectUserMapper.MapToViewModel(pUser, ProjectUser);
                return true;
            }
        }

        private bool SetProjectGuidance(out string error)
        {
            error = "";
            ProjectGuidanceRepository pGuidanceRepo = new ProjectGuidanceRepository();
            ProjectGuidanceMapper guidanceMapper = new ProjectGuidanceMapper();
            IQueryable<ProjectGuidance> queryGuidance = pGuidanceRepo.Read().Where(pg => pg.ProjectID == Project.ID);

            ProjectGuidance guidanceModel = queryGuidance.Where(qg => qg.Language.Name == ProjectUser.DefaultLanguage).FirstOrDefault();
            if (guidanceModel == null)
                guidanceModel = queryGuidance.FirstOrDefault();

            if (guidanceModel != null)
                Projectguidance = guidanceMapper.MapToViewModel(guidanceModel);

            return true;

        }

        private bool SetProjectUserHierarchies(out string error)
        {
            error = "";
            var repo = new ProjectUserHierarchyItemRepository();
            var mapper = new ProjectUserHierarchyItemMapper();
            UserHierarchies = repo.Read()
                                  .Where(x => x.ProjectID == Project.ID)
                                  .Where(x => x.ProjectUserID == ProjectUser.ID)
                                  .ToList()
                                  .Select(x => mapper.MapToViewModel(x))
                                  .ToList();

            return true;
        }

    }
}
