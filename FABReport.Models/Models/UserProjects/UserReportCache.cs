﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class UserReportCache
    {
        [Key]
        public long ID { get; set; }
        [Index]
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public string AccessMetaname { get; set; }
        [Index]
        [Required]
        public string UserID { get; set; }
        
        public Project Project { get; set; }
        public User User { get; set; }
    }
}
