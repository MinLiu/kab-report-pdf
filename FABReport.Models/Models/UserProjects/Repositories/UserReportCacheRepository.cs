﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class UserReportCacheRepository : EntityRespository<UserReportCache>
    {
        public UserReportCacheRepository()
            : this(new SnapDbContext())
        {

        }

        public UserReportCacheRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
