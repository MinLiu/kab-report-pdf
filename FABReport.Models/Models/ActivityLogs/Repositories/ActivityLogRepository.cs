﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ActivityLogRepository : EntityRespository<ActivityLog>
    {
        public ActivityLogRepository()
            : this(new SnapDbContext())
        {

        }

        public ActivityLogRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
