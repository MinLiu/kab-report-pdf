﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ActivityLog
    {
        [Key]
        public long ID { get; set; }
        public string UserID { get; set; }
        public string ProjectUserID { get; set; }
        public int ProjectID { get; set; }
        public string IPAddress { get; set; }
        public DateTime Timestamp { get; set; }
        public string Description { get; set; }

        public virtual User User { get; set; }
        public virtual ProjectUser ProjectUser { get; set; }
        public virtual Project Project { get; set; }
    }

    public class ActivityLogViewModel
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IPAddress { get; set; }
        public DateTime Timestamp { get; set; }
        public string Description { get; set; }
    }
}
