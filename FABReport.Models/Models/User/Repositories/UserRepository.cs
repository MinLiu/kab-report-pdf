﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class UserRepository : EntityRespository<User>
    {
        public UserRepository()
            : this(new SnapDbContext())
        {

        }

        public UserRepository(SnapDbContext db)
            : base(db)
        {

        }

        
        public override void Delete(User entity)
        {            
            var set = _db.Set<User>().Where(a => a.Id == entity.Id)
                                    .Include(a => a.Claims);

            _db.Set<User>().RemoveRange(set);
            _db.SaveChanges();
        }
         

    }

}
