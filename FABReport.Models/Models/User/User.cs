﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace KABReport.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    [Table("AspNetUsers")]
    public class User : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        //Addition user information. Stored along side in the same tables.
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PlainPassword { get; set; }
        public string Token { get; set; } //Login token
                         
        //Foreign References     
        //[ForeignKey("AssignedUserID")]
        //public virtual ICollection<Claim> AssignedClaims { get; set; }
        

        //[ForeignKey("AssignedUserID")]
        //public virtual ICollection<CandidateEvent> AssignedCandidateEvents { get; set; }

        //[ForeignKey("CreatorID")]
        //public virtual ICollection<CandidateEvent> CreatorCandidateEvents { get; set; }

        public virtual ICollection<ProjectUser> ProjectUsers { get; set; }
        
    }


}