﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectThemeTemplate : IEntity
    {

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        public string ViewPanelBackgroundColor { get; set; }
        public string ButtonColor { get; set; }
        public string ButtonTextColor { get; set; }
        public string PageBackgroundColor { get; set; }
        public string TreeViewBackgroundColor { get; set; }
        public string TreeViewTextColor { get; set; }
        public string LogoPath { get; set; }
        public string TitleColor { get; set; }
        public string TitleAlign { get; set; }
        public string TreeViewSelectedTextColor { get; set; }
        public string TreeViewSelectedBackgroundColor { get; set; }
        public int TreeViewPanelWidth { get; set; }

        public bool Deleted { get; set; }

        public DateTime Created { get; set; }
    }



    public class ProjectThemeTemplateViewModel : IEntityViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "* The Name field is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public string ViewPanelBackgroundColor { get; set; }
        public string ButtonColor { get; set; }
        public string ButtonTextColor { get; set; }
        public string PageBackgroundColor { get; set; }
        public string TreeViewBackgroundColor { get; set; }
        public string TreeViewTextColor { get; set; }
        public string LogoPath { get; set; }
        public string TitleColor { get; set; }
        public string TitleAlign { get; set; }
        public string TreeViewSelectedTextColor { get; set; }
        public string TreeViewSelectedBackgroundColor { get; set; }
        public int TreeViewPanelWidth { get; set; }
    }



    public class ProjectThemeTemplateMapper : ModelMapper<ProjectThemeTemplate, ProjectThemeTemplateViewModel>
    {
        public override void MapToViewModel(ProjectThemeTemplate model, ProjectThemeTemplateViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ButtonColor = model.ButtonColor;
            viewModel.ButtonTextColor = model.ButtonTextColor;
            viewModel.LogoPath = model.LogoPath;
            viewModel.Name = model.Name;
            viewModel.PageBackgroundColor = model.PageBackgroundColor;
            viewModel.TreeViewBackgroundColor = model.TreeViewBackgroundColor;
            viewModel.TreeViewTextColor = model.TreeViewTextColor;
            viewModel.ViewPanelBackgroundColor = model.ViewPanelBackgroundColor;
            viewModel.TitleColor = model.TitleColor;
            viewModel.TitleAlign = model.TitleAlign;
            viewModel.TreeViewSelectedTextColor = model.TreeViewSelectedTextColor;
            viewModel.TreeViewSelectedBackgroundColor = model.TreeViewSelectedBackgroundColor;
            viewModel.TreeViewPanelWidth = model.TreeViewPanelWidth;
        }

        public override void MapToModel(ProjectThemeTemplateViewModel viewModel, ProjectThemeTemplate model)
        {
            model.ButtonColor = viewModel.ButtonColor;
            model.ButtonTextColor = viewModel.ButtonTextColor;
            model.LogoPath = viewModel.LogoPath;
            model.Name = viewModel.Name;
            model.PageBackgroundColor = viewModel.PageBackgroundColor;
            model.TreeViewBackgroundColor = viewModel.TreeViewBackgroundColor;
            model.TreeViewTextColor = viewModel.TreeViewTextColor;
            model.ViewPanelBackgroundColor = viewModel.ViewPanelBackgroundColor;
            model.TitleColor = viewModel.TitleColor;
            model.TitleAlign = viewModel.TitleAlign;
            model.TreeViewSelectedTextColor = viewModel.TreeViewSelectedTextColor;
            model.TreeViewSelectedBackgroundColor = viewModel.TreeViewSelectedBackgroundColor;
            model.TreeViewPanelWidth = viewModel.TreeViewPanelWidth;
        }

    }


}
