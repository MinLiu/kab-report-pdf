﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectUserHierarchyItem : IEntity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string UserID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public string ProjectUserID { get; set; }
        [Required]
        public string Metaname { get; set; }
        
    }

    public class ProjectUserHierarchyItemViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string UserID { get; set; }
        public string ProjectUserID { get; set; }
        public string Metaname { get; set; }
    }

    public class ProjectUserHierarchyItemGridViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string UserID { get; set; }
        public string ProjectUserID { get; set; }
        public string Metaname { get; set; }
    }


    public class ProjectUserHierarchyItemMapper : ModelMapper<ProjectUserHierarchyItem, ProjectUserHierarchyItemViewModel>
    {
        public override void MapToViewModel(ProjectUserHierarchyItem model, ProjectUserHierarchyItemViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.UserID = model.UserID;
            viewModel.Metaname = model.Metaname;
            viewModel.ProjectUserID = model.ProjectUserID;
        }

        public override void MapToModel(ProjectUserHierarchyItemViewModel viewModel, ProjectUserHierarchyItem model)
        {
            throw new NotImplementedException();
        }
    }

    public class ProjectUserHierarchyItemGridMapper : ModelMapper<ProjectUserHierarchyItem, ProjectUserHierarchyItemGridViewModel>
    {
        public override void MapToViewModel(ProjectUserHierarchyItem model, ProjectUserHierarchyItemGridViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.Metaname = model.Metaname;
            viewModel.UserID = model.UserID;
            viewModel.ProjectUserID = model.ProjectUserID;
        }

        public override void MapToModel(ProjectUserHierarchyItemGridViewModel viewModel, ProjectUserHierarchyItem model)
        {
            throw new NotImplementedException();
        }
    }
}
