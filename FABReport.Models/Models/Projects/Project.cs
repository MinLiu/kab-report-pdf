﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class Project : IEntity
    {

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Client { get; set; }
        public string LogoPath { get; set; }
        public string TitleLogoPath { get; set; } 
        public string Color { get; set; }

        public bool HierarchyUploaded { get; set; }
        public bool ReportListUploaded { get; set; }
        public bool UserListUploaded { get; set; }
        public bool Enabled { get; set; }
        public DateTime Created { get; set; }
        public string CreatedByID { get; set; }
        public int? ProjectThemeTemplateID { get; set; }
        public string PageTitle { get; set; }
        public bool ShowHeaderLogo { get; set; }
        public bool ShowTitleLogo { get; set; }

        public virtual ICollection<ProjectGuidance> Guidance { get; set; }
        public virtual User CreatedBy { get; set; }
        public virtual ProjectThemeTemplate ProjectThemeTemplate { get; set; }
    }



    public class ProjectViewModel : IEntityViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "* The Name field is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "* The Client field is required.")]
        [Display(Name = "Client")]
        public string Client { get; set; }

        public string LogoPath { get; set; }
        public string TitleLogoPath { get; set; }
        public string Color { get; set; }

        public bool HierarchyUploaded { get; set; }
        public bool ReportListUploaded { get; set; }
        public bool UserListUploaded { get; set; }
        public bool Enabled { get; set; }
        public string PageTitle { get; set; }
        public int? ProjectThemeTemplateID { get; set; }
        public ProjectThemeTemplate ProjectTheme { get; set; }
        public bool ShowHeaderLogo { get; set; }
        public bool ShowTitleLogo { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
    }



    public class ProjectMapper : ModelMapper<Project, ProjectViewModel>
    {
        public override void MapToViewModel(Project model, ProjectViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Client = model.Client;
            viewModel.Name = model.Name;
            viewModel.Client = model.Client;
            viewModel.LogoPath = model.LogoPath;
            viewModel.TitleLogoPath = model.TitleLogoPath;
            viewModel.Color = model.Color;
            viewModel.HierarchyUploaded = model.HierarchyUploaded;
            viewModel.ReportListUploaded = model.ReportListUploaded;
            viewModel.UserListUploaded = model.UserListUploaded;
            viewModel.Created = model.Created;
            viewModel.CreatedBy = model.CreatedByID != null ? model.CreatedBy.Email : "";
            viewModel.ProjectThemeTemplateID = model.ProjectThemeTemplateID;
            viewModel.ProjectTheme = model.ProjectThemeTemplate ?? null;
            viewModel.Enabled = model.Enabled;
            viewModel.PageTitle = model.PageTitle;
            viewModel.ShowHeaderLogo = model.ShowHeaderLogo;
            viewModel.ShowTitleLogo = model.ShowTitleLogo;
        }

        public override void MapToModel(ProjectViewModel viewModel, Project model)
        {
            model.Client = viewModel.Client;
            model.Name = viewModel.Name;
            model.Client = viewModel.Client;
            model.LogoPath = viewModel.LogoPath;
            model.Color = viewModel.Color;
            model.Created = viewModel.Created;
            model.ProjectThemeTemplateID = viewModel.ProjectThemeTemplateID;
            model.Enabled = viewModel.Enabled;
            model.PageTitle = viewModel.PageTitle;
            model.ShowHeaderLogo = viewModel.ShowHeaderLogo;
            model.ShowTitleLogo = viewModel.ShowTitleLogo;
        }

    }


}
