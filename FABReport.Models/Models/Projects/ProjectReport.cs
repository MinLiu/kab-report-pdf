﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectReport : IEntity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public string Metaname { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string ReportFile { get; set; }
        [Required]
        public string ReportLabel { get; set; }

        public string UrlPath { get; set; }
        public string RelativePath { get; set; }
        public bool Uploaded { get; set; }
        public int FileSize { get; set; }
        
    }

    public class ProjectReportViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string Metaname { get; set; }
        public string Type { get; set; }
        public string ReportFile { get; set; }
        public string ReportLabel { get; set; }
        public string Path { get; set; }
        public bool Uploaded { get; set; }
        public string FileSize { get; set; }
    }

    public class ProjectReportGridViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public string Metaname { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string ReportFile { get; set; }
        [Required]
        public string ReportLabel { get; set; }
        public string Path { get; set; }
        public bool Uploaded { get; set; }
        public string FileSize { get; set; }
    }

    public class ProjectReportMapper : ModelMapper<ProjectReport, ProjectReportViewModel>
    {
        public override void MapToViewModel(ProjectReport model, ProjectReportViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.Metaname = model.Metaname;
            viewModel.Path = model.UrlPath;
            viewModel.ReportFile = model.ReportFile;
            viewModel.ReportLabel = model.ReportLabel;
            viewModel.Type = model.Type;
            viewModel.Uploaded = model.Uploaded;
            viewModel.FileSize = model.Uploaded == true? string.Format("{0:n0} KB", model.FileSize) : "";
        }

        public override void MapToModel(ProjectReportViewModel viewModel, ProjectReport model)
        {
            throw new NotImplementedException();
        }
    }

    public class ProjectReportGridMapper : ModelMapper<ProjectReport, ProjectReportGridViewModel>
    {
        public override void MapToViewModel(ProjectReport model, ProjectReportGridViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.Metaname = model.Metaname;
            viewModel.Path = model.UrlPath;
            viewModel.ReportFile = model.ReportFile;
            viewModel.ReportLabel = model.ReportLabel;
            viewModel.Type = model.Type;
            viewModel.Uploaded = model.Uploaded;
            viewModel.FileSize = model.Uploaded == true ? string.Format("{0:n0} KB", model.FileSize) : "";
        }

        public override void MapToModel(ProjectReportGridViewModel viewModel, ProjectReport model)
        {
            model.Metaname = viewModel.Metaname;
            model.ProjectID = viewModel.ProjectID;
            model.ReportFile = viewModel.ReportFile;
            model.ReportLabel = viewModel.ReportLabel;
            model.Type = viewModel.Type;
            model.Uploaded = viewModel.Uploaded;
        }
    }
}
