﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectGuidance : IEntity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public int LanguageID { get; set; }
        [Required]
        public string Text { get; set; }

        public virtual Project Project { get; set; }
        public virtual Language Language { get; set; }
    }

    public class ProjectGuidanceViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public int LanguageID { get; set; }
        [Required(ErrorMessage="Please type in guidance")]
        public string Text { get; set; }

        public LanguageViewModel Language { get; set; }
    }


    public class ProjectGuidanceMapper : ModelMapper<ProjectGuidance, ProjectGuidanceViewModel>
    {
        public override void MapToViewModel(ProjectGuidance model, ProjectGuidanceViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.LanguageID = model.LanguageID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.Text = model.Text;
            viewModel.Language = new LanguageViewModel() { ID = model.Language.ID, Name = model.Language.Name };
        }

        public override void MapToModel(ProjectGuidanceViewModel viewModel, ProjectGuidance model)
        {
            model.LanguageID = viewModel.Language.ID;
            model.ProjectID = viewModel.ProjectID;
            model.Text = viewModel.Text;
        }

    }
}
