﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectEmailTemplate : IEntity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public int LanguageID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Template { get; set; }

        public virtual Project Project { get; set; }
        public virtual Language Language { get; set; }
    }

    public class ProjectEmailTemplateViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public int LanguageID { get; set; }
        [Required(ErrorMessage = "Please type in title")]
        public string Title { get; set; }
        [Required(ErrorMessage="Please type in templates")]
        public string Template { get; set; }

        public LanguageViewModel Language { get; set; }
    }


    public class ProjectEmailTemplateMapper : ModelMapper<ProjectEmailTemplate, ProjectEmailTemplateViewModel>
    {
        public override void MapToViewModel(ProjectEmailTemplate model, ProjectEmailTemplateViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.LanguageID = model.LanguageID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.Title = model.Title;
            viewModel.Template = model.Template;
            viewModel.Language = new LanguageViewModel() { ID = model.Language.ID, Name = model.Language.Name };
        }

        public override void MapToModel(ProjectEmailTemplateViewModel viewModel, ProjectEmailTemplate model)
        {
            model.LanguageID = viewModel.Language.ID;
            model.ProjectID = viewModel.ProjectID;
            model.Title = viewModel.Title;
            model.Template = viewModel.Template;
        }

    }
}
