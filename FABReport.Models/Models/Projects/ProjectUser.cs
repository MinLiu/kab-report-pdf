﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectUser : IGuidEntity
    {
        [Key]
        [MaxLength(128)]
        public string ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public string UserID { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DefaultLanguage { get; set; }
        public string Grade { get; set; }
        public string Alignment { get; set; }
        public string Dist { get; set; }
        public string Parent { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public string UserDefinedField1 { get; set; }
        public string UserDefinedField2 { get; set; }
        public string UserDefinedField3 { get; set; }
        public string UserDefinedField4 { get; set; }
        public string UserDefinedField5 { get; set; }
        public string UserDefinedField6 { get; set; }
        public string UserDefinedField7 { get; set; }
        public string UserDefinedField8 { get; set; }
        public string UserDefinedField9 { get; set; }
        public string UserDefinedField10 { get; set; }

        public virtual Project Project { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ProjectUserHierarchyItem> ProjectUserHierarchyItem { get; set; }
        public virtual ICollection<EmailQueue> SentEmails { get; set; }
        public virtual ICollection<ActivityLog> ActivityLogs { get; set; }
        
    }

    public class ProjectUserViewModel :IGuidEntityViewModel
    {
        public string ID { get; set; }
        public int ProjectID { get; set; }
        public string UserID { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DefaultLanguage { get; set; }
        public string Grade { get; set; }
        public string Alignment { get; set; }
        public string Dist { get; set; }
        public string Parent { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public string UserDefinedField1 { get; set; }
        public string UserDefinedField2 { get; set; }
        public string UserDefinedField3 { get; set; }
        public string UserDefinedField4 { get; set; }
        public string UserDefinedField5 { get; set; }
        public string UserDefinedField6 { get; set; }
        public string UserDefinedField7 { get; set; }
        public string UserDefinedField8 { get; set; }
        public string UserDefinedField9 { get; set; }
        public string UserDefinedField10 { get; set; }

        public string Metaname { get; set; }

        // EmailQueue
        public string EmailContent { get; set; }
        public DateTime? EmailExpectedSendingTime { get; set; }
        public DateTime? EmailSendingTime { get; set; }
        public int EmailsTotal { get; set; }

        // Last Login Time
        public DateTime? LastLoginTime { get; set; }
    }

    public class ProjectUserGridViewModel : IGuidEntityViewModel
    {
        public string ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        public string UserID { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string DefaultLanguage { get; set; }
        public string Grade { get; set; }
        public string Alignment { get; set; }
        public string Dist { get; set; }
        public string Parent { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public string UserDefinedField1 { get; set; }
        public string UserDefinedField2 { get; set; }
        public string UserDefinedField3 { get; set; }
        public string UserDefinedField4 { get; set; }
        public string UserDefinedField5 { get; set; }
        public string UserDefinedField6 { get; set; }
        public string UserDefinedField7 { get; set; }
        public string UserDefinedField8 { get; set; }
        public string UserDefinedField9 { get; set; }
        public string UserDefinedField10 { get; set; }
        public string Metaname { get; set; }
        [Required]
        public List<MetanameItemViewModel> MetanameIDs { get; set; }

        // EmailQueue
        public string EmailContent { get; set; }
        public DateTime? EmailExpectedSendingTime { get; set; }
        public DateTime? EmailSendingTime { get; set; }
        public int EmailsTotal { get; set; }

        // Last Login Time
        public DateTime? LastLoginTime { get; set; }
    }

    public class ProjectUserMapper : ModelMapper<ProjectUser, ProjectUserViewModel>
    {
        public override void MapToViewModel(ProjectUser model, ProjectUserViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.UserID = model.UserID;
            viewModel.Email = model.Email;
            viewModel.Metaname = string.Join(",", new SnapDbContext().ProjectUserHierarchyItems.Where(pu => pu.ProjectID == model.ProjectID && pu.UserID == model.User.Id).Select(pu => pu.Metaname.ToString()).ToList());
            viewModel.UserDefinedField1 = model.UserDefinedField1;
            viewModel.UserDefinedField2 = model.UserDefinedField2;
            viewModel.UserDefinedField3 = model.UserDefinedField3;
            viewModel.UserDefinedField4 = model.UserDefinedField4;
            viewModel.UserDefinedField5 = model.UserDefinedField5;
            viewModel.UserDefinedField6 = model.UserDefinedField6;
            viewModel.UserDefinedField7 = model.UserDefinedField7;
            viewModel.UserDefinedField8 = model.UserDefinedField8;
            viewModel.UserDefinedField9 = model.UserDefinedField9;
            viewModel.UserDefinedField10 = model.UserDefinedField10;
            viewModel.FirstName = model.FirstName;
            viewModel.LastName = model.LastName;
            viewModel.DefaultLanguage = model.DefaultLanguage;
            viewModel.Grade = model.Grade;
            viewModel.Alignment = model.Alignment;
            viewModel.Dist = model.Dist;
            viewModel.Parent = model.Parent;
            viewModel.Password = model.Password;
            viewModel.Role = model.Role;
            //viewModel.EmailContent = model.SentEmails.Count() > 0? model.SentEmails.OrderByDescending(e => e.SendingTime).FirstOrDefault().Message: "";
            viewModel.EmailExpectedSendingTime = model.SentEmails.Count() > 0 ? model.SentEmails.OrderByDescending(e => e.ID).FirstOrDefault().ExpectedSendingTime : (DateTime?)null;
            viewModel.EmailSendingTime = model.SentEmails.Count() > 0 ? model.SentEmails.OrderByDescending(e => e.ID).FirstOrDefault().SendingTime : (DateTime?)null;
            viewModel.EmailsTotal = model.SentEmails.Where(x => x.SendingTime != null).Count();

            viewModel.LastLoginTime = model.ActivityLogs.Where(x => x.Description == "Login").OrderByDescending(x => x.Timestamp).Any() ? model.ActivityLogs.Where(x => x.Description == "Login").OrderByDescending(x => x.Timestamp).First().Timestamp : (DateTime?)null;
        }

        public override void MapToModel(ProjectUserViewModel viewModel, ProjectUser model)
        {
            throw new NotImplementedException();
        }
    }

    public class ProjectUserGridMapper : ModelMapper<ProjectUser, ProjectUserGridViewModel>
    {
        public override void MapToViewModel(ProjectUser model, ProjectUserGridViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.UserID = model.UserID;
            viewModel.Email = model.Email;
            viewModel.MetanameIDs = new SnapDbContext().ProjectUserHierarchyItems.Where(pu => pu.ProjectID == model.ProjectID && pu.UserID == model.User.Id).Select(pu => new MetanameItemViewModel { Name= pu.Metaname }).ToList();
            viewModel.Metaname = string.Join(",", new SnapDbContext().ProjectUserHierarchyItems.Where(pu => pu.ProjectID == model.ProjectID && pu.UserID == model.User.Id).Select(pu => pu.Metaname.ToString()).ToList());
            viewModel.UserDefinedField1 = model.UserDefinedField1;
            viewModel.UserDefinedField2 = model.UserDefinedField2;
            viewModel.UserDefinedField3 = model.UserDefinedField3;
            viewModel.UserDefinedField4 = model.UserDefinedField4;
            viewModel.UserDefinedField5 = model.UserDefinedField5;
            viewModel.UserDefinedField6 = model.UserDefinedField6;
            viewModel.UserDefinedField7 = model.UserDefinedField7;
            viewModel.UserDefinedField8 = model.UserDefinedField8;
            viewModel.UserDefinedField9 = model.UserDefinedField9;
            viewModel.UserDefinedField10 = model.UserDefinedField10;
            viewModel.UserDefinedField10 = model.UserDefinedField10;
            viewModel.FirstName = model.FirstName;
            viewModel.LastName = model.LastName;
            viewModel.DefaultLanguage = model.DefaultLanguage;
            viewModel.Grade = model.Grade;
            viewModel.Alignment = model.Alignment;
            viewModel.Dist = model.Dist;
            viewModel.Parent = model.Parent;
            viewModel.Password = model.Password == ""? "USING PREVIOUS PASSWORD" : model.Password;
            viewModel.Role = model.Role;
            //viewModel.EmailContent = model.SentEmails.Count() > 0 ? model.SentEmails.OrderByDescending(e => e.SendingTime).FirstOrDefault().Message : "";
            viewModel.EmailExpectedSendingTime = model.SentEmails.Count() > 0 ? model.SentEmails.OrderByDescending(e => e.ID).FirstOrDefault().ExpectedSendingTime : (DateTime?)null;
            viewModel.EmailSendingTime = model.SentEmails.Count() > 0 ? model.SentEmails.OrderByDescending(e => e.ID).FirstOrDefault().SendingTime : (DateTime?)null;
            viewModel.EmailsTotal = model.SentEmails.Where(x => x.SendingTime != null).Count();
            viewModel.LastLoginTime = model.ActivityLogs.Where(x => x.Description == "Login").OrderByDescending(x => x.Timestamp).Any() ? model.ActivityLogs.Where(x => x.Description == "Login").OrderByDescending(x => x.Timestamp).First().Timestamp : (DateTime?)null;
        }

        public override void MapToModel(ProjectUserGridViewModel viewModel, ProjectUser model)
        {
            model.Alignment = viewModel.Alignment;
            model.DefaultLanguage = viewModel.DefaultLanguage;
            model.Dist = viewModel.Dist;
            model.FirstName = viewModel.FirstName;
            model.Grade = viewModel.Grade;
            model.LastName = viewModel.LastName;
            model.UserDefinedField1 = viewModel.UserDefinedField1;
            model.UserDefinedField2 = viewModel.UserDefinedField2;
            model.UserDefinedField3 = viewModel.UserDefinedField3;
            model.UserDefinedField4 = viewModel.UserDefinedField4;
            model.UserDefinedField5 = viewModel.UserDefinedField5;
            model.UserDefinedField6 = viewModel.UserDefinedField6;
            model.UserDefinedField7 = viewModel.UserDefinedField7;
            model.UserDefinedField8 = viewModel.UserDefinedField8;
            model.UserDefinedField9 = viewModel.UserDefinedField9;
            model.UserDefinedField10 = viewModel.UserDefinedField10;
            model.Parent = viewModel.Parent;
        }
    }
}
