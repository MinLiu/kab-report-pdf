﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectCustomizedLabel : IEntity
    {

        [Key]
        public int ID { get; set; }

        public int ProjectID { get; set; }

        public string Name { get; set; }
        
        public string LabelIndex { get; set; }

        public virtual Project Project { get; set; }

    }



    public class ProjectCustomizedLabelViewModel : IEntityViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string LabelIndex { get; set; }
    }



    public class ProjectCustomizedLabeltMapper : ModelMapper<ProjectCustomizedLabel, ProjectCustomizedLabelViewModel>
    {
        public override void MapToViewModel(ProjectCustomizedLabel model, ProjectCustomizedLabelViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.LabelIndex = model.LabelIndex;
        }

        public override void MapToModel(ProjectCustomizedLabelViewModel viewModel, ProjectCustomizedLabel model)
        {
            throw new NotImplementedException();
        }

    }


}
