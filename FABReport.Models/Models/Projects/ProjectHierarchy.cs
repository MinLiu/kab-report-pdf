﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectHierarchy : IEntity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int ProjectID { get; set; }
        [Required]
        public string Metaname { get; set; }
        [Required]
        public string InternalLabelName { get; set; }
        [Required]
        public string ExternalLabelName { get; set; }

        public string StructureName { get; set; }
        public int Level { get; set; }
        public string ParentMetaname { get; set; }
        public int? ParentHierarchyID { get; set; }

        public virtual ProjectHierarchy ParentHierarchy { get; set; }
        public virtual ICollection<ProjectHierarchy> ChildrenHierarchies { get; set; }
        public void GetChildrenHiers(List<ProjectHierarchy> list)
        {
            list.Add(this);
            foreach (ProjectHierarchy child in ChildrenHierarchies)
            {
                child.GetChildrenHiers(list);
            }
        }
    }

    public class ProjectHierarchyViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string Metaname { get; set; }
        public string InternalLabelName { get; set; }
        public string ExternalLabelName { get; set; }
        public string StructureName { get; set; }
        public int Level { get; set; }
    }

    public class ProjectHierarchyGridViewModel : IEntityViewModel
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string Metaname { get; set; }
        public string InternalLabelName { get; set; }
        public string ExternalLabelName { get; set; }
        public string StructureName { get; set; }
        public int Level { get; set; }
    }

    public class ProjectHierarchyMapper : ModelMapper<ProjectHierarchy, ProjectHierarchyViewModel>
    {
        public override void MapToViewModel(ProjectHierarchy model, ProjectHierarchyViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.Metaname = model.Metaname;
            viewModel.InternalLabelName = model.InternalLabelName;
            viewModel.ExternalLabelName = model.ExternalLabelName;
            viewModel.StructureName = model.StructureName;
            viewModel.Level = model.Level;
        }

        public override void MapToModel(ProjectHierarchyViewModel viewModel, ProjectHierarchy model)
        {
            throw new NotImplementedException();
        }
    }


    public class ProjectHierarchyGridMapper : ModelMapper<ProjectHierarchy, ProjectHierarchyGridViewModel>
    {
        public override void MapToViewModel(ProjectHierarchy model, ProjectHierarchyGridViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.ProjectID = model.ProjectID;
            viewModel.Metaname = model.Metaname;
            viewModel.InternalLabelName = model.InternalLabelName;
            viewModel.ExternalLabelName = model.ExternalLabelName;
            viewModel.StructureName = model.StructureName;
            viewModel.Level = model.Level;

        }

        public override void MapToModel(ProjectHierarchyGridViewModel viewModel, ProjectHierarchy model)
        {
            throw new NotImplementedException();
        }

    }
}
