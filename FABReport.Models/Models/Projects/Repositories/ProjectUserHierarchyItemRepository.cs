﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectUserHierarchyItemRepository : EntityRespository<ProjectUserHierarchyItem>
    {
        public ProjectUserHierarchyItemRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectUserHierarchyItemRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
