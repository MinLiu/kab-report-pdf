﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectUserRepository : EntityRespository<ProjectUser>
    {
        public ProjectUserRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectUserRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
