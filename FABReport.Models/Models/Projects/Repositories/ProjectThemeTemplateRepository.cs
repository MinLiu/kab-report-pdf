﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectThemeTemplateRepository : EntityRespository<ProjectThemeTemplate>
    {
        public ProjectThemeTemplateRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectThemeTemplateRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(ProjectThemeTemplate entity)
        {
            var model = _db.Set<ProjectThemeTemplate>().Where(x => x.ID == entity.ID).FirstOrDefault();
            if (model != null)
            {
                model.Deleted = true;
                _db.SaveChanges();
            }
        }


    }

}
