﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectGuidanceRepository : EntityRespository<ProjectGuidance>
    {
        public ProjectGuidanceRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectGuidanceRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
