﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectReportRepository : EntityRespository<ProjectReport>
    {
        public ProjectReportRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectReportRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
