﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectEmailTemplateRepository : EntityRespository<ProjectEmailTemplate>
    {
        public ProjectEmailTemplateRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectEmailTemplateRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
