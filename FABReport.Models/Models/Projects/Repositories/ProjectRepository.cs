﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectRepository : EntityRespository<Project>
    {
        public ProjectRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Project entity)
        {
            var set = _db.Set<Project>().Where(a => a.ID == entity.ID);
            _db.Set<Project>().RemoveRange(set);
            _db.SaveChanges();
        }


    }

}
