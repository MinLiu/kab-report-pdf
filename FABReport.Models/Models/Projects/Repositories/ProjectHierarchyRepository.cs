﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectHierarchyRepository : EntityRespository<ProjectHierarchy>
    {
        public ProjectHierarchyRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectHierarchyRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
