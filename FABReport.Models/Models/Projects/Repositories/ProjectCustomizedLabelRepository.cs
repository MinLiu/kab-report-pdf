﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace KABReport.Models
{
    public class ProjectCustomizedLabelRepository : EntityRespository<ProjectCustomizedLabel>
    {
        public ProjectCustomizedLabelRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectCustomizedLabelRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
