namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProjectStructure : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectStructures", "Level", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectStructures", "Level");
        }
    }
}
