namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CandidateUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidates", "CurrentEmployer", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Candidates", "CurrentEmployer");
        }
    }
}
