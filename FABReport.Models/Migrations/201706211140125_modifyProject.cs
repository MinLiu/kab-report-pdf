namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "LogoPath", c => c.String());
            AddColumn("dbo.Projects", "Color", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Color");
            DropColumn("dbo.Projects", "LogoPath");
        }
    }
}
