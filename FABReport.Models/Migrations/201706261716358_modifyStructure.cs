namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyStructure : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HierarchyStructureItems", "AccessMetaname", c => c.Int(nullable: false));
            DropColumn("dbo.HierarchyStructureItems", "StructureID");
            DropColumn("dbo.ProjectUsers", "ProjectStructureID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectUsers", "ProjectStructureID", c => c.Int(nullable: false));
            AddColumn("dbo.HierarchyStructureItems", "StructureID", c => c.Int(nullable: false));
            DropColumn("dbo.HierarchyStructureItems", "AccessMetaname");
        }
    }
}
