namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateALot : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectUsers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 128),
                        Metaname = c.String(),
                        ProjectStructureID = c.Int(nullable: false),
                        UserDefinedField1 = c.String(),
                        UserDefinedField2 = c.String(),
                        UserDefinedField3 = c.String(),
                        UserDefinedField4 = c.String(),
                        UserDefinedField5 = c.String(),
                        UserDefinedField6 = c.String(),
                        UserDefinedField7 = c.String(),
                        UserDefinedField8 = c.String(),
                        UserDefinedField9 = c.String(),
                        UserDefinedField10 = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Projects", t => t.ProjectID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.ProjectID)
                .Index(t => t.UserID);
            
            AddColumn("dbo.Projects", "UserListUploaded", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "Role", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectUsers", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ProjectUsers", "ProjectID", "dbo.Projects");
            DropIndex("dbo.ProjectUsers", new[] { "UserID" });
            DropIndex("dbo.ProjectUsers", new[] { "ProjectID" });
            DropColumn("dbo.AspNetUsers", "Role");
            DropColumn("dbo.Projects", "UserListUploaded");
            DropTable("dbo.ProjectUsers");
        }
    }
}
