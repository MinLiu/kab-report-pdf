namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.SearchStatus", newName: "ClientIndustries");
            DropForeignKey("dbo.Candidates", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.Candidates", "SearchStatusID", "dbo.SearchStatus");
            DropIndex("dbo.Candidates", new[] { "ClientID" });
            DropIndex("dbo.Candidates", new[] { "SearchStatusID" });
            DropPrimaryKey("dbo.CandidateOpportunities");
            CreateTable(
                "dbo.ClientIndustryItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        ClientIndustryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientIndustries", t => t.ClientIndustryID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientIndustryID);
            
            CreateTable(
                "dbo.ClientSectorItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        CandidateSectorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CandidateSectors", t => t.CandidateSectorID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID)
                .Index(t => t.CandidateSectorID);
            
            AddPrimaryKey("dbo.CandidateOpportunities", "CandidateID");
            DropColumn("dbo.Candidates", "ClientID");
            DropColumn("dbo.CandidateOpportunities", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CandidateOpportunities", "ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Candidates", "ClientID", c => c.Int());
            DropForeignKey("dbo.ClientSectorItems", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientSectorItems", "CandidateSectorID", "dbo.CandidateSectors");
            DropForeignKey("dbo.ClientIndustryItems", "ClientIndustryID", "dbo.ClientIndustries");
            DropForeignKey("dbo.ClientIndustryItems", "ClientID", "dbo.Clients");
            DropIndex("dbo.ClientSectorItems", new[] { "CandidateSectorID" });
            DropIndex("dbo.ClientSectorItems", new[] { "ClientID" });
            DropIndex("dbo.ClientIndustryItems", new[] { "ClientIndustryID" });
            DropIndex("dbo.ClientIndustryItems", new[] { "ClientID" });
            DropPrimaryKey("dbo.CandidateOpportunities");
            DropTable("dbo.ClientSectorItems");
            DropTable("dbo.ClientIndustryItems");
            AddPrimaryKey("dbo.CandidateOpportunities", "ID");
            CreateIndex("dbo.Candidates", "SearchStatusID");
            CreateIndex("dbo.Candidates", "ClientID");
            AddForeignKey("dbo.Candidates", "SearchStatusID", "dbo.SearchStatus", "ID");
            AddForeignKey("dbo.Candidates", "ClientID", "dbo.Clients", "ID");
            RenameTable(name: "dbo.ClientIndustries", newName: "SearchStatus");
        }
    }
}
