namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyMetaname : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HierarchyStructureItems", "Metaname", c => c.String(nullable: false));
            AlterColumn("dbo.HierarchyStructureItems", "AccessMetaname", c => c.String(nullable: false));
            AlterColumn("dbo.ProjectHierarchies", "Metaname", c => c.String(nullable: false));
            AlterColumn("dbo.ProjectReports", "Metaname", c => c.String(nullable: false));
            AlterColumn("dbo.ProjectUserHierarchyItems", "Metaname", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProjectUserHierarchyItems", "Metaname", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectReports", "Metaname", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectHierarchies", "Metaname", c => c.Int(nullable: false));
            AlterColumn("dbo.HierarchyStructureItems", "AccessMetaname", c => c.Int(nullable: false));
            AlterColumn("dbo.HierarchyStructureItems", "Metaname", c => c.Int(nullable: false));
        }
    }
}
