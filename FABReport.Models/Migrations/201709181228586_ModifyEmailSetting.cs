namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyEmailSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailSettings", "Threshhold", c => c.Int(nullable: false, defaultValue: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSettings", "Threshhold");
        }
    }
}
