namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReplacementDateRemove : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.CandidateOpportunities", new[] { "StatusID" });
            AddColumn("dbo.Clients", "Source", c => c.String());
            AlterColumn("dbo.CandidateOpportunities", "StatusID", c => c.Int());
            CreateIndex("dbo.CandidateOpportunities", "StatusID");
            DropColumn("dbo.Opportunities", "ReplacementDate");
            DropColumn("dbo.CandidateOpportunityStatus", "HexColour");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CandidateOpportunityStatus", "HexColour", c => c.String(nullable: false));
            AddColumn("dbo.Opportunities", "ReplacementDate", c => c.DateTime(nullable: false));
            DropIndex("dbo.CandidateOpportunities", new[] { "StatusID" });
            AlterColumn("dbo.CandidateOpportunities", "StatusID", c => c.Int(nullable: false));
            DropColumn("dbo.Clients", "Source");
            CreateIndex("dbo.CandidateOpportunities", "StatusID");
        }
    }
}
