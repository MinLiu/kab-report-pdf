﻿namespace KABReport.Models.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KABReport.Models.SnapDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(KABReport.Models.SnapDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            
            //Add roles            
            context.Roles.AddOrUpdate(
                new IdentityRole { Id = "-1", Name = "Super Admin" },
                new IdentityRole { Id = "1", Name = "Admin" },
                new IdentityRole { Id = "2", Name = "User" }
            );
            context.SaveChanges();

            
            
            ////Add Company  
            //if (context.Companies.Count() <= 0)
            //{
            //    context.Companies.AddOrUpdate(
            //        new Company
            //        {
            //            ID = 1,
            //            Name = "Super User Company",
            //            Email = "developer@fruitfulgroup.com"
            //        }
            //    );
            //    context.SaveChanges();
            //}

            
            //Add Default Users Passwords : "Dev123!" 
            if (context.Users.Where(u => u.Id == "abe7ab8c-0063-48c2-9130-060ffc8169e1").LongCount() <= 0)
            {
                context.Users.Add(
                    new User
                    {
                        Id = "abe7ab8c-0063-48c2-9130-060ffc8169e1",
                        Email = "developer@fruitfulgroup.com",
                        UserName = "developer@fruitfulgroup.com",
                        PasswordHash = "AIh69MUSNlJKo1QGyT2tC1OSZK8NX3VM/x3gd6LLIRbhHO3WVNF+8FqVjv3XKnPN1A==",
                        SecurityStamp = "b75b58c7-4318-4cbf-99c7-8429856c9d0b",
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = false,
                        AccessFailedCount = 0,
                        //CompanyID = 1,
                        Roles = { new IdentityUserRole { UserId = "abe7ab8c-0063-48c2-9130-060ffc8169e1", RoleId = "-1" } },
                    }
                );
            }

            context.Languages.AddOrUpdate(
                new Language { ID = 1, Name = "English", Label="English" }
            );

            context.LanguageSets.AddOrUpdate(
                new LanguageSet { ID = 1, LanguageID = 1, Label_Hello = "Hello", Label_LogOff = "Log Off", Label_NotYet = "Not Yet", Label_Reports = "Reports", Label_Refresh = "Refresh", Label_ReportName = "Report Name", Label_SearchPlaceholder = "Search by Name, or Type", Label_Type = "Type", Label_Uploaded = "Uploaded", Label_NoReportsFound = "No Reports found", Label_Download = "Download", Label_DownloadAll = "Download All", Label_SelectAll = "Select All" }
            );

            //context.CandidateRoles.AddOrUpdate(
            //    new CandidateRole { ID = 1, Name = "Account Manager" },
            //    new CandidateRole { ID = 2, Name = "Account Director" },
            //    new CandidateRole { ID = 3, Name = "Business Development" },
            //    new CandidateRole { ID = 4, Name = "Channels" },
            //    new CandidateRole { ID = 5, Name = "Consultant" },
            //    new CandidateRole { ID = 6, Name = "Customer Success" },
            //    new CandidateRole { ID = 7, Name = "Finance" }
            //);

            // context.CandidateSectors.AddOrUpdate(
            //    new CandidateSector { ID = 1, Name = "Finance" },
            //    new CandidateSector { ID = 2, Name = "Govt" },
            //    new CandidateSector { ID = 3, Name = "Health & Life Science" }
            //);

            // context.OpportunityStatuses.AddOrUpdate(
            //     new OpportunityStatus { ID = OpportunityStatusValues.Open, Name = "Open", HexColour = "#5f8dd3" },
            //     new OpportunityStatus { ID = OpportunityStatusValues.Closed, Name = "Closed", HexColour = "#c83771" }
            // );

            // context.CandidateOpportunityStatuses.AddOrUpdate(
            //     new CandidateOpportunityStatus { ID = 1, Name = "."},
            //      new CandidateOpportunityStatus { ID = 2, Name = "Not Taken" },
            //      new CandidateOpportunityStatus { ID = 3, Name = "Taken" }
            //  );        
   
            context.SaveChanges();
           
        }
    }
}
