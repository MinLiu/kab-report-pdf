namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HierarchyStructureItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Metaname = c.String(nullable: false),
                        StructureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProjectStructures",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Metaname = c.String(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Projects", "HierarchyUploaded", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "ReportListUploaded", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "ReportListUploaded");
            DropColumn("dbo.Projects", "HierarchyUploaded");
            DropTable("dbo.ProjectStructures");
            DropTable("dbo.HierarchyStructureItems");
        }
    }
}
