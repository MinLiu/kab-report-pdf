namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjectTheme : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectThemeTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        ViewPanelBackgroundColor = c.String(),
                        ButtonColor = c.String(),
                        ButtonTextColor = c.String(),
                        PageBackgroundColor = c.String(),
                        TreeViewBackgroundColor = c.String(),
                        TreeViewTextColor = c.String(),
                        LogoPath = c.String(),
                        Deleted = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Projects", "Enabled", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Enabled");
            DropTable("dbo.ProjectThemeTemplates");
        }
    }
}
