namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParentHierID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectHierarchies", "ParentHierarchyID", c => c.Int());
            CreateIndex("dbo.ProjectHierarchies", "ParentHierarchyID");
            AddForeignKey("dbo.ProjectHierarchies", "ParentHierarchyID", "dbo.ProjectHierarchies", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectHierarchies", "ParentHierarchyID", "dbo.ProjectHierarchies");
            DropIndex("dbo.ProjectHierarchies", new[] { "ParentHierarchyID" });
            DropColumn("dbo.ProjectHierarchies", "ParentHierarchyID");
        }
    }
}
