namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFileSize : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectReports", "FileSize", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectReports", "FileSize");
        }
    }
}
