namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyReport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectReports", "UrlPath", c => c.String());
            AddColumn("dbo.ProjectReports", "PhysicalPath", c => c.String());
            DropColumn("dbo.ProjectReports", "Path");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectReports", "Path", c => c.String());
            DropColumn("dbo.ProjectReports", "PhysicalPath");
            DropColumn("dbo.ProjectReports", "UrlPath");
        }
    }
}
