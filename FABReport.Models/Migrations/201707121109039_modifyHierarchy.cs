namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyHierarchy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectHierarchies", "StructureName", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "Level", c => c.Int(nullable: false));
            DropColumn("dbo.ProjectHierarchies", "StructureLevel1");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel2");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel3");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel4");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel5");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel6");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel7");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel8");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel9");
            DropColumn("dbo.ProjectHierarchies", "StructureLevel10");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectHierarchies", "StructureLevel10", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel9", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel8", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel7", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel6", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel5", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel4", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel3", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel2", c => c.String());
            AddColumn("dbo.ProjectHierarchies", "StructureLevel1", c => c.String());
            DropColumn("dbo.ProjectHierarchies", "Level");
            DropColumn("dbo.ProjectHierarchies", "StructureName");
        }
    }
}
