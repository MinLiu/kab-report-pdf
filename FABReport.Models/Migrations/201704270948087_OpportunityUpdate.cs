namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OpportunityUpdate : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.CandidateOpportunities");
            AddColumn("dbo.CandidateOpportunities", "ID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.CandidateOpportunities", "ID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.CandidateOpportunities");
            DropColumn("dbo.CandidateOpportunities", "ID");
            AddPrimaryKey("dbo.CandidateOpportunities", "CandidateID");
        }
    }
}
