namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLanguageSet : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LanguageSets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LanguageID = c.Int(nullable: false),
                        Label_Reports = c.String(nullable: false),
                        Label_Refresh = c.String(nullable: false),
                        Label_SearchPlaceholder = c.String(nullable: false),
                        Label_ReportName = c.String(nullable: false),
                        Label_Type = c.String(nullable: false),
                        Label_Uploaded = c.String(nullable: false),
                        Label_Hello = c.String(nullable: false),
                        Label_LogOff = c.String(nullable: false),
                        Label_NotYet = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Languages", t => t.LanguageID)
                .Index(t => t.LanguageID);
            
            AddColumn("dbo.Languages", "Label", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LanguageSets", "LanguageID", "dbo.Languages");
            DropIndex("dbo.LanguageSets", new[] { "LanguageID" });
            DropColumn("dbo.Languages", "Label");
            DropTable("dbo.LanguageSets");
        }
    }
}
