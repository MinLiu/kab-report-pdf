namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyEmailQueue1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EmailQueues", "SendingTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EmailQueues", "SendingTime", c => c.DateTime(nullable: false));
        }
    }
}
