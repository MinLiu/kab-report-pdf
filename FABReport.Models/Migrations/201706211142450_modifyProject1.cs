namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProject1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Projects", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "Description", c => c.String(nullable: false));
        }
    }
}
