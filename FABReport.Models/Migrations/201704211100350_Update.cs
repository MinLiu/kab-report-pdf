namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.CandidateLanguageItems", name: "LanguageID", newName: "CandidateLanguageID");
            RenameIndex(table: "dbo.CandidateLanguageItems", name: "IX_LanguageID", newName: "IX_CandidateLanguageID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.CandidateLanguageItems", name: "IX_CandidateLanguageID", newName: "IX_LanguageID");
            RenameColumn(table: "dbo.CandidateLanguageItems", name: "CandidateLanguageID", newName: "LanguageID");
        }
    }
}
