namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addHierarchy : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectHierarchies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Metaname = c.String(nullable: false),
                        InternalLabelName = c.String(nullable: false),
                        ExternalLabelName = c.String(nullable: false),
                        StructureLevel1 = c.String(),
                        StructureLevel2 = c.String(),
                        StructureLevel3 = c.String(),
                        StructureLevel4 = c.String(),
                        StructureLevel5 = c.String(),
                        StructureLevel6 = c.String(),
                        StructureLevel7 = c.String(),
                        StructureLevel8 = c.String(),
                        StructureLevel9 = c.String(),
                        StructureLevel10 = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProjectHierarchies");
        }
    }
}
