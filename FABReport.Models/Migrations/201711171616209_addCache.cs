namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCache : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserReportCaches",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        AccessMetaname = c.String(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Projects", t => t.ProjectID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.ProjectID)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserReportCaches", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserReportCaches", "ProjectID", "dbo.Projects");
            DropIndex("dbo.UserReportCaches", new[] { "UserID" });
            DropIndex("dbo.UserReportCaches", new[] { "ProjectID" });
            DropTable("dbo.UserReportCaches");
        }
    }
}
