namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyUser2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "PlainPassword", c => c.String());
            DropColumn("dbo.ProjectUsers", "Password");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectUsers", "Password", c => c.String());
            DropColumn("dbo.AspNetUsers", "PlainPassword");
        }
    }
}
