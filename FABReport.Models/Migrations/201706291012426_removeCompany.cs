namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectUsers", "Password", c => c.String());
            DropTable("dbo.Companies");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Telephone = c.String(),
                        Fax = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        VatNumber = c.String(),
                        LogoImageURL = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropColumn("dbo.ProjectUsers", "Password");
        }
    }
}
