namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmailQueue : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailQueues",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Message = c.String(nullable: false),
                        ExpectedSendingTime = c.DateTime(nullable: false),
                        SendingTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmailQueues");
        }
    }
}
