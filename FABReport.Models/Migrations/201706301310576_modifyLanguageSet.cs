namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyLanguageSet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LanguageSets", "Label_NoReportsFound", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LanguageSets", "Label_NoReportsFound");
        }
    }
}
