namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OppUpdate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CandidateOpportunities", "Created");
            DropColumn("dbo.CandidateOpportunities", "Modified");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CandidateOpportunities", "Modified", c => c.DateTime(nullable: false));
            AddColumn("dbo.CandidateOpportunities", "Created", c => c.DateTime(nullable: false));
        }
    }
}
