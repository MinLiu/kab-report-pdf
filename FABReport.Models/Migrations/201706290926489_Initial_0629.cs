namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_0629 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CandidateLanguageItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateOpportunities", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.Opportunities", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.Opportunities", "StatusID", "dbo.OpportunityStatus");
            DropForeignKey("dbo.CandidateOpportunities", "OpportunityID", "dbo.Opportunities");
            DropForeignKey("dbo.CandidateOpportunities", "StatusID", "dbo.CandidateOpportunityStatus");
            DropForeignKey("dbo.CandidateRoleItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateRoleItems", "CandidateRoleID", "dbo.CandidateRoles");
            DropForeignKey("dbo.CandidateSectorItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateSectorItems", "CandidateSectorID", "dbo.CandidateSectors");
            DropForeignKey("dbo.CandidateSoftwareItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateSoftwareItems", "CandidateSoftwareID", "dbo.CandidateSoftwares");
            DropForeignKey("dbo.Candidates", "LinkedInStatusID", "dbo.LinkedInStatus");
            DropForeignKey("dbo.CandidateLanguageItems", "CandidateLanguageID", "dbo.CandidateLanguages");
            DropForeignKey("dbo.ClientIndustryItems", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientIndustryItems", "ClientIndustryID", "dbo.ClientIndustries");
            DropIndex("dbo.CandidateLanguageItems", new[] { "CandidateID" });
            DropIndex("dbo.CandidateLanguageItems", new[] { "CandidateLanguageID" });
            DropIndex("dbo.Candidates", new[] { "LinkedInStatusID" });
            DropIndex("dbo.CandidateOpportunities", new[] { "CandidateID" });
            DropIndex("dbo.CandidateOpportunities", new[] { "OpportunityID" });
            DropIndex("dbo.CandidateOpportunities", new[] { "StatusID" });
            DropIndex("dbo.Opportunities", new[] { "ClientID" });
            DropIndex("dbo.Opportunities", new[] { "StatusID" });
            DropIndex("dbo.CandidateRoleItems", new[] { "CandidateID" });
            DropIndex("dbo.CandidateRoleItems", new[] { "CandidateRoleID" });
            DropIndex("dbo.CandidateSectorItems", new[] { "CandidateID" });
            DropIndex("dbo.CandidateSectorItems", new[] { "CandidateSectorID" });
            DropIndex("dbo.CandidateSoftwareItems", new[] { "CandidateID" });
            DropIndex("dbo.CandidateSoftwareItems", new[] { "CandidateSoftwareID" });
            DropIndex("dbo.ClientIndustryItems", new[] { "ClientID" });
            DropIndex("dbo.ClientIndustryItems", new[] { "ClientIndustryID" });
            DropTable("dbo.CandidateLanguageItems");
            DropTable("dbo.Candidates");
            DropTable("dbo.CandidateOpportunities");
            DropTable("dbo.Opportunities");
            DropTable("dbo.Clients");
            DropTable("dbo.OpportunityStatus");
            DropTable("dbo.CandidateOpportunityStatus");
            DropTable("dbo.CandidateRoleItems");
            DropTable("dbo.CandidateRoles");
            DropTable("dbo.CandidateSectorItems");
            DropTable("dbo.CandidateSectors");
            DropTable("dbo.CandidateSoftwareItems");
            DropTable("dbo.CandidateSoftwares");
            DropTable("dbo.LinkedInStatus");
            DropTable("dbo.CandidateLanguages");
            DropTable("dbo.ClientIndustries");
            DropTable("dbo.ClientIndustryItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ClientIndustryItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        ClientIndustryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientIndustries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateLanguages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LinkedInStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateSoftwares",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateSoftwareItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        CandidateSoftwareID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateSectors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateSectorItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        CandidateSectorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateRoleItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        CandidateRoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateOpportunityStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OpportunityStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        HexColour = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        DefaultColor = c.String(),
                        LogoPath = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Opportunities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        StatusID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateOpportunities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        OpportunityID = c.Int(nullable: false),
                        StatusID = c.Int(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Candidates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Notes = c.String(),
                        Rating = c.Int(nullable: false),
                        Rating2 = c.String(),
                        Attitude = c.Int(nullable: false),
                        CurrentEmployer = c.String(),
                        Source = c.String(),
                        LinkedIn = c.String(),
                        LinkedInStatusID = c.Int(),
                        LanguageID = c.Int(),
                        SearchStatusID = c.Int(),
                        CvGuideSent = c.Boolean(nullable: false),
                        CvFileName = c.String(),
                        CvFileUrl = c.String(),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateLanguageItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        CandidateLanguageID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.ClientIndustryItems", "ClientIndustryID");
            CreateIndex("dbo.ClientIndustryItems", "ClientID");
            CreateIndex("dbo.CandidateSoftwareItems", "CandidateSoftwareID");
            CreateIndex("dbo.CandidateSoftwareItems", "CandidateID");
            CreateIndex("dbo.CandidateSectorItems", "CandidateSectorID");
            CreateIndex("dbo.CandidateSectorItems", "CandidateID");
            CreateIndex("dbo.CandidateRoleItems", "CandidateRoleID");
            CreateIndex("dbo.CandidateRoleItems", "CandidateID");
            CreateIndex("dbo.Opportunities", "StatusID");
            CreateIndex("dbo.Opportunities", "ClientID");
            CreateIndex("dbo.CandidateOpportunities", "StatusID");
            CreateIndex("dbo.CandidateOpportunities", "OpportunityID");
            CreateIndex("dbo.CandidateOpportunities", "CandidateID");
            CreateIndex("dbo.Candidates", "LinkedInStatusID");
            CreateIndex("dbo.CandidateLanguageItems", "CandidateLanguageID");
            CreateIndex("dbo.CandidateLanguageItems", "CandidateID");
            AddForeignKey("dbo.ClientIndustryItems", "ClientIndustryID", "dbo.ClientIndustries", "ID");
            AddForeignKey("dbo.ClientIndustryItems", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.CandidateLanguageItems", "CandidateLanguageID", "dbo.CandidateLanguages", "ID");
            AddForeignKey("dbo.Candidates", "LinkedInStatusID", "dbo.LinkedInStatus", "ID");
            AddForeignKey("dbo.CandidateSoftwareItems", "CandidateSoftwareID", "dbo.CandidateSoftwares", "ID");
            AddForeignKey("dbo.CandidateSoftwareItems", "CandidateID", "dbo.Candidates", "ID");
            AddForeignKey("dbo.CandidateSectorItems", "CandidateSectorID", "dbo.CandidateSectors", "ID");
            AddForeignKey("dbo.CandidateSectorItems", "CandidateID", "dbo.Candidates", "ID");
            AddForeignKey("dbo.CandidateRoleItems", "CandidateRoleID", "dbo.CandidateRoles", "ID");
            AddForeignKey("dbo.CandidateRoleItems", "CandidateID", "dbo.Candidates", "ID");
            AddForeignKey("dbo.CandidateOpportunities", "StatusID", "dbo.CandidateOpportunityStatus", "ID");
            AddForeignKey("dbo.CandidateOpportunities", "OpportunityID", "dbo.Opportunities", "ID");
            AddForeignKey("dbo.Opportunities", "StatusID", "dbo.OpportunityStatus", "ID");
            AddForeignKey("dbo.Opportunities", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.CandidateOpportunities", "CandidateID", "dbo.Candidates", "ID");
            AddForeignKey("dbo.CandidateLanguageItems", "CandidateID", "dbo.Candidates", "ID");
        }
    }
}
