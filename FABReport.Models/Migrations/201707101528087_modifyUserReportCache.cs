namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyUserReportCache : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserReportCaches", "UserID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserReportCaches", "UserID", c => c.Guid(nullable: false));
        }
    }
}
