namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjectCustomizedLabel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectCustomizedLabels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Name = c.String(),
                        LabelIndex = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Projects", t => t.ProjectID)
                .Index(t => t.ProjectID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectCustomizedLabels", "ProjectID", "dbo.Projects");
            DropIndex("dbo.ProjectCustomizedLabels", new[] { "ProjectID" });
            DropTable("dbo.ProjectCustomizedLabels");
        }
    }
}
