namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProject2 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ProjectGuidances", "LanguageID");
            AddForeignKey("dbo.ProjectGuidances", "LanguageID", "dbo.Languages", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectGuidances", "LanguageID", "dbo.Languages");
            DropIndex("dbo.ProjectGuidances", new[] { "LanguageID" });
        }
    }
}
