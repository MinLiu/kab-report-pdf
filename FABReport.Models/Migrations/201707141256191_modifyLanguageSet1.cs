namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyLanguageSet1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LanguageSets", "Label_SelectAll", c => c.String(nullable: false));
            AddColumn("dbo.LanguageSets", "Label_Download", c => c.String(nullable: false));
            AddColumn("dbo.LanguageSets", "Label_DownloadAll", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LanguageSets", "Label_DownloadAll");
            DropColumn("dbo.LanguageSets", "Label_Download");
            DropColumn("dbo.LanguageSets", "Label_SelectAll");
        }
    }
}
