namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyReport1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectReports", "RelativePath", c => c.String());
            DropColumn("dbo.ProjectReports", "PhysicalPath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectReports", "PhysicalPath", c => c.String());
            DropColumn("dbo.ProjectReports", "RelativePath");
        }
    }
}
