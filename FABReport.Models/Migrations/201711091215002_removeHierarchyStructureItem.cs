namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeHierarchyStructureItem : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.HierarchyStructureItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.HierarchyStructureItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Metaname = c.String(nullable: false),
                        AccessMetaname = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
    }
}
