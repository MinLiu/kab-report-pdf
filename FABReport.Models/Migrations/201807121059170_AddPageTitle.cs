namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPageTitle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "PageTitle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "PageTitle");
        }
    }
}
