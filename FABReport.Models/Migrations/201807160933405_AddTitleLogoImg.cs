namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTitleLogoImg : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "TitleLogoPath", c => c.String());
            AddColumn("dbo.Projects", "ShowHeaderLogo", c => c.Boolean(nullable: true));
            AddColumn("dbo.Projects", "ShowTitleLogo", c => c.Boolean(nullable: false));
            DropColumn("dbo.Projects", "ShowKnbLogo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "ShowKnbLogo", c => c.Boolean(nullable: false));
            DropColumn("dbo.Projects", "ShowTitleLogo");
            DropColumn("dbo.Projects", "ShowHeaderLogo");
            DropColumn("dbo.Projects", "TitleLogoPath");
        }
    }
}
