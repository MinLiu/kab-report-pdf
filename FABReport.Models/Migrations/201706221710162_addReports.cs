namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addReports : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectReports",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Metaname = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        ReportFile = c.String(nullable: false),
                        ReportLabel = c.String(nullable: false),
                        Path = c.String(),
                        Uploaded = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProjectReports");
        }
    }
}
