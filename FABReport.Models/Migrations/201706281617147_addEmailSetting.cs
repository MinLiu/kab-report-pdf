namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmailSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Email = c.String(),
                        Password = c.String(),
                        Host = c.String(),
                        Port = c.Int(nullable: false),
                        EnableSSL = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmailSettings");
        }
    }
}
