namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecordEmailSending : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailQueues", "ProjectUserID", c => c.String(maxLength: 128));
            CreateIndex("dbo.EmailQueues", "ProjectUserID");
            AddForeignKey("dbo.EmailQueues", "ProjectUserID", "dbo.ProjectUsers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmailQueues", "ProjectUserID", "dbo.ProjectUsers");
            DropIndex("dbo.EmailQueues", new[] { "ProjectUserID" });
            DropColumn("dbo.EmailQueues", "ProjectUserID");
        }
    }
}
