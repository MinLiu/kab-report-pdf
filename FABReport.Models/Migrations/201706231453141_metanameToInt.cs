namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class metanameToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HierarchyStructureItems", "Metaname", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectHierarchies", "Metaname", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectReports", "Metaname", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectStructures", "Metaname", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectUsers", "Metaname", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProjectUsers", "Metaname", c => c.String());
            AlterColumn("dbo.ProjectStructures", "Metaname", c => c.String(nullable: false));
            AlterColumn("dbo.ProjectReports", "Metaname", c => c.String(nullable: false));
            AlterColumn("dbo.ProjectHierarchies", "Metaname", c => c.String(nullable: false));
            AlterColumn("dbo.HierarchyStructureItems", "Metaname", c => c.String(nullable: false));
        }
    }
}
