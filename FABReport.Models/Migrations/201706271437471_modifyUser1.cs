namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyUser1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectUserHierarchyItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.String(nullable: false),
                        ProjectID = c.Int(nullable: false),
                        Metaname = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.ProjectUsers", "Email", c => c.String());
            AddColumn("dbo.ProjectUsers", "FirstName", c => c.String());
            AddColumn("dbo.ProjectUsers", "LastName", c => c.String());
            AddColumn("dbo.ProjectUsers", "DefaultLanguage", c => c.String());
            AddColumn("dbo.ProjectUsers", "Grade", c => c.String());
            AddColumn("dbo.ProjectUsers", "Alignment", c => c.String());
            AddColumn("dbo.ProjectUsers", "Dist", c => c.String());
            AddColumn("dbo.ProjectUsers", "Parent", c => c.String());
            AddColumn("dbo.ProjectUsers", "Role", c => c.String());
            DropColumn("dbo.ProjectUsers", "Metaname");
            DropColumn("dbo.AspNetUsers", "Type");
            DropColumn("dbo.AspNetUsers", "DefaultLanguage");
            DropColumn("dbo.AspNetUsers", "Grade");
            DropColumn("dbo.AspNetUsers", "Alignment");
            DropColumn("dbo.AspNetUsers", "Dist");
            DropColumn("dbo.AspNetUsers", "Parent");
            DropColumn("dbo.AspNetUsers", "Role");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Role", c => c.String());
            AddColumn("dbo.AspNetUsers", "Parent", c => c.String());
            AddColumn("dbo.AspNetUsers", "Dist", c => c.String());
            AddColumn("dbo.AspNetUsers", "Alignment", c => c.String());
            AddColumn("dbo.AspNetUsers", "Grade", c => c.String());
            AddColumn("dbo.AspNetUsers", "DefaultLanguage", c => c.String());
            AddColumn("dbo.AspNetUsers", "Type", c => c.String());
            AddColumn("dbo.ProjectUsers", "Metaname", c => c.Int(nullable: false));
            DropColumn("dbo.ProjectUsers", "Role");
            DropColumn("dbo.ProjectUsers", "Parent");
            DropColumn("dbo.ProjectUsers", "Dist");
            DropColumn("dbo.ProjectUsers", "Alignment");
            DropColumn("dbo.ProjectUsers", "Grade");
            DropColumn("dbo.ProjectUsers", "DefaultLanguage");
            DropColumn("dbo.ProjectUsers", "LastName");
            DropColumn("dbo.ProjectUsers", "FirstName");
            DropColumn("dbo.ProjectUsers", "Email");
            DropTable("dbo.ProjectUserHierarchyItems");
        }
    }
}
