namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CandidateCv : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CandidateAttachments", "CandidateID", "dbo.Candidates");
            DropIndex("dbo.CandidateAttachments", new[] { "CandidateID" });
            AddColumn("dbo.Candidates", "CvFileName", c => c.String());
            AddColumn("dbo.Candidates", "CvFileUrl", c => c.String());
            DropTable("dbo.CandidateAttachments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CandidateAttachments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropColumn("dbo.Candidates", "CvFileUrl");
            DropColumn("dbo.Candidates", "CvFileName");
            CreateIndex("dbo.CandidateAttachments", "CandidateID");
            AddForeignKey("dbo.CandidateAttachments", "CandidateID", "dbo.Candidates", "ID");
        }
    }
}
