namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CandidateAttachments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Candidates", t => t.CandidateID)
                .Index(t => t.CandidateID);
            
            CreateTable(
                "dbo.Candidates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Notes = c.String(),
                        Rating = c.Int(nullable: false),
                        Rating2 = c.String(),
                        Attitude = c.Int(nullable: false),
                        ClientID = c.Int(),
                        Source = c.String(),
                        LinkedIn = c.String(),
                        LinkedInStatusID = c.Int(),
                        LanguageID = c.Int(),
                        SearchStatusID = c.Int(),
                        CvGuideSent = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.LinkedInStatus", t => t.LinkedInStatusID)
                .ForeignKey("dbo.SearchStatus", t => t.SearchStatusID)
                .Index(t => t.ClientID)
                .Index(t => t.LinkedInStatusID)
                .Index(t => t.SearchStatusID);
            
            CreateTable(
                "dbo.CandidateLanguageItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Candidates", t => t.CandidateID)
                .ForeignKey("dbo.CandidateLanguages", t => t.LanguageID)
                .Index(t => t.CandidateID)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.CandidateLanguages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateOpportunities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        OpportunityID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                        Comment = c.String(),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Candidates", t => t.CandidateID)
                .ForeignKey("dbo.Opportunities", t => t.OpportunityID)
                .ForeignKey("dbo.CandidateOpportunityStatus", t => t.StatusID)
                .Index(t => t.CandidateID)
                .Index(t => t.OpportunityID)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.Opportunities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        StatusID = c.Int(nullable: false),
                        ReplacementDate = c.DateTime(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.OpportunityStatus", t => t.StatusID)
                .Index(t => t.ClientID)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Fax = c.String(),
                        Website = c.String(),
                        Notes = c.String(),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OpportunityStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        HexColour = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateOpportunityStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        HexColour = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateRoleItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        CandidateRoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Candidates", t => t.CandidateID)
                .ForeignKey("dbo.CandidateRoles", t => t.CandidateRoleID)
                .Index(t => t.CandidateID)
                .Index(t => t.CandidateRoleID);
            
            CreateTable(
                "dbo.CandidateRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateSectorItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        CandidateSectorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Candidates", t => t.CandidateID)
                .ForeignKey("dbo.CandidateSectors", t => t.CandidateSectorID)
                .Index(t => t.CandidateID)
                .Index(t => t.CandidateSectorID);
            
            CreateTable(
                "dbo.CandidateSectors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CandidateSoftwareItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        CandidateSoftwareID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Candidates", t => t.CandidateID)
                .ForeignKey("dbo.CandidateSoftwares", t => t.CandidateSoftwareID)
                .Index(t => t.CandidateID)
                .Index(t => t.CandidateSoftwareID);
            
            CreateTable(
                "dbo.CandidateSoftwares",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LinkedInStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SearchStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Telephone = c.String(),
                        Fax = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        VatNumber = c.String(),
                        LogoImageURL = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Token = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Candidates", "SearchStatusID", "dbo.SearchStatus");
            DropForeignKey("dbo.Candidates", "LinkedInStatusID", "dbo.LinkedInStatus");
            DropForeignKey("dbo.Candidates", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.CandidateSoftwareItems", "CandidateSoftwareID", "dbo.CandidateSoftwares");
            DropForeignKey("dbo.CandidateSoftwareItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateSectorItems", "CandidateSectorID", "dbo.CandidateSectors");
            DropForeignKey("dbo.CandidateSectorItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateRoleItems", "CandidateRoleID", "dbo.CandidateRoles");
            DropForeignKey("dbo.CandidateRoleItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateOpportunities", "StatusID", "dbo.CandidateOpportunityStatus");
            DropForeignKey("dbo.Opportunities", "StatusID", "dbo.OpportunityStatus");
            DropForeignKey("dbo.Opportunities", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.CandidateOpportunities", "OpportunityID", "dbo.Opportunities");
            DropForeignKey("dbo.CandidateOpportunities", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateLanguageItems", "LanguageID", "dbo.CandidateLanguages");
            DropForeignKey("dbo.CandidateLanguageItems", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidateAttachments", "CandidateID", "dbo.Candidates");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.CandidateSoftwareItems", new[] { "CandidateSoftwareID" });
            DropIndex("dbo.CandidateSoftwareItems", new[] { "CandidateID" });
            DropIndex("dbo.CandidateSectorItems", new[] { "CandidateSectorID" });
            DropIndex("dbo.CandidateSectorItems", new[] { "CandidateID" });
            DropIndex("dbo.CandidateRoleItems", new[] { "CandidateRoleID" });
            DropIndex("dbo.CandidateRoleItems", new[] { "CandidateID" });
            DropIndex("dbo.Opportunities", new[] { "StatusID" });
            DropIndex("dbo.Opportunities", new[] { "ClientID" });
            DropIndex("dbo.CandidateOpportunities", new[] { "StatusID" });
            DropIndex("dbo.CandidateOpportunities", new[] { "OpportunityID" });
            DropIndex("dbo.CandidateOpportunities", new[] { "CandidateID" });
            DropIndex("dbo.CandidateLanguageItems", new[] { "LanguageID" });
            DropIndex("dbo.CandidateLanguageItems", new[] { "CandidateID" });
            DropIndex("dbo.Candidates", new[] { "SearchStatusID" });
            DropIndex("dbo.Candidates", new[] { "LinkedInStatusID" });
            DropIndex("dbo.Candidates", new[] { "ClientID" });
            DropIndex("dbo.CandidateAttachments", new[] { "CandidateID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Companies");
            DropTable("dbo.SearchStatus");
            DropTable("dbo.LinkedInStatus");
            DropTable("dbo.CandidateSoftwares");
            DropTable("dbo.CandidateSoftwareItems");
            DropTable("dbo.CandidateSectors");
            DropTable("dbo.CandidateSectorItems");
            DropTable("dbo.CandidateRoles");
            DropTable("dbo.CandidateRoleItems");
            DropTable("dbo.CandidateOpportunityStatus");
            DropTable("dbo.OpportunityStatus");
            DropTable("dbo.Clients");
            DropTable("dbo.Opportunities");
            DropTable("dbo.CandidateOpportunities");
            DropTable("dbo.CandidateLanguages");
            DropTable("dbo.CandidateLanguageItems");
            DropTable("dbo.Candidates");
            DropTable("dbo.CandidateAttachments");
        }
    }
}
