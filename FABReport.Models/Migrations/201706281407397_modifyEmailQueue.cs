namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyEmailQueue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailQueues", "EmailAddressTo", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailQueues", "EmailAddressTo");
        }
    }
}
