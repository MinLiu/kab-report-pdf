namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreatedBy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "CreatedByID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Projects", "CreatedByID");
            AddForeignKey("dbo.Projects", "CreatedByID", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "CreatedByID", "dbo.AspNetUsers");
            DropIndex("dbo.Projects", new[] { "CreatedByID" });
            DropColumn("dbo.Projects", "CreatedByID");
        }
    }
}
