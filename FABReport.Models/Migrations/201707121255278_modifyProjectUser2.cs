namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProjectUser2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectUsers", "Password", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectUsers", "Password");
        }
    }
}
