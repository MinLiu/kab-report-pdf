namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProjectUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectUserHierarchyItems", "ProjectUserID", c => c.Int(nullable: false));
            CreateIndex("dbo.ProjectUserHierarchyItems", "ProjectUserID");
            AddForeignKey("dbo.ProjectUserHierarchyItems", "ProjectUserID", "dbo.ProjectUsers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectUserHierarchyItems", "ProjectUserID", "dbo.ProjectUsers");
            DropIndex("dbo.ProjectUserHierarchyItems", new[] { "ProjectUserID" });
            DropColumn("dbo.ProjectUserHierarchyItems", "ProjectUserID");
        }
    }
}
