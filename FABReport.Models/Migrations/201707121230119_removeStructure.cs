namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeStructure : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.ProjectStructures");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProjectStructures",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        Metaname = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Level = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
    }
}
