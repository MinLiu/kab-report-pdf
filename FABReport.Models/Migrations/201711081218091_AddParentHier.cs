namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParentHier : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectHierarchies", "ParentMetaname", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectHierarchies", "ParentMetaname");
        }
    }
}
