// <auto-generated />
namespace KABReport.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class modifyUser2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(modifyUser2));
        
        string IMigrationMetadata.Id
        {
            get { return "201707110938336_modifyUser2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
