namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmailTemplate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectEmailTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                        Template = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Languages", t => t.LanguageID)
                .ForeignKey("dbo.Projects", t => t.ProjectID)
                .Index(t => t.ProjectID)
                .Index(t => t.LanguageID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectEmailTemplates", "ProjectID", "dbo.Projects");
            DropForeignKey("dbo.ProjectEmailTemplates", "LanguageID", "dbo.Languages");
            DropIndex("dbo.ProjectEmailTemplates", new[] { "LanguageID" });
            DropIndex("dbo.ProjectEmailTemplates", new[] { "ProjectID" });
            DropTable("dbo.ProjectEmailTemplates");
        }
    }
}
