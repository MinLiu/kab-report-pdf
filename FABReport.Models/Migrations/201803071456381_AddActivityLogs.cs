namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActivityLogs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        UserID = c.String(maxLength: 128),
                        ProjectUserID = c.String(maxLength: 128),
                        ProjectID = c.Int(nullable: false),
                        IPAddress = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Projects", t => t.ProjectID)
                .ForeignKey("dbo.ProjectUsers", t => t.ProjectUserID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID)
                .Index(t => t.ProjectUserID)
                .Index(t => t.ProjectID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActivityLogs", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ActivityLogs", "ProjectUserID", "dbo.ProjectUsers");
            DropForeignKey("dbo.ActivityLogs", "ProjectID", "dbo.Projects");
            DropIndex("dbo.ActivityLogs", new[] { "ProjectID" });
            DropIndex("dbo.ActivityLogs", new[] { "ProjectUserID" });
            DropIndex("dbo.ActivityLogs", new[] { "UserID" });
            DropTable("dbo.ActivityLogs");
        }
    }
}
