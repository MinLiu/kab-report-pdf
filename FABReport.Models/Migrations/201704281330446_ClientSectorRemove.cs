namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientSectorRemove : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ClientSectorItems", "CandidateSectorID", "dbo.CandidateSectors");
            DropForeignKey("dbo.ClientSectorItems", "ClientID", "dbo.Clients");
            DropIndex("dbo.ClientSectorItems", new[] { "ClientID" });
            DropIndex("dbo.ClientSectorItems", new[] { "CandidateSectorID" });
            DropTable("dbo.ClientSectorItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ClientSectorItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        CandidateSectorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.ClientSectorItems", "CandidateSectorID");
            CreateIndex("dbo.ClientSectorItems", "ClientID");
            AddForeignKey("dbo.ClientSectorItems", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.ClientSectorItems", "CandidateSectorID", "dbo.CandidateSectors", "ID");
        }
    }
}
