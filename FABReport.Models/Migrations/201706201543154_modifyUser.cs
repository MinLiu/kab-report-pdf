namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Type", c => c.String());
            AddColumn("dbo.AspNetUsers", "DefaultLanguage", c => c.String());
            AddColumn("dbo.AspNetUsers", "Grade", c => c.String());
            AddColumn("dbo.AspNetUsers", "Alignment", c => c.String());
            AddColumn("dbo.AspNetUsers", "Dist", c => c.String());
            AddColumn("dbo.AspNetUsers", "Parent", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Parent");
            DropColumn("dbo.AspNetUsers", "Dist");
            DropColumn("dbo.AspNetUsers", "Alignment");
            DropColumn("dbo.AspNetUsers", "Grade");
            DropColumn("dbo.AspNetUsers", "DefaultLanguage");
            DropColumn("dbo.AspNetUsers", "Type");
        }
    }
}
