namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserReportCache : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserReportCaches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Guid(nullable: false),
                        ProjectID = c.Int(nullable: false),
                        CacheReportID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserReportCaches");
        }
    }
}
