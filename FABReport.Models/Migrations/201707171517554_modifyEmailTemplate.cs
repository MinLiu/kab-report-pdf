namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyEmailTemplate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectEmailTemplates", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectEmailTemplates", "Title");
        }
    }
}
