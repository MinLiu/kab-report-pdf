// <auto-generated />
namespace KABReport.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddPageTitle : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddPageTitle));
        
        string IMigrationMetadata.Id
        {
            get { return "201807121059170_AddPageTitle"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
