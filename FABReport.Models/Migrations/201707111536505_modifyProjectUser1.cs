namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProjectUser1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProjectUserHierarchyItems", "ProjectUserID", "dbo.ProjectUsers");
            DropIndex("dbo.ProjectUserHierarchyItems", new[] { "ProjectUserID" });
            DropPrimaryKey("dbo.ProjectUsers");
            AlterColumn("dbo.ProjectUserHierarchyItems", "ProjectUserID", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.ProjectUsers", "ID");
            AddColumn("dbo.ProjectUsers", "ID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.ProjectUsers", "ID");
            CreateIndex("dbo.ProjectUserHierarchyItems", "ProjectUserID");
            AddForeignKey("dbo.ProjectUserHierarchyItems", "ProjectUserID", "dbo.ProjectUsers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectUserHierarchyItems", "ProjectUserID", "dbo.ProjectUsers");
            DropIndex("dbo.ProjectUserHierarchyItems", new[] { "ProjectUserID" });
            DropPrimaryKey("dbo.ProjectUsers");
            AlterColumn("dbo.ProjectUsers", "ID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.ProjectUserHierarchyItems", "ProjectUserID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.ProjectUsers", "ID");
            CreateIndex("dbo.ProjectUserHierarchyItems", "ProjectUserID");
            AddForeignKey("dbo.ProjectUserHierarchyItems", "ProjectUserID", "dbo.ProjectUsers", "ID");
        }
    }
}
