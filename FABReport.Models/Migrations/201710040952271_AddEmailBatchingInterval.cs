namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailBatchingInterval : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailSettings", "Interval", c => c.Int(nullable: false, defaultValue: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSettings", "Interval");
        }
    }
}
