namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyClient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "DefaultColor", c => c.String());
            AddColumn("dbo.Clients", "LogoPath", c => c.String());
            DropColumn("dbo.Clients", "Address1");
            DropColumn("dbo.Clients", "Address2");
            DropColumn("dbo.Clients", "Address3");
            DropColumn("dbo.Clients", "Town");
            DropColumn("dbo.Clients", "County");
            DropColumn("dbo.Clients", "Postcode");
            DropColumn("dbo.Clients", "Country");
            DropColumn("dbo.Clients", "Email");
            DropColumn("dbo.Clients", "Telephone");
            DropColumn("dbo.Clients", "Mobile");
            DropColumn("dbo.Clients", "Fax");
            DropColumn("dbo.Clients", "Website");
            DropColumn("dbo.Clients", "Notes");
            DropColumn("dbo.Clients", "Source");
            DropColumn("dbo.Clients", "Created");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clients", "Source", c => c.String());
            AddColumn("dbo.Clients", "Notes", c => c.String());
            AddColumn("dbo.Clients", "Website", c => c.String());
            AddColumn("dbo.Clients", "Fax", c => c.String());
            AddColumn("dbo.Clients", "Mobile", c => c.String());
            AddColumn("dbo.Clients", "Telephone", c => c.String());
            AddColumn("dbo.Clients", "Email", c => c.String());
            AddColumn("dbo.Clients", "Country", c => c.String());
            AddColumn("dbo.Clients", "Postcode", c => c.String());
            AddColumn("dbo.Clients", "County", c => c.String());
            AddColumn("dbo.Clients", "Town", c => c.String());
            AddColumn("dbo.Clients", "Address3", c => c.String());
            AddColumn("dbo.Clients", "Address2", c => c.String());
            AddColumn("dbo.Clients", "Address1", c => c.String());
            DropColumn("dbo.Clients", "LogoPath");
            DropColumn("dbo.Clients", "DefaultColor");
        }
    }
}
