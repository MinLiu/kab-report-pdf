namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProjectTheme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectThemeTemplates", "TitleColor", c => c.String());
            AddColumn("dbo.ProjectThemeTemplates", "TitleAlign", c => c.String());
            AddColumn("dbo.ProjectThemeTemplates", "TreeViewSelectedTextColor", c => c.String());
            AddColumn("dbo.ProjectThemeTemplates", "TreeViewSelectedBackgroundColor", c => c.String());
            AddColumn("dbo.ProjectThemeTemplates", "TreeViewPanelWidth", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectThemeTemplates", "TreeViewPanelWidth");
            DropColumn("dbo.ProjectThemeTemplates", "TreeViewSelectedBackgroundColor");
            DropColumn("dbo.ProjectThemeTemplates", "TreeViewSelectedTextColor");
            DropColumn("dbo.ProjectThemeTemplates", "TitleAlign");
            DropColumn("dbo.ProjectThemeTemplates", "TitleColor");
        }
    }
}
