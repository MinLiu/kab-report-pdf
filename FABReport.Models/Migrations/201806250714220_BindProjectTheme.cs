namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BindProjectTheme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "ProjectThemeTemplateID", c => c.Int());
            CreateIndex("dbo.Projects", "ProjectThemeTemplateID");
            AddForeignKey("dbo.Projects", "ProjectThemeTemplateID", "dbo.ProjectThemeTemplates", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "ProjectThemeTemplateID", "dbo.ProjectThemeTemplates");
            DropIndex("dbo.Projects", new[] { "ProjectThemeTemplateID" });
            DropColumn("dbo.Projects", "ProjectThemeTemplateID");
        }
    }
}
