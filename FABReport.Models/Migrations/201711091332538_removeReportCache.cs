namespace KABReport.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeReportCache : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.UserReportCaches");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserReportCaches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.String(),
                        ProjectID = c.Int(nullable: false),
                        CacheReportID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
    }
}
