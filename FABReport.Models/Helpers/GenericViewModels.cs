﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KABReport.Models
{
    public class SelectItemViewModel
    {
        public int? ID { get; set; }
        public string Name { get; set; }
    }

    public class UserItemViewModel
    {
        public string ID { get; set; }
        public string Email { get; set; }
    }

    public class MetanameItemViewModel
    {
        public string Name { get; set; }
    }

    public class DropDownListItemViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

}